# rdfs:comment a ObjectProperty

## Domain

definition: A comment on the dataset<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:string

## Annotations


