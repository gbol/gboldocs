# rdfs:label a ObjectProperty

## Domain

definition: Used to provide a human-readable version of a resource's name<br>
[void:Dataset](/ns/void/Dataset)

definition: Used to provide a human-readable version of a resource's name<br>
[dc:MediaTypeOrExtent](/dc/terms/MediaTypeOrExtent)

## Range

xsd:string

## Annotations


