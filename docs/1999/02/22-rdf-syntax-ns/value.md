# rdf:value a ObjectProperty

## Domain

definition: Used in describing structured values.<br>
[dc:MediaTypeOrExtent](/dc/terms/MediaTypeOrExtent)

definition: Used in describing structured values.<br>
[bibo:Literature](/ontology/bibo/Literature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to describe the content of a bibo:Document and other bibliographic resources.<br><br>We suggest to use this property instead of the deprecated \"bibo:content\" one.|

