# Contributing to GBOL

If you want to contribute to GBOL please follow the following instructions:

As most users do not have direct write access to the GBOL repository located [here](https://gitlab.com/gbol/GBOLOntology), a fork needs to be created. To create a fork follow the instructions  [here](https://docs.gitlab.com/ce/workflow/forking_workflow.html). In brief, click on the **Fork** button just below the header. Once the fork is created, clone the forked repository using the default git commands 

`git clone https://gitlab.com/mypath/torepo`

Once the repository is cloned, the easiest way to view the ontology is through [Protégé](http://protege.stanford.edu). Open the *gbol-ontology.ttl* file, which is inside the repository you just cloned with Protégé. Once open, click on Entities in the top bar of the window and then you should be able to see the *owl:Thing* class. Unfold it and more classes should appear. New classes can be added using the corresponding buttons and content can be changed by clicking twice on it.

Once you have finished the adjustments you can add the file to the git repository by using the following command from within the cloned directory:

`git add gbol-ontology.ttl`

Then write a message about the changes you made

`git commit -m "expanded skos:definition for MiscFeature`

And finalize by a push

`git push`

This should push your changes to **your** forked repository. From within gitlab a merge request can be performed if you think the changes you have made should be merged into the original ontology. More information about merge requests can be found [here](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).