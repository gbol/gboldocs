# Provenance a owl:Class

## Subclasses

[FeatureProvenance](/0.1/FeatureProvenance)

[XRefProvenance](/0.1/XRefProvenance)

## Annotations

|||
|-----|-----|
|skos:definition|Capturing element wise provenance, hold reference to data-wise provenance|
|gen:framing|:annotation|
|subDomain|ElementWiseProv|
|rdfs:label|Provenance|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[origin](/0.1/origin)|Link to the data-wise provenance|1:1|[AnnotationResult](/0.1/AnnotationResult)|
|[annotation](/0.1/annotation)|Note on the provenance|0:1|[ProvenanceApplication](/0.1/ProvenanceApplication)|
