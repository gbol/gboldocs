# LowQualitySeqRegion a skos:Concept, [ReasonArtificialLocation](/0.1/ReasonArtificialLocation)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|low-quality sequence region|

