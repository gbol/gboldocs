# UnsureBases a owl:Class extends [AssemblyAnnotation](/0.1/AssemblyAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A small region of sequenced bases, which could not be confidently identified.|
|rdfs:comment|a small region of sequenced bases, generally 10 or fewer in its length, which could not be confidently identified. Such a region might contain called bases(A, T, G, or C), or a mixture of called-bases and uncalled-bases ('N').The unsure feature should not be used when annotating gaps in genome assemblies.Please refer to assembly_gap feature for gaps within the sequence of an assembled genome. For annotation of gaps in other sequences than assembled genomes use the gap feature. *Note as observed in GCA_000018045 the unsure region can be larger*|
|rdfs:comment|use /replace=\"\" to annotate deletion, e.g. unsure      11..15 /replace=\"\"|
|ddbjLabel|unsure|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001086">http://purl.obolibrary.org/obo/SO_0001086</a>|

## Properties

