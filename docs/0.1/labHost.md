# labHost a ObjectProperty

## Domain

definition: Scientific name of the laboratory host used to propagate the source organism from which the sample was obtained. The full binomial scientific name of the host organism should be used when known. Extra conditional information relating to the host may also be included. For example '12 year old human with IBD' for a gut bacteria.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|rdfs:comment|the full binomial scientific name of the host organism should be used when known; extra conditional information relating tothe host may also be included|
|ddbjLabel|lab_host|

