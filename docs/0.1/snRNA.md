# snRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small nuclear RNA molecules involved in pre-mRNA splicing and processing.|
|ddbjLabel|snRNA|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q284578">http://www.wikidata.org/entity/Q284578</a>|

