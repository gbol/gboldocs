# CRISPRCassette a owl:Class extends [RepeatFeature](/0.1/RepeatFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|rdfs:label|CRISPRCassette|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[spacer](/0.1/spacer)|The regions that contain a spacer|1:N|[Region](/0.1/Region)|
