# SwitchRegion a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|switch region of immunoglobulin heavy chains involved in the rearrangement of heavy chain DNA leading to the expression of a different immunoglobulin class from the same B-cell;|
|ddbjLabel|S_region|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001836">http://purl.obolibrary.org/obo/SO_0001836</a>|

