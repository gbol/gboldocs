# TypeMaterial a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|type material|
|rdfs:label|type material|

