# RankLevel4 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 4|
|parentRank|<a href="http://gbol.life/0.1/RankLevel3">http://gbol.life/0.1/RankLevel3</a>|
|rdfs:label|Rank Level 4|

