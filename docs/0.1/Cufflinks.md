# Cufflinks a owl:Class extends [ProvenanceMapping](/0.1/ProvenanceMapping)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[coverage](/0.1/coverage)||1:1|xsd:Double|
|[confidenceLow](/0.1/confidenceLow)||1:1|xsd:Double|
|[confidenceHigh](/0.1/confidenceHigh)||1:1|xsd:Double|
