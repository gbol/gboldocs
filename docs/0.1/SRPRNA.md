# SRPRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|signal recognition particle, a universally conserved ribonucleoprotein involved in the co-translational targeting of proteins to membranes.|
|ddbjLabel|SRP_RNA|

