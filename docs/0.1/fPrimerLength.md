# fPrimerLength a ObjectProperty

## Domain

definition: Forward primer length<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


