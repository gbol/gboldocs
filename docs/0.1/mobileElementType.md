# mobileElementType a ObjectProperty

## Domain

definition: Mobile element type<br>
[MobileElement](/0.1/MobileElement)

## Range

[MobileElementType](/0.1/MobileElementType)

## Annotations

|||
|-----|-----|
|ddbjLabel|mobile_element_type|
|skos:editorialNote|replaced with mobileElementType and mobileElement properties|

