# SpecimenVoucher a skos:Concept, [CollectionSampleType](/0.1/CollectionSampleType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A physical specimen that remains after the sequence has been obtained. If the specimen was destroyed in the process of sequencing, electronic images (e-vouchers) are an adequate substitute for a physical voucher specimen. Ideally the specimens will be deposited in a curated museum, herbarium, or frozen tissue collection, but often they will remain in a personal or laboratory collection for some time before they are deposited in a curated collection.|

