# Variation a owl:Class extends [VariationFeature](/0.1/VariationFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Defines a genomic variant|
|rdfs:label|Variation|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[genotype](/0.1/genotype)|Genotypes of the sample the variant was called on|0:N|[VariantGenotype](/0.1/VariantGenotype)|
|[homozygousSamples](/0.1/homozygousSamples)|Number of Homozygous variant samples|0:1|xsd:Integer|
|[validated](/0.1/validated)|Validated|0:1|xsd:string|
|[end](/0.1/end)|The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.|0:1|[Position](/0.1/Position)|
|[readDepth](/0.1/readDepth)|Read depth|0:1|xsd:string|
|[numberOfSamples](/0.1/numberOfSamples)|Number of samples with data|0:1|xsd:Integer|
|[mapQuality](/0.1/mapQuality)|Mapping Quality|0:1|xsd:string|
|[alleleFreq](/0.1/alleleFreq)|Frequency in which the alternate allele is found|0:1|xsd:string|
|[referenceAllele](/0.1/referenceAllele)|Allele of the reference sequence|0:1|xsd:string|
|[ancestralAllele](/0.1/ancestralAllele)|Ancestral Allele|0:1|xsd:string|
|[quality](/0.1/quality)|Quality score of the marker|0:1|xsd:string|
|[numberNotCalled](/0.1/numberNotCalled)|Number of samples not called|0:1|xsd:Integer|
|[alleleNumber](/0.1/alleleNumber)|total number of alleles in called genotypes|0:1|xsd:Integer|
|[alleleCount](/0.1/alleleCount)|Alelle Count|0:1|xsd:Integer|
|[varType](/0.1/varType)|The type of Variation|1:1|[VariationTypes](/0.1/VariationTypes)|
|[strandBias](/0.1/strandBias)|Strand bias|0:1|xsd:string|
|[heterozygousSamples](/0.1/heterozygousSamples)|Number of Heterozygous variant samples|0:1|xsd:Integer|
|[alternate](/0.1/alternate)|Allele of the alternate sequence|0:N|xsd:string|
|[dbSNP](/0.1/dbSNP)|dbSNP membership|0:1|xsd:string|
|[CIGAR](/0.1/CIGAR)|Cigar string describing alignment of alternate allele to reference|0:1|xsd:string|
|[ID](/0.1/ID)|Variant identification code|0:1|xsd:string|
|[filter](/0.1/filter)|filter|0:1|xsd:string|
|[wildtypeSamples](/0.1/wildtypeSamples)|Number of wildtype samples|0:1|xsd:Integer|
|[baseQuality](/0.1/baseQuality)|RMS base quality|0:1|xsd:string|
