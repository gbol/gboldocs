# subLocation a ObjectProperty

## Domain

definition: subcellular localization of a predicted protein<br>
[sapp:WoLFPSort](/sapp/0.1/WoLFPSort)

## Range

xsd:anyURI

## Annotations


