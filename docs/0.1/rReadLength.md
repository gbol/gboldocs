# rReadLength a ObjectProperty

## Domain

definition: Reverse read length<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


