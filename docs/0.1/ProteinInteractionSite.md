# ProteinInteractionSite a owl:Class extends [BindingSite](/0.1/BindingSite)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of the protein that bind to another protein|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bindingProtein](/0.1/bindingProtein)|If known, a region within another protein that binds this region. If region of other protein is unknown use fuzzy position from begin to end.|0:1|[ProteinInteractionSite](/0.1/ProteinInteractionSite)|
