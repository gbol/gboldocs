# UnknownGap a skos:Concept, [GapType](/0.1/GapType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|ddbjLabel|unkown|

