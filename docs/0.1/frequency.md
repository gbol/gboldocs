# frequency a ObjectProperty

## Domain

definition: The frequency of this variation within the population<br>
[NaturalVariation](/0.1/NaturalVariation)

## Range

xsd:double

## Annotations

|||
|-----|-----|
|ddbjLabel|frequency|
|skos:editorialNote|Added to only natural variation feature, indicating the frequency of the variation|

