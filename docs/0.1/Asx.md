# Asx a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|B|
|ddbjLabel|Asx|
|rdfs:label|Asparagine or Aspartate|

