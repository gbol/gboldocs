# WholeGenomeShotgunEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|WGS|
|rdfs:label|Whole Genome Shotgun|

