# MiscRna a owl:Class extends [Transcript](/0.1/Transcript)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Any transcript that cannot be defined by other transcript types (precursor_RNA, mRNA, ncRNA, rRNA, tmRNA and tRNA);|
|ddbjLabel|misc_RNA|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
