# MobileElementType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|All types of mobile elements|
|subDomain|SequenceFeatureCore|

## skos:member

[InsertionSequence](/0.1/InsertionSequence)

[Retrotransposon](/0.1/Retrotransposon)

[Transposon](/0.1/Transposon)

[OtherMobileElementType](/0.1/OtherMobileElementType)

[LINE](/0.1/LINE)

[MITE](/0.1/MITE)

[NonLTRRetrotransposon](/0.1/NonLTRRetrotransposon)

[Integron](/0.1/Integron)

[SINE](/0.1/SINE)

