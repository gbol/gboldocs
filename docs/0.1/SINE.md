# SINE a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repetitive element, a few hundred base pairs long, that is dispersed throughout the genome. A common human SINE is the Alu element.|
|ddbjLabel|SINE|
|rdfs:label|SINE|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000206">http://purl.obolibrary.org/obo/SO_0000206</a>|

