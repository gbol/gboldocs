# Acp a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Acp|
|rdfs:label|6-Aminocaproic acid|

