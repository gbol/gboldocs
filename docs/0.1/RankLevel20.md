# RankLevel20 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 20|
|parentRank|<a href="http://gbol.life/0.1/RankLevel19">http://gbol.life/0.1/RankLevel19</a>|
|rdfs:label|Rank Level 20|

