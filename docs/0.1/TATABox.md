# TATABox a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A conserved AT-rich septamer found about 25-bp before the start point of many eukaryotic RNA polymerase II transcript units; may be involved in positioning the enzyme for correct initiation; consensus=TATA(A|T)A(A|T).|
|ddbjLabel|TATA_box|
|ddbjLabel|TATA_signal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000174">http://purl.obolibrary.org/obo/SO_0000174</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1413955">http://www.wikidata.org/entity/Q1413955</a>|

