# Enhancer a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A cis-acting sequence that increases the utilization of (some) eukaryotic promoters, and can function in either orientation and in any location (upstream or downstream) relative to the promoter.|
|ddbjLabel|enhancer|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000165">http://purl.obolibrary.org/obo/SO_0000165</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q913367">http://www.wikidata.org/entity/Q913367</a>|

