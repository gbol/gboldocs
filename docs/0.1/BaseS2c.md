# BaseS2c a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|s2c|
|rdfs:label|2-thiocytidine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q20890505">http://www.wikidata.org/entity/Q20890505</a>|

