# DataFile a owl:Class extends [prov:Entity](/ns/prov/Entity), [void:Dataset](/ns/void/Dataset)

## Subclasses

[LocalDataFile](/0.1/LocalDataFile)

[RemoteDataFile](/0.1/RemoteDataFile)

[PublicDataFile](/0.1/PublicDataFile)

## Annotations

|||
|-----|-----|
|skos:definition|An independent file on local disk or remote repository.|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[containedEntity](/0.1/containedEntity)|Optional list of entities that are contained within the file|0:N|[prov:Entity](/ns/prov/Entity)|
