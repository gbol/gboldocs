# Integron a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|integron|
|rdfs:label|integron|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000365">http://purl.obolibrary.org/obo/SO_0000365</a>|

