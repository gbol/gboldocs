# RankLevel9 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 9|
|parentRank|<a href="http://gbol.life/0.1/RankLevel8">http://gbol.life/0.1/RankLevel8</a>|
|rdfs:label|Rank Level 9|

