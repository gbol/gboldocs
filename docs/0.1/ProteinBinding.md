# ProteinBinding a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|non-covalent protein binding site on nucleic acid;|
|rdfs:comment|note that featureKey regulatory with /regulatory_class=\"ribosome_binding_site\"should be used for ribosome binding sites.|
|ddbjLabel|protein_bind|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000410">http://purl.obolibrary.org/obo/SO_0000410</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q167149">http://www.wikidata.org/entity/Q167149</a>|

## Properties

