# LinkageEvidence a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|All types of evidence possible to link to contigs together into one scaffold|
|subDomain|SequenceFeatureCore|

## skos:member

[LinkageNA](/0.1/LinkageNA)

[AlignGenus](/0.1/AlignGenus)

[PCR](/0.1/PCR)

[Map](/0.1/Map)

[AlignXGenus](/0.1/AlignXGenus)

[Strobe](/0.1/Strobe)

[Unspecified](/0.1/Unspecified)

[PairedEnds](/0.1/PairedEnds)

[WithinClone](/0.1/WithinClone)

[AlignTranscript](/0.1/AlignTranscript)

[CloneContig](/0.1/CloneContig)

