# geographicalCoordinate a ObjectProperty

## Domain

definition: The geographical coordinates(latitude + longitude) of the location where the specimen was collected<br>
[Sample](/0.1/Sample)

## Range

[GeographicalCoordinate](/0.1/GeographicalCoordinate)

## Annotations


