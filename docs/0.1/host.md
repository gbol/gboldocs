# host a ObjectProperty

## Domain

definition: The host of the organism (from which the sample was obtained), which it lives in, upon or attached to in its natural environment.<br>
[Sample](/0.1/Sample)

## Range

[TaxonomyRef](/0.1/TaxonomyRef)

## Annotations

|||
|-----|-----|
|ddbjLabel|host|

