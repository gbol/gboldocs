# LongTerminalRepeat a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|long terminal repeat, a sequence directly repeated at both ends of a defined sequence, of the sort typically found in retroviruses;|
|ddbjLabel|long_terminal_repeat|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000286">http://purl.obolibrary.org/obo/SO_0000286</a>|

