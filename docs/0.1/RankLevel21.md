# RankLevel21 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 21|
|parentRank|<a href="http://gbol.life/0.1/RankLevel20">http://gbol.life/0.1/RankLevel20</a>|
|rdfs:label|Rank Level 21|

