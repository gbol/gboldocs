# codonStart a ObjectProperty

## Domain

definition: If the start position is unknown, indicate with this property, where the first complete codon can be found<br>
[CDS](/0.1/CDS)

## Range

xsd:integer

## Annotations

|||
|-----|-----|
|ddbjLabel|codon_start|
|skos:editorialNote|Included in the exon as the reading frame can be shifted if the exon is part of a CDS|

