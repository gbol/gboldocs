# SNP a skos:Concept, [VariationTypes](/0.1/VariationTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:definition|Single Nucleotide Polymorphism|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000694">http://purl.obolibrary.org/obo/SO_0000694</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q501128">http://www.wikidata.org/entity/Q501128</a>|

