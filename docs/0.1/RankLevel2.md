# RankLevel2 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 2|
|parentRank|<a href="http://gbol.life/0.1/RankLevel1">http://gbol.life/0.1/RankLevel1</a>|
|rdfs:label|Rank Level 2|

