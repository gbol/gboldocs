# experimentalEvidence a ObjectProperty

## Domain

definition: The experimental evidence for the feature annotation (use Evidence Ontology term if possible)<br>
[ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Range

xsd:string

## Annotations


