# organismName a ObjectProperty

## Domain

definition: The name of an organism<br>
[sapp:Cog](/sapp/0.1/Cog)

## Range

xsd:string

## Annotations


