# rPrimerLength a ObjectProperty

## Domain

definition: Reverse primer length<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


