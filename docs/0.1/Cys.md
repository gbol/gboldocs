# Cys a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|C|
|ddbjLabel|Cys|
|rdfs:label|Cysteine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001447">http://purl.obolibrary.org/obo/SO_0001447</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q186474">http://www.wikidata.org/entity/Q186474</a>|

