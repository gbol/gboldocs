# BaseI6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|i6a|
|rdfs:label|N6-isopentenyladenosine|

