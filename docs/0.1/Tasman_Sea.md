# Tasman_Sea a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Tasman Sea|
|rdfs:label|Tasman Sea|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q33254">http://www.wikidata.org/entity/Q33254</a>|

