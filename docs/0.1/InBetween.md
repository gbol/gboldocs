# InBetween a owl:Class extends [Location](/0.1/Location)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Identifies a location between two bases|
|subDomain|LocationCore|
|skos:editorialNote|Faldo also has a before property, but as before is equal to after + 1 we removed this property.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#InBetweenPosition">http://biohackathon.org/resource/faldo#InBetweenPosition</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[after](/0.1/after)|The base after which the in between two bases location is located|1:1|[Position](/0.1/Position)|
|[strand](/0.1/strand)|The strand on which it is positioned|0:1|[StrandPosition](/0.1/StrandPosition)|
