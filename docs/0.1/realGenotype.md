# realGenotype a ObjectProperty

## Domain

definition: Actual Genotype of a sample for a specific variant<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


