# MaturePeptide a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Mature peptide or protein, after completion of the post-translational modifications.|
|rdfs:comment|The location does not include the stop codon (unlike the corresponding CDS)|
|ddbjLabel|mat_peptide|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000419">http://purl.obolibrary.org/obo/SO_0000419</a>|

## Properties

