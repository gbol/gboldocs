# RankLevel22 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 22|
|parentRank|<a href="http://gbol.life/0.1/RankLevel21">http://gbol.life/0.1/RankLevel21</a>|
|rdfs:label|Rank Level 22|

