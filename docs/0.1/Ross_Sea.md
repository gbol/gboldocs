# Ross_Sea a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Ross Sea|
|rdfs:label|Ross Sea|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q164466">http://www.wikidata.org/entity/Q164466</a>|

