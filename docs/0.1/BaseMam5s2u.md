# BaseMam5s2u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mam5s2u|
|rdfs:label|5-methylaminomethyl-2-thiouridine|

