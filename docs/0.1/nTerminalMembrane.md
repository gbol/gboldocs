# nTerminalMembrane a ObjectProperty

## Domain

definition: Name of the membrane on the n terminal side<br>
[IntraMembraneRegion](/0.1/IntraMembraneRegion)

## Range

xsd:string

## Annotations


