# orcid a ObjectProperty

## Domain

definition: The unique identifier to identify the scientist<br>
[foaf:Person](/foaf/0.1/Person)

definition: The unique identifier to identify the scientist<br>
[Curator](/0.1/Curator)

definition: The unique identifier to identify the scientist<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:anyURI

## Annotations


