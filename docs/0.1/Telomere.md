# Telomere a owl:Class extends [RepeatFeature](/0.1/RepeatFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of biological interest identified as a telomere and which has been experimentally characterized;|
|rdfs:comment|The telomere feature describes the interval of DNA that corresponds to a specific structure at the end of the linear eukaryotic chromosome which is required for the integrity and maintenance of the end; this region is unique compared to the rest of the chromosome and represent the physical end of the chromosome;|
|ddbjLabel|telomere|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000624">http://purl.obolibrary.org/obo/SO_0000624</a>|

## Properties

