# mismatches a ObjectProperty

## Domain

definition: The number of mismatches between two sequence objects<br>
[sapp:Blast](/sapp/0.1/Blast)

## Range

xsd:integer

## Annotations


