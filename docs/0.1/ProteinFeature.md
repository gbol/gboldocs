# ProteinFeature a owl:Class extends [Feature](/0.1/Feature)

## Subclasses

[ProteinHomology](/0.1/ProteinHomology)

[ProteinStructure](/0.1/ProteinStructure)

[BindingSite](/0.1/BindingSite)

[SignalPeptide](/0.1/SignalPeptide)

[ModifiedResidue](/0.1/ModifiedResidue)

[CrossLink](/0.1/CrossLink)

[ImmunoglobulinFeature](/0.1/ImmunoglobulinFeature)

[ProteinRepeat](/0.1/ProteinRepeat)

[Chain](/0.1/Chain)

[ConservedRegion](/0.1/ConservedRegion)

[ProPeptide](/0.1/ProPeptide)

[MaturePeptide](/0.1/MaturePeptide)

[IntraMembraneRegion](/0.1/IntraMembraneRegion)

[TransMembraneRegion](/0.1/TransMembraneRegion)

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|Some of the subclasses are based on the ontology found in the metacyc database <a href="http://bioinformatics.ai.sri.com/ptools/protein-features-ontology.html">http://bioinformatics.ai.sri.com/ptools/protein-features-ontology.html</a>|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000839">http://purl.obolibrary.org/obo/SO_0000839</a>|

## Properties

