# aHyl a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|aHyl|
|rdfs:label|allo-Hydroxylysine|

