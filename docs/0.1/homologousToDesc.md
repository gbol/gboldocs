# homologousToDesc a ObjectProperty

## Domain

definition: An optional description of the element to which it is homologous to<br>
[ProteinHomology](/0.1/ProteinHomology)

## Range

xsd:string

## Annotations


