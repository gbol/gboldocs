# RepeatType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|A list of repeat types within a genome.|
|subDomain|SequenceFeatureCore|

## skos:member

[Direct](/0.1/Direct)

[Terminal](/0.1/Terminal)

[CentromericRepeat](/0.1/CentromericRepeat)

[Dispersed](/0.1/Dispersed)

[Tandem](/0.1/Tandem)

[TelomericRepeat](/0.1/TelomericRepeat)

[YPrimeElement](/0.1/YPrimeElement)

[Nested](/0.1/Nested)

[NonLtrRetrotransposonPolymericTract](/0.1/NonLtrRetrotransposonPolymericTract)

[CRISPRRepeat](/0.1/CRISPRRepeat)

[Inverted](/0.1/Inverted)

[LongTerminalRepeat](/0.1/LongTerminalRepeat)

[RepeatTypeOther](/0.1/RepeatTypeOther)

[XElementCombinatorialRepeat](/0.1/XElementCombinatorialRepeat)

[Flanking](/0.1/Flanking)

