# ExactPosition a owl:Class extends [Position](/0.1/Position)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Default Position type, points to an exactly known position.|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from the faldo ontology. We include a 'compressed' format which allows for putting directly a xsd:PositiveInteger value within the LocatableObject without using a separate ExactPosition object. Not included the N- and C-TerminalPositidon from Faldo as all positions are counting from the 5' or N terminal site. C-Terminal counting is rarely used and can also be done from N terminal site, if that is not possible its means we have undefined sequence length which is anyhow incomparable.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#ExactPosition">http://biohackathon.org/resource/faldo#ExactPosition</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[position](/0.1/position)|The exact position (5 prime to 3 prime)|1:1|xsd:Long|
