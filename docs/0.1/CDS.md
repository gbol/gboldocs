# CDS a owl:Class extends [TranscriptFeature](/0.1/TranscriptFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Coding sequence; sequence of nucleotides that corresponds with the sequence of amino acids in a protein (location includes stop codon); feature includes amino acid conceptual translation.|
|ddbjLabel|CDS|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|TransLExcept class added to ontology, only CDS has this property<br><br>/codon_start has valid value of 1 or 2 or 3, indicating the offset at which the first complete codon of a coding feature can be found, relative to the first base of that feature; /transl_table defines the genetic code table used if other than the universal genetic code table; genetic code exceptions outside the range of the specified tables is reported in /transl_except qualifier; /protein_id consists of a stable ID portion (3+5 format with 3 position letters and 5 numbers) plus a version number after the decimal point; when the protein sequence encoded by the CDS changes, only the version number of the /protein_id value is incremented; the stable part of the /protein_id remains unchanged and as a result will permanently be associated with a given protein;|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000316">http://purl.obolibrary.org/obo/SO_0000316</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q3780824">http://www.wikidata.org/entity/Q3780824</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[proteinId](/0.1/proteinId)|The ID of the protein if translated from this CDS (format ^\\s*([A-Z]{3}\\d{5})(\\.)(\\d+)\\s*$)|0:1|xsd:string|
|[ribosomalSlippage](/0.1/ribosomalSlippage)|Indicate the presence of ribosomal slippage, the slippage region are excluded from the CDS using a CollectionOfRegions location for the CDS on the transcript.|0:1|xsd:Boolean|
|[exception](/0.1/exception)|Deprecated feature, which is imported from GenBank, mark a unconventional translation of the transcript|0:1|xsd:string|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
|[codonStart](/0.1/codonStart)|If the start position is unknown, indicate with this property, where the first complete codon can be found|0:1|xsd:Integer|
|[protein](/0.1/protein)|The resulting product protein when translating this CDS, each protein should have an IRI equal to http://gbol.life/0.1/protein/<SHA-384 key of protein sequence>.|1:1|[Protein](/0.1/Protein)|
|[translExcept](/0.1/translExcept)|Indicate all codons, which have translation other then the default translation of the translation table|0:N|[TranslExcept](/0.1/TranslExcept)|
