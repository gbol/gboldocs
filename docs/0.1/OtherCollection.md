# OtherCollection a skos:Concept, [CollectionSampleType](/0.1/CollectionSampleType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:definition|Any collection that is not culture collection and not a specimen vouchers collection.|

