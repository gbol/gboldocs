# Indian_Ocean a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Indian Ocean|
|rdfs:label|Indian Ocean|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1239">http://www.wikidata.org/entity/Q1239</a>|

