# pseudogene a ObjectProperty

## Domain

definition: If this gene is a pseudo gene, indicate the type<br>
[Gene](/0.1/Gene)

## Range

[PseudoGeneType](/0.1/PseudoGeneType)

## Annotations

|||
|-----|-----|
|ddbjLabel|pseudogene|
|skos:editorialNote|Feature added to the Gene feature, removed from all other features.|

