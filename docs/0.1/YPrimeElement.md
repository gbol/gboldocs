# YPrimeElement a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A Y' element is a repeat region (SO:0000657) located adjacent to telomeric repeats or X element combinatorial repeats, either as a single copy or tandem repeat of two to four copies.|
|ddbjLabel|y_prime_element|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001485">http://purl.obolibrary.org/obo/SO_0001485</a>|

