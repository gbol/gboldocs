# DiversitySegment a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Diversity segment of immunoglobulin heavy chain, andT-cell receptor beta chain;|
|ddbjLabel|D_segment|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000458">http://purl.obolibrary.org/obo/SO_0000458</a>|

