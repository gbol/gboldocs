# BaseMs2t6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|ms2t6a|
|rdfs:label|N-((9-beta-D-ribofuranosyl-2-methyltiopurin-6-yl)carbamoyl)threonine|

