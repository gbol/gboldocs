# Insulator a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory region that 1) when located between a CM and a gene's promoter prevents the CRM from modulating that genes expression and 2) acts as a chromatin boundary element or barrier that can block the encroachment of condensed chromatin from an adjacent region.|
|ddbjLabel|insulator|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000627">http://purl.obolibrary.org/obo/SO_0000627</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1471601">http://www.wikidata.org/entity/Q1471601</a>|

