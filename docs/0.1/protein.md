# protein a ObjectProperty

## Domain

definition: The resulting product protein when translating this CDS, each protein should have an IRI equal to http://gbol.life/0.1/protein/<SHA-384 key of protein sequence>.<br>
[CDS](/0.1/CDS)

## Range

[Protein](/0.1/Protein)

## Annotations


