# Xaa a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|X|
|ddbjLabel|Xaa|
|rdfs:label|Any amino acid|

