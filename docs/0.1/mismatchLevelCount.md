# mismatchLevelCount a ObjectProperty

## Domain

definition: Mismatch level count<br>
[ASVSet](/0.1/ASVSet)

## Range

xsd:integer

## Annotations


