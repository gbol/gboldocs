# ProteinRepeat a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat within a protein|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rptUnitSeq](/0.1/rptUnitSeq)|Pattern of the repeat. For example (NAG)8(WN)2|0:1|xsd:string|
|[rptUnitRange](/0.1/rptUnitRange)|Base range of the sequence that constitutes a repeated sequence|0:1|[Region](/0.1/Region)|
