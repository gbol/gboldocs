# BaseM4c a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m4c|
|rdfs:label|N4-methylcytosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632688">http://www.wikidata.org/entity/Q15632688</a>|

