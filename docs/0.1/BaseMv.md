# BaseMv a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mv|
|rdfs:label|uridine-5-oxoacetic acid methylester|

