# oldLocusTag a ObjectProperty

## Domain

definition: Old locus tag of the gene<br>
[Gene](/0.1/Gene)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|old_locus_tag|
|skos:editorialNote|Added to gene class, any sub elements related to gene should have the same old locus tag in the genbank file|

