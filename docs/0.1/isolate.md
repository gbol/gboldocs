# isolate a ObjectProperty

## Domain

definition: Individual isolate from which the sequence was obtained. For example ('Patient #152','DGGE band PSBAC-13')<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|isolate|

