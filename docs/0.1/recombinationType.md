# recombinationType a ObjectProperty

## Domain

definition: The type of recombination<br>
[MiscRecomb](/0.1/MiscRecomb)

## Range

[RecombinationType](/0.1/RecombinationType)

## Annotations


