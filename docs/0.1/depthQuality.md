# depthQuality a ObjectProperty

## Domain

definition: Read depth quality<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


