# RankSubTribe a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub tribe|
|parentRank|<a href="http://gbol.life/0.1/RankTribe">http://gbol.life/0.1/RankTribe</a>|
|rdfs:label|sub tribe|

