# VariationFeature a owl:Class extends [SequenceAnnotation](/0.1/SequenceAnnotation)

## Subclasses

[Variation](/0.1/Variation)

[NaturalVariation](/0.1/NaturalVariation)

[MiscVariation](/0.1/MiscVariation)

[UpdatedSequence](/0.1/UpdatedSequence)

## Annotations

|||
|-----|-----|
|skos:definition|Annotation on the sequence of modified bases in comparison to|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[replace](/0.1/replace)||0:1|xsd:string|
