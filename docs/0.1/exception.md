# exception a ObjectProperty

## Domain

definition: Deprecated feature, which is imported from GenBank, mark a unconventional translation of the transcript<br>
[CDS](/0.1/CDS)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|exception|
|owl:deprecated|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:editorialNote|deprecated, not be used any more in GBOL|

