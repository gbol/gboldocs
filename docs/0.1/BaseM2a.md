# BaseM2a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m2a|
|rdfs:label|2-methyladenosine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001296">http://purl.obolibrary.org/obo/SO_0001296</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632790">http://www.wikidata.org/entity/Q15632790</a>|

