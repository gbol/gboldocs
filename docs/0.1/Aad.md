# Aad a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Aad|
|rdfs:label|2-Aminoadipic acid|

