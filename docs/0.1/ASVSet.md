# ASVSet a owl:Class extends [SequenceSet](/0.1/SequenceSet)

## Subclasses

[RejectedASV](/0.1/RejectedASV)

[RejectedAsChimera](/0.1/RejectedAsChimera)

## Annotations

|||
|-----|-----|
|skos:definition|The definition of an ASV pair that can is a result of amplicon sequencing.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[masterASVId](/0.1/masterASVId)|ASV identifier|1:1|xsd:string|
|[ratio](/0.1/ratio)|Ratio|0:1|xsd:Float|
|[clusteredReadCount](/0.1/clusteredReadCount)|Clustered read count|0:1|xsd:Integer|
|[reverseASV](/0.1/reverseASV)|The reverse primer(s)|0:1|[ASVSequence](/0.1/ASVSequence)|
|[forwardASV](/0.1/forwardASV)|The forward primer(s)|1:1|[ASVSequence](/0.1/ASVSequence)|
|[assignedTaxon](/0.1/assignedTaxon)|Taxonomy|0:1|[Taxon](/0.1/Taxon)|
|[readCount](/0.1/readCount)|Read count|0:1|xsd:Integer|
|[asvAssignment](/0.1/asvAssignment)|ASV assignment|0:N|[ASVAssignment](/0.1/ASVAssignment)|
|[taxonHitCount](/0.1/taxonHitCount)|Taxon hit count|0:1|xsd:Integer|
|[mismatchLevelCount](/0.1/mismatchLevelCount)|Mismatch level count|0:N|xsd:Integer|
|[usedTaxonLevel](/0.1/usedTaxonLevel)|Used taxon level|0:1|xsd:Integer|
|[mismatchCount](/0.1/mismatchCount)|Mismatch count|0:1|xsd:Integer|
|[taxonType](/0.1/taxonType)|The type of taxon|0:1|[TaxonAssignmentType](/0.1/TaxonAssignmentType)|
