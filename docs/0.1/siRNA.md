# siRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small RNA molecule that is the product of a longer exogenous or endogenous double stranded RNA, which is either a bimolecular duplex or very long hairpin, processed (via the Dicer pathway) such that numerous siRNAs accumulate from both strands of the double stranded RNA. sRNAs trigger the cleavage of their target molecules.|
|ddbjLabel|siRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000646">http://purl.obolibrary.org/obo/SO_0000646</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q203221">http://www.wikidata.org/entity/Q203221</a>|

