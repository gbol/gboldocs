# tissueLib a ObjectProperty

## Domain

definition: The tissue library from which the sample was obtained<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|tissue_lib|

