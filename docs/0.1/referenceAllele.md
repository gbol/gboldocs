# referenceAllele a ObjectProperty

## Domain

definition: Allele of the reference sequence<br>
[Variation](/0.1/Variation)

## Range

xsd:string

## Annotations


