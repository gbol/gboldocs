# StandardQTL a skos:Concept, [QTLTypes](/0.1/QTLTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:definition|Standard QTL type mapped with genome data|

