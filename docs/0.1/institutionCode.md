# institutionCode a ObjectProperty

## Domain

definition: code of the institution taken from the list available at ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/coll_dump.txt<br>
[MaterialSource](/0.1/MaterialSource)

## Range

xsd:string

## Annotations


