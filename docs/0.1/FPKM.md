# FPKM a skos:Concept, [ExpressionType](/0.1/ExpressionType) extends [Normalised](/0.1/Normalised)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Isoform-level relative abundance in Fragments Per Kilobase of exon model per Million mapped fragments|

