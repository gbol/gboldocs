# Gln a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|Q|
|ddbjLabel|Gln|
|rdfs:label|Glutamine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001448">http://purl.obolibrary.org/obo/SO_0001448</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q181619">http://www.wikidata.org/entity/Q181619</a>|

