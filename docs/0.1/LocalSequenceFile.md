# LocalSequenceFile a owl:Class extends [LocalDataFile](/0.1/LocalDataFile)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A sequence file on the local disk of the type FASTA or FASTQ|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[reads](/0.1/reads)|Number of reads in the file|1:1|xsd:Long|
|[sequencer](/0.1/sequencer)||0:1|[SequencingPlatform](/0.1/SequencingPlatform)|
|[bases](/0.1/bases)||1:1|xsd:Long|
|[readLength](/0.1/readLength)||1:1|xsd:Long|
|[sequencingDepth](/0.1/sequencingDepth)||0:1|xsd:Integer|
|[StrandOrientation](/0.1/StrandOrientation)||0:1|[ReadOrientation](/0.1/ReadOrientation)|
|[adapter](/0.1/adapter)||0:1|xsd:string|
