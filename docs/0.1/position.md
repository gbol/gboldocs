# position a ObjectProperty

## Domain

definition: The exact position (5 prime to 3 prime)<br>
[ExactPosition](/0.1/ExactPosition)

[AfterPosition](/0.1/AfterPosition)

[OneOfPosition](/0.1/OneOfPosition)

[BeforePosition](/0.1/BeforePosition)

## Range

xsd:long

## Annotations


