# dataSetVersion a ObjectProperty

## Domain

definition: The version of the GBOL Data set, increase by 1 for every (set of) update(s)<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

xsd:integer

## Annotations


