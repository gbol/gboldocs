# ForwardStrandPosition a skos:Concept, [StrandPosition](/0.1/StrandPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|The feature is located/only tells something about the forward strand.|
|skos:editorialNote|Taken from Faldo ontology|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#ForwardStrandPosition">http://biohackathon.org/resource/faldo#ForwardStrandPosition</a>|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001030">http://purl.obolibrary.org/obo/SO_0001030</a>|

