# bAla a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|bAla|
|rdfs:label|beta-Alanine (beta-Aminoproprionic acid)|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q310919">http://www.wikidata.org/entity/Q310919</a>|

