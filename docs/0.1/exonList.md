# exonList a ObjectProperty

## Domain

definition: This list off exons that form this transcript, in case of prokaryote it contains one element<br>
[Transcript](/0.1/Transcript)

## Range

[ExonList](/0.1/ExonList)

## Annotations


