# Silencer a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory region which upon binding of transcription factors, suppress the transcription of the gene or genes they control.|
|ddbjLabel|silencer|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000625">http://purl.obolibrary.org/obo/SO_0000625</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1781769">http://www.wikidata.org/entity/Q1781769</a>|

