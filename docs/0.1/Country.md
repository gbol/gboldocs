# Country a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The list of all possible countries|
|subDomain|DocumentCore|

## skos:member

[Oman](/0.1/Oman)

[Malaysia](/0.1/Malaysia)

[Tunisia](/0.1/Tunisia)

[Faroe_Islands](/0.1/Faroe_Islands)

[USA](/0.1/USA)

[Honduras](/0.1/Honduras)

[Singapore](/0.1/Singapore)

[Greece](/0.1/Greece)

[Guyana](/0.1/Guyana)

[French_Southern_and_Antarctic_Lands](/0.1/French_Southern_and_Antarctic_Lands)

[Latvia](/0.1/Latvia)

[Indonesia](/0.1/Indonesia)

[Sint_Maarten](/0.1/Sint_Maarten)

[Korea](/0.1/Korea)

[Former_Yugoslav_Republic_of_Macedonia](/0.1/Former_Yugoslav_Republic_of_Macedonia)

[Micronesia](/0.1/Micronesia)

[United_Kingdom](/0.1/United_Kingdom)

[Moldova](/0.1/Moldova)

[Philippines](/0.1/Philippines)

[Sierra_Leone](/0.1/Sierra_Leone)

[Switzerland](/0.1/Switzerland)

[West_Bank](/0.1/West_Bank)

[Egypt](/0.1/Egypt)

[Guatemala](/0.1/Guatemala)

[Togo](/0.1/Togo)

[Paracel_Islands](/0.1/Paracel_Islands)

[Reunion](/0.1/Reunion)

[Cameroon](/0.1/Cameroon)

[Yemen](/0.1/Yemen)

[Cocos_Islands](/0.1/Cocos_Islands)

[Baker_Island](/0.1/Baker_Island)

[Cayman_Islands](/0.1/Cayman_Islands)

[Italy](/0.1/Italy)

[Nauru](/0.1/Nauru)

[Palmyra_Atoll](/0.1/Palmyra_Atoll)

[Venezuela](/0.1/Venezuela)

[Botswana](/0.1/Botswana)

[Dominica](/0.1/Dominica)

[Swaziland](/0.1/Swaziland)

[Wallis_and_Futuna](/0.1/Wallis_and_Futuna)

[North_Korea](/0.1/North_Korea)

[South_Korea](/0.1/South_Korea)

[Juan_de_Nova_Island](/0.1/Juan_de_Nova_Island)

[Bahamas](/0.1/Bahamas)

[Gibraltar](/0.1/Gibraltar)

[Tromelin_Island](/0.1/Tromelin_Island)

[Kyrgyzstan](/0.1/Kyrgyzstan)

[Libya](/0.1/Libya)

[Solomon_Islands](/0.1/Solomon_Islands)

[Gambia](/0.1/Gambia)

[Barbados](/0.1/Barbados)

[Afghanistan](/0.1/Afghanistan)

[Ethiopia](/0.1/Ethiopia)

[Lebanon](/0.1/Lebanon)

[Trinidad_and_Tobago](/0.1/Trinidad_and_Tobago)

[Gabon](/0.1/Gabon)

[Guinea](/0.1/Guinea)

[Wake_Island](/0.1/Wake_Island)

[Slovakia](/0.1/Slovakia)

[Ashmore_and_Cartier_Islands](/0.1/Ashmore_and_Cartier_Islands)

[Angola](/0.1/Angola)

[Kenya](/0.1/Kenya)

[Curacao](/0.1/Curacao)

[Syria](/0.1/Syria)

[Andorra](/0.1/Andorra)

[Albania](/0.1/Albania)

[GuineaBissau](/0.1/GuineaBissau)

[Pakistan](/0.1/Pakistan)

[Tokelau](/0.1/Tokelau)

[Northern_Mariana_Islands](/0.1/Northern_Mariana_Islands)

[Mayotte](/0.1/Mayotte)

[Somalia](/0.1/Somalia)

[Croatia](/0.1/Croatia)

[Zaire](/0.1/Zaire)

[Coral_Sea_Islands](/0.1/Coral_Sea_Islands)

[Eritrea](/0.1/Eritrea)

[Mongolia](/0.1/Mongolia)

[Paraguay](/0.1/Paraguay)

[Bahrain](/0.1/Bahrain)

[Portugal](/0.1/Portugal)

[Bolivia](/0.1/Bolivia)

[Turks_and_Caicos_Islands](/0.1/Turks_and_Caicos_Islands)

[Brazil](/0.1/Brazil)

[Liberia](/0.1/Liberia)

[Senegal](/0.1/Senegal)

[Peru](/0.1/Peru)

[South_Africa](/0.1/South_Africa)

[Kuwait](/0.1/Kuwait)

[Siam](/0.1/Siam)

[Liechtenstein](/0.1/Liechtenstein)

[Cambodia](/0.1/Cambodia)

[Jamaica](/0.1/Jamaica)

[Netherlands_Antilles](/0.1/Netherlands_Antilles)

[Israel](/0.1/Israel)

[Jersey](/0.1/Jersey)

[Puerto_Rico](/0.1/Puerto_Rico)

[Hungary](/0.1/Hungary)

[Cyprus](/0.1/Cyprus)

[American_Samoa](/0.1/American_Samoa)

[South_Sudan](/0.1/South_Sudan)

[Democratic_Republic_of_the_Congo](/0.1/Democratic_Republic_of_the_Congo)

[Papua_New_Guinea](/0.1/Papua_New_Guinea)

[Aruba](/0.1/Aruba)

[Isle_of_Man](/0.1/Isle_of_Man)

[El_Salvador](/0.1/El_Salvador)

[Sao_Tome_and_Principe](/0.1/Sao_Tome_and_Principe)

[Serbia_and_Montenegro](/0.1/Serbia_and_Montenegro)

[Iran](/0.1/Iran)

[Saudi_Arabia](/0.1/Saudi_Arabia)

[British_Guiana](/0.1/British_Guiana)

[Brunei](/0.1/Brunei)

[Iraq](/0.1/Iraq)

[State_of_Palestine](/0.1/State_of_Palestine)

[Nigeria](/0.1/Nigeria)

[Ireland](/0.1/Ireland)

[Svalbard](/0.1/Svalbard)

[China](/0.1/China)

[Equatorial_Guinea](/0.1/Equatorial_Guinea)

[Anguilla](/0.1/Anguilla)

[Comoros](/0.1/Comoros)

[Falkland_Islands](/0.1/Falkland_Islands)

[Marshall_Islands](/0.1/Marshall_Islands)

[Heard_Island_and_McDonald_Islands](/0.1/Heard_Island_and_McDonald_Islands)

[French_Guiana](/0.1/French_Guiana)

[Bosnia_and_Herzegovina](/0.1/Bosnia_and_Herzegovina)

[Chad](/0.1/Chad)

[Fiji](/0.1/Fiji)

[Howland_Island](/0.1/Howland_Island)

[Denmark](/0.1/Denmark)

[Djibouti](/0.1/Djibouti)

[Haiti](/0.1/Haiti)

[Namibia](/0.1/Namibia)

[Czechoslovakia](/0.1/Czechoslovakia)

[Line_Islands](/0.1/Line_Islands)

[Lesotho](/0.1/Lesotho)

[Turkey](/0.1/Turkey)

[Canada](/0.1/Canada)

[United_Arab_Emirates](/0.1/United_Arab_Emirates)

[Ukraine](/0.1/Ukraine)

[Austria](/0.1/Austria)

[Norfolk_Island](/0.1/Norfolk_Island)

[Taiwan](/0.1/Taiwan)

[Borneo](/0.1/Borneo)

[USSR](/0.1/USSR)

[Central_African_Republic](/0.1/Central_African_Republic)

[Suriname](/0.1/Suriname)

[Czech_Republic](/0.1/Czech_Republic)

[Niue](/0.1/Niue)

[Western_Sahara](/0.1/Western_Sahara)

[Estonia](/0.1/Estonia)

[Belize](/0.1/Belize)

[Costa_Rica](/0.1/Costa_Rica)

[Montserrat](/0.1/Montserrat)

[Niger](/0.1/Niger)

[Samoa](/0.1/Samoa)

[Ghana](/0.1/Ghana)

[Tajikistan](/0.1/Tajikistan)

[Saint_Helena](/0.1/Saint_Helena)

[Sudan](/0.1/Sudan)

[Guadeloupe](/0.1/Guadeloupe)

[Australia](/0.1/Australia)

[Saint_Kitts_and_Nevis](/0.1/Saint_Kitts_and_Nevis)

[Malawi](/0.1/Malawi)

[Kazakhstan](/0.1/Kazakhstan)

[Benin](/0.1/Benin)

[Chile](/0.1/Chile)

[Macedonia](/0.1/Macedonia)

[Johnston_Atoll](/0.1/Johnston_Atoll)

[Republic_of_the_Congo](/0.1/Republic_of_the_Congo)

[Russia](/0.1/Russia)

[Zimbabwe](/0.1/Zimbabwe)

[Slovenia](/0.1/Slovenia)

[Colombia](/0.1/Colombia)

[Guernsey](/0.1/Guernsey)

[Montenegro](/0.1/Montenegro)

[Guam](/0.1/Guam)

[Mauritania](/0.1/Mauritania)

[Saint_Vincent_and_the_Grenadines](/0.1/Saint_Vincent_and_the_Grenadines)

[New_Caledonia](/0.1/New_Caledonia)

[Bangladesh](/0.1/Bangladesh)

[UnknownCountry](/0.1/UnknownCountry)

[Mexico](/0.1/Mexico)

[Bhutan](/0.1/Bhutan)

[Kerguelen_Archipelago](/0.1/Kerguelen_Archipelago)

[Burkina_Faso](/0.1/Burkina_Faso)

[Belgian_Congo](/0.1/Belgian_Congo)

[Palau](/0.1/Palau)

[Jordan](/0.1/Jordan)

[India](/0.1/India)

[Grenada](/0.1/Grenada)

[Iceland](/0.1/Iceland)

[Spain](/0.1/Spain)

[Sri_Lanka](/0.1/Sri_Lanka)

[Clipperton_Island](/0.1/Clipperton_Island)

[Bermuda](/0.1/Bermuda)

[Germany](/0.1/Germany)

[Midway_Islands](/0.1/Midway_Islands)

[Poland](/0.1/Poland)

[Sweden](/0.1/Sweden)

[Madagascar](/0.1/Madagascar)

[Panama](/0.1/Panama)

[Uzbekistan](/0.1/Uzbekistan)

[Belarus](/0.1/Belarus)

[Virgin_Islands](/0.1/Virgin_Islands)

[San_Marino](/0.1/San_Marino)

[Zambia](/0.1/Zambia)

[Seychelles](/0.1/Seychelles)

[Morocco](/0.1/Morocco)

[Kingman_Reef](/0.1/Kingman_Reef)

[Cook_Islands](/0.1/Cook_Islands)

[Lithuania](/0.1/Lithuania)

[Jarvis_Island](/0.1/Jarvis_Island)

[Viet_Nam](/0.1/Viet_Nam)

[Georgia](/0.1/Georgia)

[Luxembourg](/0.1/Luxembourg)

[Mali](/0.1/Mali)

[Monaco](/0.1/Monaco)

[Spratly_Islands](/0.1/Spratly_Islands)

[Europa_Island](/0.1/Europa_Island)

[Yugoslavia](/0.1/Yugoslavia)

[Cape_Verde](/0.1/Cape_Verde)

[France](/0.1/France)

[Serbia](/0.1/Serbia)

[South_Georgia_and_the_South_Sandwich_Islands](/0.1/South_Georgia_and_the_South_Sandwich_Islands)

[Greenland](/0.1/Greenland)

[Japan](/0.1/Japan)

[Kosovo](/0.1/Kosovo)

[Burundi](/0.1/Burundi)

[Nicaragua](/0.1/Nicaragua)

[Tanzania](/0.1/Tanzania)

[Tonga](/0.1/Tonga)

[East_Timor](/0.1/East_Timor)

[Pitcairn_Islands](/0.1/Pitcairn_Islands)

[Navassa_Island](/0.1/Navassa_Island)

[Bouvet_Island](/0.1/Bouvet_Island)

[Mozambique](/0.1/Mozambique)

[Cuba](/0.1/Cuba)

[New_Zealand](/0.1/New_Zealand)

[Romania](/0.1/Romania)

[Argentina](/0.1/Argentina)

[Christmas_Island](/0.1/Christmas_Island)

[Norway](/0.1/Norway)

[Uganda](/0.1/Uganda)

[Azerbaijan](/0.1/Azerbaijan)

[Laos](/0.1/Laos)

[Uruguay](/0.1/Uruguay)

[Algeria](/0.1/Algeria)

[Bassas_da_India](/0.1/Bassas_da_India)

[British_Virgin_Islands](/0.1/British_Virgin_Islands)

[Kiribati](/0.1/Kiribati)

[Rwanda](/0.1/Rwanda)

[Antarctica](/0.1/Antarctica)

[French_Polynesia](/0.1/French_Polynesia)

[Antigua_and_Barbuda](/0.1/Antigua_and_Barbuda)

[Myanmar](/0.1/Myanmar)

[Thailand](/0.1/Thailand)

[Cote_d_Ivoire](/0.1/Cote_d_Ivoire)

[Burma](/0.1/Burma)

[Mauritius](/0.1/Mauritius)

[Martinique](/0.1/Martinique)

[Tuvalu](/0.1/Tuvalu)

[Dominican_Republic](/0.1/Dominican_Republic)

[Vanuatu](/0.1/Vanuatu)

[Maldives](/0.1/Maldives)

[Qatar](/0.1/Qatar)

[Finland](/0.1/Finland)

[Saint_Pierre_and_Miquelon](/0.1/Saint_Pierre_and_Miquelon)

[Belgium](/0.1/Belgium)

[Nepal](/0.1/Nepal)

[Glorioso_Islands](/0.1/Glorioso_Islands)

[Netherlands](/0.1/Netherlands)

[Hong_Kong](/0.1/Hong_Kong)

[Gaza_Strip](/0.1/Gaza_Strip)

[Saint_Lucia](/0.1/Saint_Lucia)

[Malta](/0.1/Malta)

[Turkmenistan](/0.1/Turkmenistan)

[Armenia](/0.1/Armenia)

[Jan_Mayen](/0.1/Jan_Mayen)

[Macau](/0.1/Macau)

[Ecuador](/0.1/Ecuador)

[Bulgaria](/0.1/Bulgaria)

