# AlignGenus a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|alignment to a reference genome within the same genus.|
|ddbjLabel|align_genus|

