# Sequence a owl:Class

## Subclasses

[Protein](/0.1/Protein)

[NASequence](/0.1/NASequence)

## Annotations

|||
|-----|-----|
|skos:definition|Represents a physical sequence|
|rdfs:comment|Physical sequence can be a DNA, RNA or Protein molecule|
|gen:framing|:feature - :Exon|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[citation](/0.1/citation)|Any publication associated to feature annotation|0:N|[Citation](/0.1/Citation)|
|[sample](/0.1/sample)|Sample from which this piece of sequence originates from.|0:1|[Sample](/0.1/Sample)|
|[function](/0.1/function)|Short description of the function|0:1|xsd:string|
|[description](/0.1/description)|An optional short description|0:1|xsd:string|
|[accession](/0.1/accession)|The accession id of the entry referenced|0:N|xsd:string|
|[sequenceVersion](/0.1/sequenceVersion)|The version of the sequence|0:1|xsd:Integer|
|[sequence](/0.1/sequence)|The sequence, encoded with the default DNA or protein encoding|1:1|xsd:string|
|[xref](/0.1/xref)|Reference to other entries holding entries with related information in other databases|0:N|[XRef](/0.1/XRef)|
|[length](/0.1/length)|Length of the sequence|1:1|xsd:Long|
|[alternativeNames](/0.1/alternativeNames)|Any alternative names of the sequence|0:N|xsd:string|
|[feature](/0.1/feature)|The set of features annotating the sequence|0:N|[Feature](/0.1/Feature)|
|[organism](/0.1/organism)|The organism from which the sequence originates from|0:1|[Organism](/0.1/Organism)|
|[recommendedName](/0.1/recommendedName)|The recommended name of the sequence|0:1|xsd:string|
|[commonName](/0.1/commonName)|The common name of the sequence|0:1|xsd:string|
|[sha384](/0.1/sha384)|The hash key of  the sequence|1:1|xsd:string|
|[standardName](/0.1/standardName)|The standard name of the feature|0:1|xsd:string|
|[shortName](/0.1/shortName)|The short name of the sequence|0:1|xsd:string|
|[note](/0.1/note)|A set off some additional notes|0:N|[Note](/0.1/Note)|
|[assembly](/0.1/assembly)|The optional assembly information of the sequence|0:1|[SequenceAssembly](/0.1/SequenceAssembly)|
