# includedXRefVersion a ObjectProperty

## Domain

definition: The version of the sequence to include, should be specified if referenced resoure can have multiple versions. If not specified it referes to the first version.<br>
[PartXRefSequence](/0.1/PartXRefSequence)

## Range

xsd:string

## Annotations


