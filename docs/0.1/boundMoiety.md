# boundMoiety a ObjectProperty

## Domain

definition: Textual description of the moiety recognizing the region<br>
[BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|bound_moiety|
|skos:editorialNote|Now linked to the biological recognized region|
|skos:editorialNote|should belong to biological recognized region, the bound_moiety being the identity that recognizes the reactions, which can be a sequence, protein, protein complex or any other molecule.|

