# RankLevel7 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 7|
|parentRank|<a href="http://gbol.life/0.1/RankLevel6">http://gbol.life/0.1/RankLevel6</a>|
|rdfs:label|Rank Level 7|

