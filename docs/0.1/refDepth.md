# refDepth a ObjectProperty

## Domain

definition: Read depth of reference base<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


