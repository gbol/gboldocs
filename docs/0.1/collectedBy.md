# collectedBy a ObjectProperty

## Domain

definition: The persons or institute who collected the specimen<br>
[Sample](/0.1/Sample)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|ddbjLabel|collected_by|

