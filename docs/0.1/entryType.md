# entryType a ObjectProperty

## Domain

definition: The type of this GBOLDataSet<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[EntryType](/0.1/EntryType)

## Annotations


