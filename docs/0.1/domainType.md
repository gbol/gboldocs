# domainType a ObjectProperty

## Domain

definition: Option to denote the type of domain<br>
[ProteinDomain](/0.1/ProteinDomain)

## Range

[ProteinDomainType](/0.1/ProteinDomainType)

## Annotations


