# linkageEvidence a ObjectProperty

## Domain

definition: The linkage evidence use to link the sequences on both sides<br>
[AssemblyGap](/0.1/AssemblyGap)

## Range

[LinkageEvidence](/0.1/LinkageEvidence)

## Annotations

|||
|-----|-----|
|ddbjLabel|linkage_evidence|
|skos:editorialNote|add to the Gap feature class, enumeration created|

