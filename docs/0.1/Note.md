# Note a owl:Class extends [Qualifier](/0.1/Qualifier)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|DocumentCore|
|rdfs:label|Note|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[provenance](/0.1/provenance)|The provenance of the feature|1:1|[Provenance](/0.1/Provenance)|
|[text](/0.1/text)|The textual description of the note|1:1|xsd:string|
