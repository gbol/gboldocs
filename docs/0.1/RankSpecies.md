# RankSpecies a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: species|
|parentRank|<a href="http://gbol.life/0.1/RankSpeciesSubGroup">http://gbol.life/0.1/RankSpeciesSubGroup</a>|
|rdfs:label|species|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7432">http://www.wikidata.org/entity/Q7432</a>|

