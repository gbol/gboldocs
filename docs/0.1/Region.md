# Region a owl:Class extends [Location](/0.1/Location)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.|
|subDomain|LocationCore|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#Region">http://biohackathon.org/resource/faldo#Region</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[strand](/0.1/strand)|The strand on which it is positioned|0:1|[StrandPosition](/0.1/StrandPosition)|
|[end](/0.1/end)|The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.|1:1|[Position](/0.1/Position)|
|[begin](/0.1/begin)|The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.|1:1|[Position](/0.1/Position)|
