# readCount a ObjectProperty

## Domain

definition: Read count<br>
[ASVSet](/0.1/ASVSet)

## Range

xsd:integer

## Annotations


