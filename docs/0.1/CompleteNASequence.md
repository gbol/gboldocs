# CompleteNASequence a owl:Class extends [NASequence](/0.1/NASequence)

## Subclasses

[MiscNA](/0.1/MiscNA)

[Plasmid](/0.1/Plasmid)

[Chromosome](/0.1/Chromosome)

## Annotations

|||
|-----|-----|
|skos:definition|Complete sequences of physical nucleotide molecule.|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[organism](/0.1/organism)|The organism from which the sequence originates from|1:1|[Organism](/0.1/Organism)|
|[strandType](/0.1/strandType)|The type of the strand|1:1|[StrandType](/0.1/StrandType)|
