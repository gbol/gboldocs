# ModifiedBase a owl:Class extends [SequenceAnnotation](/0.1/SequenceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The indicated nucleotide is a modified nucleotide and should be substituted for by the indicated molecule(given in the mod_base qualifier value)|
|rdfs:comment|value is limited to the restricted vocabulary for modified base abbreviations;|
|ddbjLabel|modified_base|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[modBase](/0.1/modBase)|The type of modification|1:1|[BaseType](/0.1/BaseType)|
|[modificationFunction](/0.1/modificationFunction)|If applicable, the biological process in which this modification is involved. Use a Biological Process from the GO ontology.|0:1|IRI|
