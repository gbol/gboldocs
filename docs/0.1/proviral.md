# proviral a ObjectProperty

## Domain

definition: Indicates that this sequence is obtained from a virus or phage that is integrated into the genome of another organism<br>
[NASequence](/0.1/NASequence)

## Range

xsd:boolean

## Annotations

|||
|-----|-----|
|ddbjLabel|proviral|

