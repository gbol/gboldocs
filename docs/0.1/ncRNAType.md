# ncRNAType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|List of non coding RNA types.|
|subDomain|SequenceFeatureCore|

## skos:member

[antisenseRNA](/0.1/antisenseRNA)

[miRNA](/0.1/miRNA)

[SRPRNA](/0.1/SRPRNA)

[scRNA](/0.1/scRNA)

[ncRNATypeOther](/0.1/ncRNATypeOther)

[piRNA](/0.1/piRNA)

[snRNA](/0.1/snRNA)

[YRNA](/0.1/YRNA)

[ribozyme](/0.1/ribozyme)

[snoRNA](/0.1/snoRNA)

[guideRNA](/0.1/guideRNA)

[rasiRNA](/0.1/rasiRNA)

[siRNA](/0.1/siRNA)

[lncRNA](/0.1/lncRNA)

[vaultRNA](/0.1/vaultRNA)

[telomeraseRNA](/0.1/telomeraseRNA)

[autocatalyticallySplicedIntron](/0.1/autocatalyticallySplicedIntron)

[hammerheadRibozyme](/0.1/hammerheadRibozyme)

[RNasePRNA](/0.1/RNasePRNA)

[RNaseMRPRNA](/0.1/RNaseMRPRNA)

