# QTL a owl:Class extends [Feature](/0.1/Feature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Quantitative Trait Loci|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000771">http://purl.obolibrary.org/obo/SO_0000771</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q853421">http://www.wikidata.org/entity/Q853421</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[comment](/0.1/comment)|comment with additional information about the QTL|0:1|xsd:string|
|[qtlMap](/0.1/qtlMap)|Map the QTL is mapped on|0:1|[QTLMap](/0.1/QTLMap)|
|[trait](/0.1/trait)|trait according to the Plant trait ontology: http://purl.obolibrary.org/obo/TO_0000661|0:N|IRI|
|[associatedFeature](/0.1/associatedFeature)|associated markers linked to the QTL region|0:N|[Feature](/0.1/Feature)|
|[symbol](/0.1/symbol)|Symbol where the QTL was published under|0:1|xsd:string|
|[relativeStart](/0.1/relativeStart)|relative position start in centiMorgan|0:1|xsd:Double|
|[relativeEnd](/0.1/relativeEnd)|relative position stop in centiMorgan|0:1|xsd:Double|
|[accession](/0.1/accession)|The accession id of the entry referenced|0:1|xsd:string|
|[parent](/0.1/parent)|Parental strains|0:N|[Organism](/0.1/Organism)|
|[traitName](/0.1/traitName)|trait name|0:1|xsd:string|
|[term](/0.1/term)|association term|0:N|xsd:string|
|[Type](/0.1/Type)|The type of QTL|1:1|[QTLTypes](/0.1/QTLTypes)|
|[linkageGroup](/0.1/linkageGroup)|linkage group|0:1|xsd:string|
