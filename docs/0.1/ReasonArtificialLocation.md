# ReasonArtificialLocation a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|Reasons why a given location is based on an artificial location.|
|subDomain|SequenceFeatureCore|

## skos:member

[HeterogeneousPopulation](/0.1/HeterogeneousPopulation)

[LowQualitySeqRegion](/0.1/LowQualitySeqRegion)

