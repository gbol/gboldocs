# ExpressionType a skos:Collection extends owl:Class

## Annotations


## skos:member

[RawCounts](/0.1/RawCounts)

[Normalised](/0.1/Normalised)

[TPM](/0.1/TPM)

[FPKM](/0.1/FPKM)

[Counts](/0.1/Counts)

[EstimatedCounts](/0.1/EstimatedCounts)

[ExpectedCounts](/0.1/ExpectedCounts)

[RPKM](/0.1/RPKM)

