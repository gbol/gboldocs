# Epitype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|specimen or illustration selected to serve as an interpretative type when the holotype, lectotype, or previously designated neotype, or all original material associated with a validly published name, cannot be identified for the purpose of the precise application of the name to a taxon|
|ddbjLabel|epitype|
|rdfs:label|epitype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q55195035">http://www.wikidata.org/entity/Q55195035</a>|

