# sourceMaterial a ObjectProperty

## Domain

definition: Identification of the material in a collection from which the sample is derived. Entries with more than one sampleSource indicates that the sequence was obtained from a sample that was deposited (by the submitter or a collaborator) in more than one collection.<br>
[Sample](/0.1/Sample)

## Range

[MaterialSource](/0.1/MaterialSource)

## Annotations


