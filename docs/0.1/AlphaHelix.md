# AlphaHelix a skos:Concept, [ProteinStructureType](/0.1/ProteinStructureType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A alpha helix|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001117">http://purl.obolibrary.org/obo/SO_0001117</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q283399">http://www.wikidata.org/entity/Q283399</a>|

