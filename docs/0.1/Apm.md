# Apm a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Apm|
|rdfs:label|2-Aminopimelic acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27094079">http://www.wikidata.org/entity/Q27094079</a>|

