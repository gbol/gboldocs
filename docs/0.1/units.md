# units a ObjectProperty

## Domain

definition: Units used for the mapping e.g. Basepairs, centiMorgan<br>
[QTLMap](/0.1/QTLMap)

## Range

[MapUnits](/0.1/MapUnits)

## Annotations


