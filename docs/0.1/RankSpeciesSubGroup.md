# RankSpeciesSubGroup a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: species sub group|
|parentRank|<a href="http://gbol.life/0.1/RankSpeciesGroup">http://gbol.life/0.1/RankSpeciesGroup</a>|
|rdfs:label|species sub group|

