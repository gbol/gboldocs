# bAib a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|bAib|
|rdfs:label|3-Aminoisobutyric acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2823212">http://www.wikidata.org/entity/Q2823212</a>|

