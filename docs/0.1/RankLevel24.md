# RankLevel24 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 24|
|parentRank|<a href="http://gbol.life/0.1/RankLevel23">http://gbol.life/0.1/RankLevel23</a>|
|rdfs:label|Rank Level 24|

