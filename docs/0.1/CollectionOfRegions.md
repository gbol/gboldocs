# CollectionOfRegions a owl:Class extends [Location](/0.1/Location)

## Subclasses

[ListOfRegions](/0.1/ListOfRegions)

[BagOfRegions](/0.1/BagOfRegions)

## Annotations

|||
|-----|-----|
|rdfs:comment|A region that is separated over several regions. Ordering of the regions is given by using the the rdfs:_n property.|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from Faldo.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#CollectionOfRegions">http://biohackathon.org/resource/faldo#CollectionOfRegions</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[members](/0.1/members)|A set of regions making up a location spreader over multiple regions. The members inherit the reference value from this CollectionOfRegions object.|1:N|[Region](/0.1/Region)|
