# PartGapUnknownLength a owl:Class extends [PartGap](/0.1/PartGap)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|#* The estimated gap length, if not given its unknown and defaults to 100<br>estimatedLength xsd:PositiveInteger?;|
|skos:definition|A gap within the assembly with unknown sequence length|
|rdfs:comment|The sequence is filled with N for the estimated length otherwise a sequence of 100 N's are included|
|subDomain|SequenceFeatureCore|

## Properties

