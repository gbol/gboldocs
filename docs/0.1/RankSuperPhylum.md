# RankSuperPhylum a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: super phylum|
|parentRank|<a href="http://gbol.life/0.1/RankSubKingdom">http://gbol.life/0.1/RankSubKingdom</a>|
|rdfs:label|super phylum|

