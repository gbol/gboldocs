# PCRPrimerSet a owl:Class extends [SequenceSet](/0.1/SequenceSet)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The definition of a primer pair that can be used for a PCR. Multiple forward and/or reverse primers might be defined.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[reversePrimer](/0.1/reversePrimer)|The reverse primer(s)|0:1|[PCRPrimer](/0.1/PCRPrimer)|
|[forwardPrimer](/0.1/forwardPrimer)|The forward primer(s)|1:1|[PCRPrimer](/0.1/PCRPrimer)|
