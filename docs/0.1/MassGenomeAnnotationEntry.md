# MassGenomeAnnotationEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|MGA|
|rdfs:label|Mass Genome Annotation|

