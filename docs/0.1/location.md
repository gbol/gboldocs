# location a ObjectProperty

## Domain

[sapp:Phobius](/sapp/0.1/Phobius)

[Feature](/0.1/Feature)

definition: TMHMM properties<br>
[sapp:TMHMM](/sapp/0.1/TMHMM)

## Range

[Location](/0.1/Location)

## Annotations


