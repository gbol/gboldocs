# ReadInformation a owl:Class

## Subclasses

[Interleaved](/0.1/Interleaved)

[PairedEnd](/0.1/PairedEnd)

[SingleEnd](/0.1/SingleEnd)

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[center](/0.1/center)||0:1|xsd:string|
|[bases](/0.1/bases)||0:1|xsd:Long|
|[file](/0.1/file)||1:N|[LocalSequenceFile](/0.1/LocalSequenceFile)|
|[readLength](/0.1/readLength)||0:1|xsd:Long|
|[machine](/0.1/machine)||0:1|xsd:string|
|[adapter](/0.1/adapter)||0:1|xsd:string|
