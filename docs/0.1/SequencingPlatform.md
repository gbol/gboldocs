# SequencingPlatform a skos:Collection extends owl:Class

## Annotations


## skos:member

[hiseqx](/0.1/hiseqx)

[hiseqx_five](/0.1/hiseqx_five)

[nextseq500](/0.1/nextseq500)

[x454](/0.1/x454)

[SOLID](/0.1/SOLID)

[hiseq](/0.1/hiseq)

[hiseqx_ten](/0.1/hiseqx_ten)

[miseq](/0.1/miseq)

[nextseq](/0.1/nextseq)

[novaseq600](/0.1/novaseq600)

[hiseq4000](/0.1/hiseq4000)

[miseqdx](/0.1/miseqdx)

[hiseq2000](/0.1/hiseq2000)

[hiseq3000](/0.1/hiseq3000)

[illumina](/0.1/illumina)

[nextseq550](/0.1/nextseq550)

[novaseq](/0.1/novaseq)

[Nanopore](/0.1/Nanopore)

[miniseq](/0.1/miniseq)

[miseqfgx](/0.1/miseqfgx)

