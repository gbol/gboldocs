# integratedInto a ObjectProperty

## Domain

definition: If this NA object is integrated into another NA object, for example a phage genome which is integrated into a bacterial genome or a plasmid which is integrated into a yeast genome.<br>
[NASequence](/0.1/NASequence)

## Range

[NASequence](/0.1/NASequence)

## Annotations


