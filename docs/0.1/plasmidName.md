# plasmidName a ObjectProperty

## Domain

definition: The name of the plasmid<br>
[Plasmid](/0.1/Plasmid)

## Range

xsd:string

## Annotations


