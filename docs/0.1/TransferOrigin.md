# TransferOrigin a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Origin of transfer; region of a DNA molecule where transfer is initiated during the process of conjugation or mobilization|
|rdfs:comment|rep_origin should be used for origins of replication; /direction has legal values RIGHT, LEFT and BOTH, however only RIGHT and LEFT are valid when used in conjunction with the oriT feature; origins of transfer can be present in the chromosome;plasmids can contain multiple origins of transfer|
|ddbjLabel|oriT|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|#* The direction of the transfer, can be forward or reverse, which effect the direction of the integrated dna strand.<br>transferDirection type::Direction?;|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000724">http://purl.obolibrary.org/obo/SO_0000724</a>|

## Properties

