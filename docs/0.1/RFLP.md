# RFLP a skos:Concept, [VariationTypes](/0.1/VariationTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Restriction Fragment Length Polymorphism|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000193">http://purl.obolibrary.org/obo/SO_0000193</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q857299">http://www.wikidata.org/entity/Q857299</a>|

