# sex a ObjectProperty

## Domain

definition: The sex of the organism from which the sample was obtained. Sex is used for eukaryotic organisms that undergo meiosis with sexually dimorphic gametes. For prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes please use the mathingType property.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|sex|

