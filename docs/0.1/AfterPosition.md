# AfterPosition a owl:Class extends [FuzzyPosition](/0.1/FuzzyPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A location that is somewhere after the given position.|
|subDomain|LocationCore|
|skos:editorialNote|Missing from the faldo ontology, taken from the genbank format. In Faldo it is supposed to be modeled with a InRange position that has no end attribute. We did not agree with this design in faldo as it does not reflect the semantics.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[position](/0.1/position)||1:1|xsd:Long|
