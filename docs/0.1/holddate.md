# holddate a ObjectProperty

## Domain

definition: The date until which the GBOL dataset should not published (only present on non public entries)<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:dateTime

## Annotations


