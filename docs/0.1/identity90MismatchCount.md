# identity90MismatchCount a ObjectProperty

## Domain

definition: Identity 90 mismatch count<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


