# Minus35Signal a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A conserved hexamer about 35-bp upstream of the start point of bacterial transcription units; consensus=TTGACa or TGTTGACA. This region is associated with sigma factor 70.|
|ddbjLabel|-35_signal|
|ddbjLabel|minus_35_signal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000176">http://purl.obolibrary.org/obo/SO_0000176</a>|

