# ManualAnnotationActivity a owl:Class extends [AnnotationActivity](/0.1/AnnotationActivity)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A manual curation activity|
|rdfs:comment|If the start and end time are not known, both can be set equal to the time at which the annotation is committed to the database.|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[wasAssociatedWith](/0.1/wasAssociatedWith)|The agent which be performed this activity|0:1|[Curator](/0.1/Curator)|
