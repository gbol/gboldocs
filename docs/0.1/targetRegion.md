# targetRegion a ObjectProperty

## Domain

definition: An optional region to indicate to which part it is homologous to<br>
[ProteinHomology](/0.1/ProteinHomology)

## Range

[Region](/0.1/Region)

## Annotations


