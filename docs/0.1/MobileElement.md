# MobileElement a owl:Class extends [RepeatFeature](/0.1/RepeatFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of genome containing mobile elements;|
|ddbjLabel|mobile_element|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001837">http://purl.obolibrary.org/obo/SO_0001837</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[mobileElementName](/0.1/mobileElementName)|The name of the mobile element|1:1|xsd:string|
|[mobileElementType](/0.1/mobileElementType)|Mobile element type|1:1|[MobileElementType](/0.1/MobileElementType)|
