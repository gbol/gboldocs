# Nva a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Nva|
|rdfs:label|Norvaline|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q418707">http://www.wikidata.org/entity/Q418707</a>|

