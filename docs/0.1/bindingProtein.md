# bindingProtein a ObjectProperty

## Domain

definition: If known, a region within another protein that binds this region. If region of other protein is unknown use fuzzy position from begin to end.<br>
[ProteinInteractionSite](/0.1/ProteinInteractionSite)

## Range

[ProteinInteractionSite](/0.1/ProteinInteractionSite)

## Annotations


