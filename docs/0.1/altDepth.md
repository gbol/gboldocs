# altDepth a ObjectProperty

## Domain

definition: Read depth of alternate base<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


