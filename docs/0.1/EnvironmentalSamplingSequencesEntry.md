# EnvironmentalSamplingSequencesEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|ENV|
|rdfs:label|Environmental sampling sequences|

