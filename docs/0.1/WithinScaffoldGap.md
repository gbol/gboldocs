# WithinScaffoldGap a skos:Concept, [GapType](/0.1/GapType)

## Subclasses

[RepeatWithinScaffoldGap](/0.1/RepeatWithinScaffoldGap)

## Annotations

|||
|-----|-----|
|ddbjLabel|within scaffold|

