# proteinId a ObjectProperty

## Domain

definition: The ID of the protein if translated from this CDS (format ^\\s*([A-Z]{3}\\d{5})(\\.)(\\d+)\\s*$)<br>
[CDS](/0.1/CDS)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|protein_id|
|skos:editorialNote|Added to the CDS, proteins can be merged from different species and therefore could have different identifiers when merged|

