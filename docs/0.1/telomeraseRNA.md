# telomeraseRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|RNA component of telomerase, a reverse transcriptase that synthesizes telomeric DNA.|
|ddbjLabel|telomerase_RNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000390">http://purl.obolibrary.org/obo/SO_0000390</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q29764204">http://www.wikidata.org/entity/Q29764204</a>|

