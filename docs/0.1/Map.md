# Map a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|linkage asserted using a non-sequence based map such as RH, linkage, fingerprint or optical.|
|ddbjLabel|map|

