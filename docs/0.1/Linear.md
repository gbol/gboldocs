# Linear a skos:Concept, [Topology](/0.1/Topology)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A linear NA molecule.|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000987">http://purl.obolibrary.org/obo/SO_0000987</a>|

