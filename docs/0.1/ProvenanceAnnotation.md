# ProvenanceAnnotation a owl:Class extends [ProvenanceApplication](/0.1/ProvenanceApplication)

## Subclasses

[sapp:Phobius](/sapp/0.1/Phobius)

[sapp:Priam](/sapp/0.1/Priam)

[sapp:WoLFPSort](/sapp/0.1/WoLFPSort)

[sapp:HMM](/sapp/0.1/HMM)

[sapp:TMHMM](/sapp/0.1/TMHMM)

[sapp:Blast](/sapp/0.1/Blast)

[sapp:InterProScanModels](/sapp/0.1/InterProScanModels)

[sapp:ENZDP](/sapp/0.1/ENZDP)

[sapp:Aragorn](/sapp/0.1/Aragorn)

[sapp:SignalP](/sapp/0.1/SignalP)

[sapp:RNAmmer](/sapp/0.1/RNAmmer)

[sapp:InterProScan](/sapp/0.1/InterProScan)

[sapp:Prodigal](/sapp/0.1/Prodigal)

## Annotations

|||
|-----|-----|
|skos:definition|The element wise annotation of the provenance. This element is typically related to one feature or qualifier, but can be shared among multiple.|
|subDomain|ElementWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[reference](/0.1/reference)|The actual reference of the citation|0:1|[bibo:Document](/ontology/bibo/Document)|
|[derivedFrom](/0.1/derivedFrom)|Optionally give a list of features upon which it is based|0:N|[Feature](/0.1/Feature)|
|[experimentalEvidence](/0.1/experimentalEvidence)|The experimental evidence for the feature annotation (use Evidence Ontology term if possible)|0:1|xsd:string|
|[provenanceNote](/0.1/provenanceNote)|Note on the provenance|0:1|xsd:string|
