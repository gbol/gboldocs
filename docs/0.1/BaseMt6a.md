# BaseMt6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mt6a|
|rdfs:label|N-((9-beta-D-ribofuranosylpurine-6-yl)N-methyl-carbamoyl)threonine|

