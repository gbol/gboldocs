# RankGenus a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: genus|
|parentRank|<a href="http://gbol.life/0.1/RankSubTribe">http://gbol.life/0.1/RankSubTribe</a>|
|rdfs:label|genus|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q34740">http://www.wikidata.org/entity/Q34740</a>|

