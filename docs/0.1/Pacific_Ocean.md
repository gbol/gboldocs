# Pacific_Ocean a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Pacific Ocean|
|rdfs:label|Pacific Ocean|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q98">http://www.wikidata.org/entity/Q98</a>|

