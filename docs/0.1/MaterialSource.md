# MaterialSource a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Describes a sample within a (long term storage) collection.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[sampleId](/0.1/sampleId)|The id of the sample within the given collection|0:1|xsd:string|
|[collectionSampleType](/0.1/collectionSampleType)|Type of the collection sample|1:1|[CollectionSampleType](/0.1/CollectionSampleType)|
|[collectionCode](/0.1/collectionCode)|The collection code of the collection within the given institute|1:1|xsd:string|
|[institutionCode](/0.1/institutionCode)|code of the institution taken from the list available at ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/coll_dump.txt|1:1|xsd:string|
