# size a ObjectProperty

## Domain

definition: Size of the file in bytes, not applicable to output files<br>
[LocalDataFile](/0.1/LocalDataFile)

definition: Size of the file in bytes, not applicable to output files<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

xsd:long

## Annotations


