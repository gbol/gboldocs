# Ocean a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|subDomain|DocumentCore|

## skos:member

[Atlantic_Ocean](/0.1/Atlantic_Ocean)

[Tasman_Sea](/0.1/Tasman_Sea)

[Indian_Ocean](/0.1/Indian_Ocean)

[Southern_Ocean](/0.1/Southern_Ocean)

[Pacific_Ocean](/0.1/Pacific_Ocean)

[North_Sea](/0.1/North_Sea)

[Ross_Sea](/0.1/Ross_Sea)

[Baltic_Sea](/0.1/Baltic_Sea)

[Mediterranean_Sea](/0.1/Mediterranean_Sea)

[Arctic_Ocean](/0.1/Arctic_Ocean)

