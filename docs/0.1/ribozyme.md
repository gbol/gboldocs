# ribozyme a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|ribonucleic acid enzyme, RNA molecule that can catalyze specific biochemical reactions.|
|ddbjLabel|ribozyme|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000374">http://purl.obolibrary.org/obo/SO_0000374</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q205858">http://www.wikidata.org/entity/Q205858</a>|

