# FuzzyPosition a owl:Class extends [Position](/0.1/Position)

## Subclasses

[InRangePosition](/0.1/InRangePosition)

[AfterPosition](/0.1/AfterPosition)

[OneOfPosition](/0.1/OneOfPosition)

[BeforePosition](/0.1/BeforePosition)

## Annotations

|||
|-----|-----|
|rdfs:comment|All Position types that not exactly identify the location|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|LocationCore|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#FuzzyPosition">http://biohackathon.org/resource/faldo#FuzzyPosition</a>|

## Properties

