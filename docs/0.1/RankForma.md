# RankForma a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: forma|
|parentRank|<a href="http://gbol.life/0.1/RankVarietas">http://gbol.life/0.1/RankVarietas</a>|
|rdfs:label|forma|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q279749">http://www.wikidata.org/entity/Q279749</a>|

