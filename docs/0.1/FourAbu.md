# FourAbu a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|FourAbu|
|rdfs:label|4-Aminobutyric acid (piperidinic acid)|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q210021">http://www.wikidata.org/entity/Q210021</a>|

