# RankSubSpecies a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub species|
|parentRank|<a href="http://gbol.life/0.1/RankSpecies">http://gbol.life/0.1/RankSpecies</a>|
|rdfs:label|sub species|

