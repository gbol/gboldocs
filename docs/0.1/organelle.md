# organelle a ObjectProperty

## Domain

definition: Type of membrane-bound intracellular structure from which the sample was obtained. Use a 'cellular component' term from the GO ontology.<br>
[Sample](/0.1/Sample)

## Range

xsd:anyURI

## Annotations

|||
|-----|-----|
|ddbjLabel|organelle|

