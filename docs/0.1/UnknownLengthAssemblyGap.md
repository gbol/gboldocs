# UnknownLengthAssemblyGap a owl:Class extends [AssemblyGap](/0.1/AssemblyGap)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|gap of unknown length in the sequence|
|rdfs:comment|The gap must be denoted with a series of N's in the sequence, if the assembly gap is of unknown length then number of N must be equal to the estimated length. If that is unknown it default to 100 and so should be denoted with 100 N's in the sequence. No upper or lower limit is set on the size of the gap.|
|ddbjLabel|gap|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[estimatedLength](/0.1/estimatedLength)|The estimated gap length, if not given its unknown and defaults to 100|0:1|xsd:Integer|
