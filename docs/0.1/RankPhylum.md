# RankPhylum a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: phylum|
|parentRank|<a href="http://gbol.life/0.1/RankSuperPhylum">http://gbol.life/0.1/RankSuperPhylum</a>|
|rdfs:label|phylum|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q38348">http://www.wikidata.org/entity/Q38348</a>|

