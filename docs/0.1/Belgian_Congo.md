# Belgian_Congo a skos:Concept, [Country](/0.1/Country)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Belgian Congo|
|rdfs:label|Belgian Congo|
|owl:deprecated|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|

