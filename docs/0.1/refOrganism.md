# refOrganism a ObjectProperty

## Domain

definition: Reference Organism<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

[Organism](/0.1/Organism)

## Annotations


