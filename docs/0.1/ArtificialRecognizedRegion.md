# ArtificialRecognizedRegion a owl:Class extends [RecognizedRegion](/0.1/RecognizedRegion)

## Subclasses

[SequenceTaggedSite](/0.1/SequenceTaggedSite)

[PrimerBinding](/0.1/PrimerBinding)

## Annotations

|||
|-----|-----|
|skos:definition|A region on genomic sequence that is recognized by a men made entity or process.|
|subDomain|SequenceFeatureCore|

## Properties

