# spacer a ObjectProperty

## Domain

definition: The regions that contain a spacer<br>
[CRISPRCassette](/0.1/CRISPRCassette)

## Range

[Region](/0.1/Region)

## Annotations


