# Direct a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat where the same sequence is repeated in the same direction. Example: GCTGA-followed by-GCTGA.|
|ddbjLabel|direct|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000314">http://purl.obolibrary.org/obo/SO_0000314</a>|

