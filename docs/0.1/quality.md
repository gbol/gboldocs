# quality a ObjectProperty

## Domain

definition: Quality score of the marker<br>
[Variation](/0.1/Variation)

## Range

xsd:string

## Annotations


