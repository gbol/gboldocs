# TranscriptionalCisRegulatoryRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory region that modulates the transcription of a gene or genes.|
|ddbjLabel|transcriptional_cis_regulatory_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001055">http://purl.obolibrary.org/obo/SO_0001055</a>|

