# ProteinHomology a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Denote a region to be homologous to another entity, which is either another protein or cluster of proteins|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[homologousToDesc](/0.1/homologousToDesc)|An optional description of the element to which it is homologous to|0:1|xsd:string|
|[homologousTo](/0.1/homologousTo)|The entity to which it is homologous to, being either another na sequence or cluster of na sequences|1:1|IRI|
|[targetRegion](/0.1/targetRegion)|An optional region to indicate to which part it is homologous to|0:1|[Region](/0.1/Region)|
