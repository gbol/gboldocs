# RankInfraOrder a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: infra order|
|parentRank|<a href="http://gbol.life/0.1/RankSubOrder">http://gbol.life/0.1/RankSubOrder</a>|
|rdfs:label|infra order|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2889003">http://www.wikidata.org/entity/Q2889003</a>|

