# MeLys a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|MeLys|
|rdfs:label|6-N-Methyllysine|

