# linkedDataBases a ObjectProperty

## Domain

definition: All databases that are linked by any of the contained XRefs<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[Database](/0.1/Database)

## Annotations


