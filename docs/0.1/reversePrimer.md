# reversePrimer a ObjectProperty

## Domain

definition: The reverse primer(s)<br>
[PCRPrimerSet](/0.1/PCRPrimerSet)

## Range

[PCRPrimer](/0.1/PCRPrimer)

## Annotations


