# RankLevel27 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 27|
|parentRank|<a href="http://gbol.life/0.1/RankLevel26">http://gbol.life/0.1/RankLevel26</a>|
|rdfs:label|Rank Level 27|

