# PCRConditions a ObjectProperty

## Domain

definition: A textual description of the PCR conditions needed for this primer<br>
[PrimerBinding](/0.1/PrimerBinding)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|PCR_conditions|
|skos:editorialNote|only present in primerBinding feature|

