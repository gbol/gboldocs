# DoubleStrandedDNA a skos:Concept, [StrandType](/0.1/StrandType)

## Subclasses

[ComplementaryDNA](/0.1/ComplementaryDNA)

## Annotations

|||
|-----|-----|
|skos:definition|A double stranded DNA molecule|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q55978910">http://www.wikidata.org/entity/Q55978910</a>|

