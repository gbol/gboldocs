# AnnotationLinkSet a owl:Class extends [AnnotationResult](/0.1/AnnotationResult), [GBOLLinkSet](/0.1/GBOLLinkSet)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|An annotation result that exist out of a set of XRefs.|
|subDomain|DocumentWiseProv|
|rdfs:label|AnnotationLinkSet|

## Properties

