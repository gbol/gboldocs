# MeVal a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|MeVal|
|rdfs:label|N-Methylvaline|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133228">http://www.wikidata.org/entity/Q27133228</a>|

