# Promoter a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|a region of DNA that initiates transcription of a particular gene|
|ddbjLabel|promoter|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000167">http://purl.obolibrary.org/obo/SO_0000167</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q224093">http://www.wikidata.org/entity/Q224093</a>|

