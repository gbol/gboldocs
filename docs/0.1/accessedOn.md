# accessedOn a ObjectProperty

## Domain

definition: The time point at which the file was downloaded<br>
[PublicDataFile](/0.1/PublicDataFile)

## Range

xsd:dateTime

## Annotations


