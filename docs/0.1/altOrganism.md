# altOrganism a ObjectProperty

## Domain

definition: Alternative Organism<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

[Organism](/0.1/Organism)

## Annotations


