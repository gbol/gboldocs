# ResponseElement a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory element that acts in response to a stimulus, usually via transcription factor binding.|
|ddbjLabel|response_element|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002205">http://purl.obolibrary.org/obo/SO_0002205</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7315962">http://www.wikidata.org/entity/Q7315962</a>|

