# altitude a ObjectProperty

## Domain

definition: Altitude of the location from which the sample was collected in meters above or below nominal sea level<br>
[Sample](/0.1/Sample)

## Range

xsd:double

## Annotations

|||
|-----|-----|
|ddbjLabel|altitude|

