# CultureCollection a skos:Concept, [CollectionSampleType](/0.1/CollectionSampleType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A live microbial or viral culture, or cell lines that have been  deposited in curated culture collections. Microbial samples in personal or laboratory collections should be annotated with the strain property. It is implied that the sequence was obtained from a sample retrieved (by the submitter or a collaborator) from the indicated culture collection, or that the sequence was obtained from a sample that was deposited (by the submitter or a collaborator) in the given culture collection.|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q64062850">http://www.wikidata.org/entity/Q64062850</a>|

