# EntryType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The GBOL document entry types, based on the genbank division types.|
|subDomain|DocumentCore|

## skos:member

[HighLevelAssemblyInformation](/0.1/HighLevelAssemblyInformation)

[MassGenomeAnnotationEntry](/0.1/MassGenomeAnnotationEntry)

[GenomeSurveySequenceEntry](/0.1/GenomeSurveySequenceEntry)

[StandardEntry](/0.1/StandardEntry)

[SequenceTaggedSiteEntry](/0.1/SequenceTaggedSiteEntry)

[HighThoughputCDNASequencingEntry](/0.1/HighThoughputCDNASequencingEntry)

[WholeGenomeShotgunEntry](/0.1/WholeGenomeShotgunEntry)

[TranscriptomeShotgunAssemblyEntry](/0.1/TranscriptomeShotgunAssemblyEntry)

[EnvironmentalSamplingSequencesEntry](/0.1/EnvironmentalSamplingSequencesEntry)

[ExpressedSequenceTagEntry](/0.1/ExpressedSequenceTagEntry)

[HighThoughputGenomeSequencingEntry](/0.1/HighThoughputGenomeSequencingEntry)

[PatentEntry](/0.1/PatentEntry)

[ThirdPArtySequenceDataRecord](/0.1/ThirdPArtySequenceDataRecord)

