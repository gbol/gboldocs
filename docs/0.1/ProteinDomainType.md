# ProteinDomainType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|A list of types for a pattern such as a hidden markov model.|
|subDomain|SequenceFeatureCore|

## skos:member

[Family](/0.1/Family)

[Domain](/0.1/Domain)

