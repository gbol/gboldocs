# releaseDate a ObjectProperty

## Domain

definition: The release date of the database<br>
[Database](/0.1/Database)

## Range

xsd:dateTime

## Annotations


