# TaxonAssignmentType a skos:Collection extends owl:Class

## Annotations


## skos:member

[HitsTaxon](/0.1/HitsTaxon)

[AssignedTaxon](/0.1/AssignedTaxon)

[BestTaxon](/0.1/BestTaxon)

