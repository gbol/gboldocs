# collectionCode a ObjectProperty

## Domain

definition: The collection code of the collection within the given institute<br>
[MaterialSource](/0.1/MaterialSource)

## Range

xsd:string

## Annotations


