# ProteinQTL a skos:Concept, [QTLTypes](/0.1/QTLTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|QTL type based on protein data|

