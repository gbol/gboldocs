# MatrixAttachmentRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A DNA region that is required for the binding of chromatin to the nuclear matrix.|
|ddbjLabel|matrix_attachment_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000036">http://purl.obolibrary.org/obo/SO_0000036</a>|

