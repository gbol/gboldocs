# RankSubPhylum a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub phylum|
|parentRank|<a href="http://gbol.life/0.1/RankPhylum">http://gbol.life/0.1/RankPhylum</a>|
|rdfs:label|sub phylum|

