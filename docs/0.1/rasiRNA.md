# rasiRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small interfering RNA of length between 17 and 28 nucleotides, derived from the transcript of a repetitive element.|
|ddbjLabel|rasiRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000454">http://purl.obolibrary.org/obo/SO_0000454</a>|

