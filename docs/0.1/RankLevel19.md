# RankLevel19 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 19|
|parentRank|<a href="http://gbol.life/0.1/RankLevel18">http://gbol.life/0.1/RankLevel18</a>|
|rdfs:label|Rank Level 19|

