# BiologicalRecognizedRegion a owl:Class extends [RecognizedRegion](/0.1/RecognizedRegion)

## Subclasses

[ProteinBinding](/0.1/ProteinBinding)

[MiscRecomb](/0.1/MiscRecomb)

[TransferOrigin](/0.1/TransferOrigin)

[ReplicationOrigin](/0.1/ReplicationOrigin)

[MiscBinding](/0.1/MiscBinding)

[DLoop](/0.1/DLoop)

[RegulationSite](/0.1/RegulationSite)

## Annotations

|||
|-----|-----|
|skos:definition|A region on genomic sequence that is recognized by a biological entity or process.|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[boundMoiety](/0.1/boundMoiety)|Textual description of the moiety recognizing the region|0:1|xsd:string|
