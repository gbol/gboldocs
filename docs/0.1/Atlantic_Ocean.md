# Atlantic_Ocean a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Atlantic Ocean|
|rdfs:label|Atlantic Ocean|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q97">http://www.wikidata.org/entity/Q97</a>|

