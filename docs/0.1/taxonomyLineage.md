# taxonomyLineage a ObjectProperty

## Domain

definition: The lineage of the organism from which the sequence originates, should match to the lineage of the referenced taxonomy<br>
[Organism](/0.1/Organism)

## Range

[Taxon](/0.1/Taxon)

## Annotations


