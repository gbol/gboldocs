# ExpectedCounts a skos:Concept, [ExpressionType](/0.1/ExpressionType) extends [Counts](/0.1/Counts)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|'expected_count' is the sum of the posterior probability of each read comes from this transcript over all reads. Because 1) each read aligning to this transcript has a probability of being generated from background noise; 2) RSEM may filter some alignable low quality reads, the sum of expected counts for all transcript are generally less than the total number of reads aligned.|

