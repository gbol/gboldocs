# Intron a owl:Class extends [TranscriptionElement](/0.1/TranscriptionElement)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|a segment of DNA that is transcribed, but removed from within the transcript by splicing together the sequences(exons) on either side of it;|
|ddbjLabel|iDNA|
|ddbjLabel|intron|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000188">http://purl.obolibrary.org/obo/SO_0000188</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q207551">http://www.wikidata.org/entity/Q207551</a>|

## Properties

