# sequencePart a ObjectProperty

## Domain

definition: Sorted list of parts that upon concatenation forms the complete sequence.<br>
[SequenceAssembly](/0.1/SequenceAssembly)

## Range

[SequencePart](/0.1/SequencePart)

## Annotations


