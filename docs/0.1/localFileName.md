# localFileName a ObjectProperty

## Domain

definition: The local file name<br>
[LocalDataFile](/0.1/LocalDataFile)

## Range

xsd:string

## Annotations


