# anticodon a ObjectProperty

## Domain

definition: The anti codon of the TRNA<br>
[tRNA](/0.1/tRNA)

## Range

[AntiCodon](/0.1/AntiCodon)

## Annotations

|||
|-----|-----|
|ddbjLabel|anticodon|
|skos:editorialNote|linked to tRNA which points to a AntiCodon object|

