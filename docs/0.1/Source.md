# Source a owl:Class extends [NAFeature](/0.1/NAFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Indicate other sample source of the sequence then associated to the sequence object.|
|rdfs:comment|Transgenic sequences can have parts that have another source.|
|ddbjLabel|source|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|transgenic sequences must have at least two source featurekeys; in a transgenic sequence the source featureKeydescribing the organism that is the recipient of the DNAmust span the entire sequence;see Appendix III /organelle for a list of <organelle_value>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[sample](/0.1/sample)|Sample from which this piece of sequence originates from.|0:1|[Sample](/0.1/Sample)|
|[organism](/0.1/organism)|The organism from which the sequence originates from|0:1|[Organism](/0.1/Organism)|
