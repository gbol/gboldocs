# subStrain a ObjectProperty

## Domain

definition: The name or identifier of a genetically or otherwise modified strain from which the sample was obtained (using PCR). The parental strain from which it is derived should be annotated with the strain property.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|sub_strain|

