# BaseUm a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|um|
|rdfs:label|2'-O-methyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632791">http://www.wikidata.org/entity/Q15632791</a>|

