# ImmunoglobulinRegionType a skos:Collection extends owl:Class

## Annotations


## skos:member

[ConstantRegion](/0.1/ConstantRegion)

[SwitchRegion](/0.1/SwitchRegion)

[GapRegion](/0.1/GapRegion)

[DiversitySegment](/0.1/DiversitySegment)

[JoiningSegment](/0.1/JoiningSegment)

[VariableRegion](/0.1/VariableRegion)

[VariableSegment](/0.1/VariableSegment)

