# TemporaryFile a owl:Class extends [prov:Entity](/ns/prov/Entity)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A temporary file, which is derived from another prov:Entity|
|rdfs:comment|Use this class to describe temporary files that contain only information from another prov:Entity, but is formatted differently. Temporary files are typically generated to create files with the correct format for the different tools and output of these tools that need to be converted back.|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[prov:wasDerivedFrom](/ns/prov/wasDerivedFrom)|Any entries used as input in the activity that created this entity|1:1|[prov:Entity](/ns/prov/Entity)|
