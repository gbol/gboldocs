# ThreePrimeUTR a owl:Class extends [TranscriptFeature](/0.1/TranscriptFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region at the 3' end of a mature transcript (following the stop codon) that is not translated into a protein|
|ddbjLabel|3'UTR|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000205">http://purl.obolibrary.org/obo/SO_0000205</a>|

## Properties

