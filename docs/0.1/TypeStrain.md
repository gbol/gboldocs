# TypeStrain a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|the living or permanently preserved culture which is the nomenclatural type of a species or infraspecific taxon|
|ddbjLabel|type strain|
|rdfs:label|type strain|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q64149240">http://www.wikidata.org/entity/Q64149240</a>|

