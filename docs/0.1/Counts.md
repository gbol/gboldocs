# Counts a skos:Concept, [ExpressionType](/0.1/ExpressionType)

## Subclasses

[EstimatedCounts](/0.1/EstimatedCounts)

[ExpectedCounts](/0.1/ExpectedCounts)

[RawCounts](/0.1/RawCounts)

## Annotations


