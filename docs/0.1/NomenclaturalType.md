# NomenclaturalType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|List of nomenclature types used to annotate a taxonomic entry.|
|subDomain|DocumentCore|

## skos:member

[Paralectotype](/0.1/Paralectotype)

[ReferenceStrain](/0.1/ReferenceStrain)

[Paratype](/0.1/Paratype)

[Syntype](/0.1/Syntype)

[Neotype](/0.1/Neotype)

[ExType](/0.1/ExType)

[Isosyntype](/0.1/Isosyntype)

[Allotype](/0.1/Allotype)

[Lectotype](/0.1/Lectotype)

[TypeStrain](/0.1/TypeStrain)

[Isotype](/0.1/Isotype)

[Hapanotype](/0.1/Hapanotype)

[Epitype](/0.1/Epitype)

[Holotype](/0.1/Holotype)

[TypeMaterial](/0.1/TypeMaterial)

[NeotypeStrain](/0.1/NeotypeStrain)

