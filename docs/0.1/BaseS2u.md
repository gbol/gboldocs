# BaseS2u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|s2u|
|rdfs:label|2-thiouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q20890502">http://www.wikidata.org/entity/Q20890502</a>|

