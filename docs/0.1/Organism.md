# Organism a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The living envelope in which all SequenceObject are enclosed.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[taxonomyLineage](/0.1/taxonomyLineage)|The lineage of the organism from which the sequence originates, should match to the lineage of the referenced taxonomy|0:N|[Taxon](/0.1/Taxon)|
|[taxonomy](/0.1/taxonomy)|The taxonomy of the organism from which the sequence originates|0:1|[TaxonomyRef](/0.1/TaxonomyRef)|
|[scientificName](/0.1/scientificName)|The taxonomic name of the organism from which the sequence originates, should match to the name of the referenced taxonomy|0:1|xsd:string|
