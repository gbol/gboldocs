# PartXRefSequence a owl:Class extends [PartSequence](/0.1/PartSequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A part of the sequence included from another sequence that is included from an externally referenced resource.|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[includedXRefVersion](/0.1/includedXRefVersion)|The version of the sequence to include, should be specified if referenced resoure can have multiple versions. If not specified it referes to the first version.|0:1|xsd:string|
|[includedXRefSequence](/0.1/includedXRefSequence)|The xref from which the sequence is included|1:1|[XRef](/0.1/XRef)|
