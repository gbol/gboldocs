# readDepth a ObjectProperty

## Domain

definition: Read depth<br>
[Variation](/0.1/Variation)

definition: Read depth<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


