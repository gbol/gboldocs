# Neotype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|neotype|
|rdfs:label|neotype|

