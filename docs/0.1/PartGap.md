# PartGap a owl:Class extends [SequencePart](/0.1/SequencePart)

## Subclasses

[PartGapKnownLength](/0.1/PartGapKnownLength)

[PartGapUnknownLength](/0.1/PartGapUnknownLength)

## Annotations

|||
|-----|-----|
|skos:definition|A gap within the assembly|
|rdfs:comment|The sequence within a gap is equal to N|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

