# RepeatRegion a owl:Class extends [RepeatFeature](/0.1/RepeatFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of genome containing repeating units;|
|ddbjLabel|repeat_region|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000657">http://purl.obolibrary.org/obo/SO_0000657</a>|

## Properties

