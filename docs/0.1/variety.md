# variety a ObjectProperty

## Domain

definition: The variety of the host from which the sample was derived. Use cultivar if the host is a cultivated plant.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|variety|

