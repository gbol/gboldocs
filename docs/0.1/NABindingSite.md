# NABindingSite a owl:Class extends [BindingSite](/0.1/BindingSite)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|#* If known specify the Protein binding region on the dna or rna<br>bindingRegion ProteinBinding?;<br>#* If unknown give a short description of the region to which this side within the protein binds to. <br>bindingRegionDesc xsd:string?;|
|skos:definition|Region that bind to a dna or rna molecule.|
|subDomain|SequenceFeatureCore|

## Properties

