# BaseCmnm5s2u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|cmnm5s2u|
|rdfs:label|5-carboxymethylaminomethyl-2-thiouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133223">http://www.wikidata.org/entity/Q27133223</a>|

