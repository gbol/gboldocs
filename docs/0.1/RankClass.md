# RankClass a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: class|
|parentRank|<a href="http://gbol.life/0.1/RankSuperClass">http://gbol.life/0.1/RankSuperClass</a>|
|rdfs:label|class|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q37517">http://www.wikidata.org/entity/Q37517</a>|

