# Centromere a owl:Class extends [RepeatFeature](/0.1/RepeatFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of biological interest identified as a centromere and which has been experimentally characterized;|
|rdfs:comment|The centromere feature describes the interval of DNA that corresponds to a region where chromatids are held and a kinetochore is formed|
|ddbjLabel|centromere|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000577">http://purl.obolibrary.org/obo/SO_0000577</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q14762596">http://www.wikidata.org/entity/Q14762596</a>|

## Properties

