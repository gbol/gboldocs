# Base a owl:Class extends [Location](/0.1/Location)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Identifies a single base.|
|rdfs:comment|One-based inclusive same as in GenBank and Faldo|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from the GenBank format and associated ENA tools.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[at](/0.1/at)|The position of the base|1:1|[Position](/0.1/Position)|
|[strand](/0.1/strand)|The strand on which it is positioned|0:1|[StrandPosition](/0.1/StrandPosition)|
