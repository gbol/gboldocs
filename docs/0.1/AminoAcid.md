# AminoAcid a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The set of all possible amino acids|
|subDomain|SequenceFeatureCore|

## skos:member

[Asn](/0.1/Asn)

[Hyl](/0.1/Hyl)

[Asp](/0.1/Asp)

[Thr](/0.1/Thr)

[AminoAcidOther](/0.1/AminoAcidOther)

[Pyl](/0.1/Pyl)

[Asx](/0.1/Asx)

[Lys](/0.1/Lys)

[Xle](/0.1/Xle)

[Orn](/0.1/Orn)

[Phe](/0.1/Phe)

[Abu](/0.1/Abu)

[Ile](/0.1/Ile)

[bAla](/0.1/bAla)

[Val](/0.1/Val)

[Leu](/0.1/Leu)

[Ahe](/0.1/Ahe)

[Apm](/0.1/Apm)

[MeGly](/0.1/MeGly)

[Ide](/0.1/Ide)

[Gln](/0.1/Gln)

[EtAsn](/0.1/EtAsn)

[ThreeHyp](/0.1/ThreeHyp)

[Des](/0.1/Des)

[His](/0.1/His)

[Acp](/0.1/Acp)

[Tyr](/0.1/Tyr)

[Xaa](/0.1/Xaa)

[Nva](/0.1/Nva)

[Ala](/0.1/Ala)

[TERM](/0.1/TERM)

[MeIle](/0.1/MeIle)

[Cys](/0.1/Cys)

[MeVal](/0.1/MeVal)

[Aad](/0.1/Aad)

[Glu](/0.1/Glu)

[Trp](/0.1/Trp)

[EtGly](/0.1/EtGly)

[Pro](/0.1/Pro)

[aHyl](/0.1/aHyl)

[Dbu](/0.1/Dbu)

[Aib](/0.1/Aib)

[MeLys](/0.1/MeLys)

[Gly](/0.1/Gly)

[Glx](/0.1/Glx)

[Ser](/0.1/Ser)

[Dpm](/0.1/Dpm)

[Met](/0.1/Met)

[Nle](/0.1/Nle)

[Dpr](/0.1/Dpr)

[bAad](/0.1/bAad)

[FourAbu](/0.1/FourAbu)

[aIle](/0.1/aIle)

[Sec](/0.1/Sec)

[FourHyp](/0.1/FourHyp)

[Arg](/0.1/Arg)

[bAib](/0.1/bAib)

