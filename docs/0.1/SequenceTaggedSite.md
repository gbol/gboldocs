# SequenceTaggedSite a owl:Class extends [ArtificialRecognizedRegion](/0.1/ArtificialRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Sequence tagged site; short, single-copy DNA sequence that characterizes a mapping landmark on the genome and can be detected by PCR; a region of the genome can be mapped by determining the order of a series of STS;|
|rdfs:comment|STS location to include primer(s) in primer_bind key or primers.|
|ddbjLabel|STS|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000331">http://purl.obolibrary.org/obo/SO_0000331</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2068713">http://www.wikidata.org/entity/Q2068713</a>|

## Properties

