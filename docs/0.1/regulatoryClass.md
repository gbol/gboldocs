# regulatoryClass a ObjectProperty

## Domain

definition: The type of regulation<br>
[RegulationSite](/0.1/RegulationSite)

## Range

[RegulatoryClass](/0.1/RegulatoryClass)

## Annotations

|||
|-----|-----|
|ddbjLabel|regulatory_class|
|skos:editorialNote|Add to RegulationSite class, enumeration from the enumeration in genbank created|

