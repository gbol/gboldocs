# percIdentity a ObjectProperty

## Domain

definition: The percentage identity of a similarity analysis<br>
[sapp:Blast](/sapp/0.1/Blast)

## Range

xsd:float

## Annotations


