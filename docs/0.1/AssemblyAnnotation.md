# AssemblyAnnotation a owl:Class extends [SequenceAnnotation](/0.1/SequenceAnnotation)

## Subclasses

[AssemblyGap](/0.1/AssemblyGap)

[UnsureBases](/0.1/UnsureBases)

## Annotations

|||
|-----|-----|
|skos:definition|Annotation on the assembly of the sequence|
|subDomain|SequenceFeatureCore|

## Properties

