# version a ObjectProperty

## Domain

definition: The version of the resource<br>
[Database](/0.1/Database)

definition: The version of the resource<br>
[AnnotationSoftware](/0.1/AnnotationSoftware)

definition: The version of the resource<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:string

## Annotations


