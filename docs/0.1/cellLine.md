# cellLine a ObjectProperty

## Domain

definition: The cell line from which the sequence was obtained, please use term from 'Cell Line Ontology' (http://www.clo-ontology.org/) where possible<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|cell_line|

