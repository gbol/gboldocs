# residue a ObjectProperty

## Domain

definition: The residue type as defined within the 'Protein Modification Ontology' (http://www.psidev.info/MOD)<br>
[ModifiedResidue](/0.1/ModifiedResidue)

## Range

xsd:anyURI

## Annotations


