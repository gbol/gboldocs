# mapAccession a ObjectProperty

## Domain

definition: Unique map accession Identifier<br>
[QTLMap](/0.1/QTLMap)

## Range

xsd:string

## Annotations


