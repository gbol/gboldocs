# BaseM5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m5u|
|rdfs:label|5-methyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q425078">http://www.wikidata.org/entity/Q425078</a>|

