# Terminal a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat at the ends of and within the sequence for which it has functional significance other than long terminal repeat.|
|ddbjLabel|terminal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002201">http://purl.obolibrary.org/obo/SO_0002201</a>|

