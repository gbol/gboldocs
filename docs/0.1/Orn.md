# Orn a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Orn|
|rdfs:label|Ornithine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27077099">http://www.wikidata.org/entity/Q27077099</a>|

