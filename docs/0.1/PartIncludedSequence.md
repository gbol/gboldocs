# PartIncludedSequence a owl:Class extends [PartSequence](/0.1/PartSequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A part of the sequence included from another sequence that is included within the same GBOL document.|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[includedSequence](/0.1/includedSequence)|Reference to the sequence that is included|1:1|[UncompleteNASequence](/0.1/UncompleteNASequence)|
