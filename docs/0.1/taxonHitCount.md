# taxonHitCount a ObjectProperty

## Domain

definition: Taxon hit count<br>
[ASVSet](/0.1/ASVSet)

## Range

xsd:integer

## Annotations


