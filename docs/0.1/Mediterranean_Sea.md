# Mediterranean_Sea a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Mediterranean Sea|
|rdfs:label|Mediterranean Sea|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4918">http://www.wikidata.org/entity/Q4918</a>|

