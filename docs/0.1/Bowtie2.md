# Bowtie2 a owl:Class extends [ProvenanceMapping](/0.1/ProvenanceMapping)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[SQ](/0.1/SQ)||1:1|xsd:string|
|[HD](/0.1/HD)||1:1|xsd:string|
|[CO](/0.1/CO)||1:1|xsd:string|
|[PG](/0.1/PG)||1:1|xsd:string|
