# WithinClone a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|sequence on both sides of the gap is derived from the same clone, but the gap is not spanned by paired-ends. The adjacent sequence contigs have unknown order and orientation.|
|ddbjLabel|within_clone|

