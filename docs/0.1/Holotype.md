# Holotype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|single physical example (or illustration) of an organism, known to have been used when the species (or lower-ranked taxon) was formally described|
|ddbjLabel|holotype|
|rdfs:label|holotype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1061403">http://www.wikidata.org/entity/Q1061403</a>|

