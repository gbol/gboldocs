# IntraMembraneRegion a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region in-between two membranes|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[nTerminalMembrane](/0.1/nTerminalMembrane)|Name of the membrane on the n terminal side|0:1|xsd:string|
|[cTerminalMembrane](/0.1/cTerminalMembrane)|Name of the membrane on the c terminal side|0:1|xsd:string|
