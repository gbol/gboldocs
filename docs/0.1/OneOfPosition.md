# OneOfPosition a owl:Class extends [FuzzyPosition](/0.1/FuzzyPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|LocationCore|
|skos:editorialNote|A position that can be either at one of the given positions. We currently have no support to include InRange, Before or After positions. If needed please notify us.|
|skos:editorialNote|Taken over from Faldo ontology, although Faldo ontology does allow for inclusion of regions. This causes the possibility of an ill defined loop as region can include OneOfPosition again, which has no semantical meaning.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#OneOfPosition">http://biohackathon.org/resource/faldo#OneOfPosition</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[position](/0.1/position)||1:N|xsd:Long|
