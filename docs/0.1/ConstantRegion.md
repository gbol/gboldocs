# ConstantRegion a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|constant region of immunoglobulin light and heavy chains, and T-cell receptor alpha, beta, and gamma chains; includes one or more exons depending on the particular chain|
|ddbjLabel|C_region|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|if any qualifier specified, do not add qualifier put check if qualifier is on transcript or Gene. Possible qualifiers /allele=\"text\"n/citation=[number]n/db_xref=\"<database>:<identifier>\"n/gene=\"text\"n/gene_synonym=\"text\"n/locus_tag=\"text\" (single token)n/map=\"text\"n/note=\"text\"n/old_locus_tag=\"text\" (single token)n/product=\"text\"n/pseudon/pseudogene=\"TYPE\"n/standard_name=\"text\"|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001834">http://purl.obolibrary.org/obo/SO_0001834</a>|

