# Des a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Des|
|rdfs:label|Desmosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q61184">http://www.wikidata.org/entity/Q61184</a>|

