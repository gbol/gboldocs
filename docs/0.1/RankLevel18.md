# RankLevel18 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 18|
|parentRank|<a href="http://gbol.life/0.1/RankLevel17">http://gbol.life/0.1/RankLevel17</a>|
|rdfs:label|Rank Level 18|

