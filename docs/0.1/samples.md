# samples a ObjectProperty

## Domain

definition: All samples from which the sequence objects are collected<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[Sample](/0.1/Sample)

## Annotations


