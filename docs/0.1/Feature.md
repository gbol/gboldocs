# Feature a owl:Class

## Subclasses

[GeneralFeature](/0.1/GeneralFeature)

[ProteinFeature](/0.1/ProteinFeature)

[QTL](/0.1/QTL)

[NAFeature](/0.1/NAFeature)

## Annotations

|||
|-----|-----|
|skos:definition|Adds a functional description to a range or multiple ranges of a biological sequence.|
|gen:framing|:location<br>:provenance|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000110">http://purl.obolibrary.org/obo/SO_0000110</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[location](/0.1/location)||1:1|[Location](/0.1/Location)|
|[citation](/0.1/citation)|Any publication associated to feature annotation|0:N|[Citation](/0.1/Citation)|
|[function](/0.1/function)|Short description of the function|0:1|xsd:string|
|[phenotype](/0.1/phenotype)|Associated phenotypic property|0:1|xsd:string|
|[provenance](/0.1/provenance)|The provenance of the feature|1:N|[FeatureProvenance](/0.1/FeatureProvenance)|
|[artificialLocation](/0.1/artificialLocation)|Indicate that the location is artificial because of a heterogeneous population for low quality sequence region|0:1|[ReasonArtificialLocation](/0.1/ReasonArtificialLocation)|
|[accession](/0.1/accession)|The accession id of the entry referenced|0:N|xsd:string|
|[standardName](/0.1/standardName)|The standard name of the feature|0:1|xsd:string|
|[xref](/0.1/xref)|Reference to other entries holding entries with related information in other databases|0:N|[XRef](/0.1/XRef)|
|[note](/0.1/note)|A set off some additional notes|0:N|[Note](/0.1/Note)|
