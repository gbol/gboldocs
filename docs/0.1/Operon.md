# Operon a owl:Class extends [TranscriptionElement](/0.1/TranscriptionElement)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region containing polycistronic transcript including a cluster of genes that are under the control of the same regulatory sequences/promotor and in the same biological pathway|
|ddbjLabel|operon|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000178">http://purl.obolibrary.org/obo/SO_0000178</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q139677">http://www.wikidata.org/entity/Q139677</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[operonId](/0.1/operonId)|Tag identifier of the operon|0:1|xsd:string|
