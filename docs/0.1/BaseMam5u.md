# BaseMam5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mam5u|
|rdfs:label|5-methylaminomethyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109321">http://www.wikidata.org/entity/Q27109321</a>|

