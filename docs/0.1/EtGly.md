# EtGly a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|EtGly|
|rdfs:label|N-Ethylglycine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27453041">http://www.wikidata.org/entity/Q27453041</a>|

