# BaseM7g a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m7g|
|rdfs:label|7-methylguanosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4642879">http://www.wikidata.org/entity/Q4642879</a>|

