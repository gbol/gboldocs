# Retrotransposon a skos:Concept, [MobileElementType](/0.1/MobileElementType) extends [Transposon](/0.1/Transposon)

## Subclasses

[NonLTRRetrotransposon](/0.1/NonLTRRetrotransposon)

## Annotations

|||
|-----|-----|
|ddbjLabel|retro transposon|
|rdfs:label|retro transposon|

