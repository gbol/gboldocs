# Arg a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|R|
|ddbjLabel|Arg|
|rdfs:label|Arginine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001451">http://purl.obolibrary.org/obo/SO_0001451</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q173670">http://www.wikidata.org/entity/Q173670</a>|

