# retrievedFrom a ObjectProperty

## Domain

definition: Optional an URL it was downloaded from<br>
[LocalDataFile](/0.1/LocalDataFile)

definition: Optional an URL it was downloaded from<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

xsd:anyURI

## Annotations


