# subClone a ObjectProperty

## Domain

definition: The name or identifier of the sub-clone from which the sample was obtained (using PCR).<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|sub_clone|

