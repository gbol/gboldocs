# target a ObjectProperty

## Domain

definition: The set of database that are target of the references associated to this link set<br>
[GBOLLinkSet](/0.1/GBOLLinkSet)

## Range

[Database](/0.1/Database)

## Annotations


