# bitscore a ObjectProperty

## Domain

definition: The bitscore of an alignment<br>
[sapp:Blast](/sapp/0.1/Blast)

## Range

xsd:float

## Annotations


