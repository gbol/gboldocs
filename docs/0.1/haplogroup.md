# haplogroup a ObjectProperty

## Domain

definition: For the host from which the sample was obtained the name for a group of similar haplotypes that share some sequence variation. Haplogroups are often used to track migration of population groups.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|haplogroup|

