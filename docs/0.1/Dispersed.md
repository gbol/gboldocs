# Dispersed a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat that is located at dispersed sites in the genome.|
|ddbjLabel|dispersed|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000658">http://purl.obolibrary.org/obo/SO_0000658</a>|

