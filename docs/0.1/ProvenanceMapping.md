# ProvenanceMapping a owl:Class extends [ProvenanceApplication](/0.1/ProvenanceApplication)

## Subclasses

[TopHat](/0.1/TopHat)

[Bowtie2](/0.1/Bowtie2)

[Kallisto](/0.1/Kallisto)

[Star](/0.1/Star)

[Cufflinks](/0.1/Cufflinks)

## Annotations

|||
|-----|-----|
|rdfs:comment|Mapping tools provenance information|
|subDomain|ElementWiseProv|

## Properties

