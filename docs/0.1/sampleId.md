# sampleId a ObjectProperty

## Domain

definition: The id of the sample within the given collection<br>
[MaterialSource](/0.1/MaterialSource)

## Range

xsd:string

## Annotations


