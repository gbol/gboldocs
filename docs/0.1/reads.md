# reads a ObjectProperty

## Domain

definition: Number of reads in the file<br>
[LocalSequenceFile](/0.1/LocalSequenceFile)

definition: Number of reads in the file<br>
[RemoteSequenceFile](/0.1/RemoteSequenceFile)

## Range

xsd:long

## Annotations


