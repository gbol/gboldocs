# Rank a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The rank of a taxonomy|
|rdfs:comment|The rank can either be defined rank, or can be an undefined rank. If undefined please use the 'Level' enumeration values. Level 1 is the highest level, which is most likely to be equal to 'Kingdom'.|
|subDomain|DocumentCore|

## skos:member

[RankSuperClass](/0.1/RankSuperClass)

[RankLevel28](/0.1/RankLevel28)

[RankLevel27](/0.1/RankLevel27)

[RankTribe](/0.1/RankTribe)

[RankLevel26](/0.1/RankLevel26)

[RankLevel25](/0.1/RankLevel25)

[RankKingdom](/0.1/RankKingdom)

[RankLevel20](/0.1/RankLevel20)

[RankLevel24](/0.1/RankLevel24)

[RankLevel23](/0.1/RankLevel23)

[RankLevel22](/0.1/RankLevel22)

[RankLevel21](/0.1/RankLevel21)

[RankOrder](/0.1/RankOrder)

[RankSubKingdom](/0.1/RankSubKingdom)

[RankSuperOrder](/0.1/RankSuperOrder)

[RankSpeciesSubGroup](/0.1/RankSpeciesSubGroup)

[RankSubSpecies](/0.1/RankSubSpecies)

[RankSubClass](/0.1/RankSubClass)

[RankSpecies](/0.1/RankSpecies)

[RankLevel9](/0.1/RankLevel9)

[RankLevel6](/0.1/RankLevel6)

[RankSubOrder](/0.1/RankSubOrder)

[RankLevel5](/0.1/RankLevel5)

[RankLevel8](/0.1/RankLevel8)

[RankVarietas](/0.1/RankVarietas)

[RankLevel7](/0.1/RankLevel7)

[RankLevel2](/0.1/RankLevel2)

[RankLevel1](/0.1/RankLevel1)

[RankLevel4](/0.1/RankLevel4)

[RankClass](/0.1/RankClass)

[RankParvOrder](/0.1/RankParvOrder)

[RankLevel3](/0.1/RankLevel3)

[RankForma](/0.1/RankForma)

[RankInfraOrder](/0.1/RankInfraOrder)

[RankSuperPhylum](/0.1/RankSuperPhylum)

[RankSpeciesGroup](/0.1/RankSpeciesGroup)

[RankSubTribe](/0.1/RankSubTribe)

[RankInfraClass](/0.1/RankInfraClass)

[RankFamily](/0.1/RankFamily)

[RankLevel17](/0.1/RankLevel17)

[RankLevel16](/0.1/RankLevel16)

[RankLevel15](/0.1/RankLevel15)

[RankLevel14](/0.1/RankLevel14)

[RankSuperFamily](/0.1/RankSuperFamily)

[RankSubGenus](/0.1/RankSubGenus)

[RankLevel19](/0.1/RankLevel19)

[RankLevel18](/0.1/RankLevel18)

[RankSubFamily](/0.1/RankSubFamily)

[RankLevel13](/0.1/RankLevel13)

[RankLevel12](/0.1/RankLevel12)

[RankLevel11](/0.1/RankLevel11)

[RankPhylum](/0.1/RankPhylum)

[RankGenus](/0.1/RankGenus)

[RankLevel10](/0.1/RankLevel10)

[RankSubPhylum](/0.1/RankSubPhylum)

[RankSuperKingdom](/0.1/RankSuperKingdom)

