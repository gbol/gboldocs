# BaseM3c a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m3c|
|rdfs:label|3-methylcytidine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632789">http://www.wikidata.org/entity/Q15632789</a>|

