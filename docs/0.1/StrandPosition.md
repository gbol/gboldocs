# StrandPosition a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|rdfs:comment|Enumeration value that specifies on which strand the element can be found.|
|subDomain|LocationCore|
|skos:editorialNote|In Faldo this is a position object, strand is not a Position to which the begin, end or position property can point to. Therefore we defined as a separate type.|

## skos:member

[ForwardStrandPosition](/0.1/ForwardStrandPosition)

[BothStrandsPosition](/0.1/BothStrandsPosition)

[ReverseStrandPosition](/0.1/ReverseStrandPosition)

