# Phe a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|F|
|ddbjLabel|Phe|
|rdfs:label|Phenylalanine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001441">http://purl.obolibrary.org/obo/SO_0001441</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q170545">http://www.wikidata.org/entity/Q170545</a>|

