# Scaffold a owl:Class extends [UncompleteNASequence](/0.1/UncompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A scaffolded set of contigs.|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000148">http://purl.obolibrary.org/obo/SO_0000148</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[sourceContigs](/0.1/sourceContigs)||0:N|[Contig](/0.1/Contig)|
