# includedSequence a ObjectProperty

## Domain

definition: Reference to the sequence that is included<br>
[PartIncludedSequence](/0.1/PartIncludedSequence)

## Range

[UncompleteNASequence](/0.1/UncompleteNASequence)

## Annotations


