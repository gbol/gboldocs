# signalTarget a ObjectProperty

## Domain

definition: If known specify target, use a 'cellular component' from the GO terms ontology<br>
[SignalPeptide](/0.1/SignalPeptide)

## Range

xsd:anyURI

## Annotations


