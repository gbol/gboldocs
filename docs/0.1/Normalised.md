# Normalised a skos:Concept, [ExpressionType](/0.1/ExpressionType)

## Subclasses

[FPKM](/0.1/FPKM)

[RPKM](/0.1/RPKM)

[TPM](/0.1/TPM)

## Annotations

|||
|-----|-----|

