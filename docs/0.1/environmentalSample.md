# environmentalSample a ObjectProperty

## Domain

definition: This denotes that this sample is the result of a bulk NA extraction method, therefore the organism of the sample and associated sequence(s) are not (exactly) known.<br>
[Sample](/0.1/Sample)

## Range

xsd:boolean

## Annotations

|||
|-----|-----|
|ddbjLabel|environmental_sample|

