# ReplicationRegulatoryRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory region that is involved in the control of the process of nucleotide replication.|
|ddbjLabel|replication_regulatory_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001682">http://purl.obolibrary.org/obo/SO_0001682</a>|

