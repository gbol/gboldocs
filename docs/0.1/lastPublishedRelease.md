# lastPublishedRelease a ObjectProperty

## Domain

definition: The version of the version that has been last published<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:integer

## Annotations


