# strand a ObjectProperty

## Domain

definition: The strand on which it is positioned<br>
[Base](/0.1/Base)

definition: The strand on which it is positioned<br>
[Region](/0.1/Region)

definition: The strand on which it is positioned<br>
[InBetween](/0.1/InBetween)

## Range

[StrandPosition](/0.1/StrandPosition)

## Annotations


