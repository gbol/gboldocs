# varType a ObjectProperty

## Domain

definition: The type of Variation<br>
[Variation](/0.1/Variation)

## Range

[VariationTypes](/0.1/VariationTypes)

## Annotations


