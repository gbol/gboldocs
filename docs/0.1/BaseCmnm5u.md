# BaseCmnm5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|cmnm5u|
|rdfs:label|5-carboxymethylaminomethyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133224">http://www.wikidata.org/entity/Q27133224</a>|

