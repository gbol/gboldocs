# genotypeName a ObjectProperty

## Domain

definition: Genotype name of a sample for a specific variant<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


