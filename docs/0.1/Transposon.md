# Transposon a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

[Retrotransposon](/0.1/Retrotransposon)

## Annotations

|||
|-----|-----|
|ddbjLabel|transposon|
|rdfs:label|transposon|

