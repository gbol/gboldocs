# cellType a ObjectProperty

## Domain

definition: The cell type from which the sequence was obtained, please use term from 'Cell Ontology' (http://cellontology.org/) where possible<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|cell_type|

