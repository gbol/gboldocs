# Trp a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|W|
|ddbjLabel|Trp|
|rdfs:label|Tryptophan|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001440">http://purl.obolibrary.org/obo/SO_0001440</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q181003">http://www.wikidata.org/entity/Q181003</a>|

