# BaseM5c a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m5c|
|rdfs:label|5-methylcytidine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q238546">http://www.wikidata.org/entity/Q238546</a>|

