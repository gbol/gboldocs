# md5 a ObjectProperty

## Domain

definition: MD5sum of the file, not applicable to output files<br>
[LocalDataFile](/0.1/LocalDataFile)

definition: MD5sum of the file, not applicable to output files<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

xsd:string

## Annotations


