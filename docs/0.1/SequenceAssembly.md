# SequenceAssembly a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|An assembly of a sequence based on other sequences.|
|rdfs:comment|An assembly is defined by list of parts that can either be included sequences or gaps. If a sequence is include a region of the included sequence must be specified. If a gap is include a known or estimated gap length must be specified, if unknown the gap length defaults to 100. The sequence at the gaps is equal to N. The provenance of the assembly process can be included.|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[provenance](/0.1/provenance)|The provenance of the feature|0:1|[Provenance](/0.1/Provenance)|
|[sequencePart](/0.1/sequencePart)|Sorted list of parts that upon concatenation forms the complete sequence.|1:N|[SequencePart](/0.1/SequencePart)|
