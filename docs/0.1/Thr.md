# Thr a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|T|
|ddbjLabel|Thr|
|rdfs:label|Threonine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001445">http://purl.obolibrary.org/obo/SO_0001445</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q186521">http://www.wikidata.org/entity/Q186521</a>|

