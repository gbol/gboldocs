# clusteredReadCount a ObjectProperty

## Domain

definition: Clustered read count<br>
[ASVSet](/0.1/ASVSet)

## Range

xsd:integer

## Annotations


