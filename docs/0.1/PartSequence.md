# PartSequence a owl:Class extends [SequencePart](/0.1/SequencePart)

## Subclasses

[PartXRefSequence](/0.1/PartXRefSequence)

[PartIncludedSequence](/0.1/PartIncludedSequence)

## Annotations

|||
|-----|-----|
|skos:definition|A part of the sequence included from another sequence.|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[includedRegion](/0.1/includedRegion)|The included region of referenced sequence, only exact position must be used to denote the start en end region, reference property should be empty. Use the strand property to include forward or reverse complement strand.|1:1|[Region](/0.1/Region)|
