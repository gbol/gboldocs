# Database a owl:Class extends [prov:Entity](/ns/prov/Entity), [void:Dataset](/ns/void/Dataset)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The description of the database containing entities to which XREF elements can reference to.|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[releaseDate](/0.1/releaseDate)|The release date of the database|0:1|xsd:DateTime|
|[version](/0.1/version)|The version of the resource|0:1|xsd:string|
|[id](/0.1/id)|The Id used, typically typed as a  prefix|1:1|xsd:string|
