# BaseP a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|p|
|rdfs:label|pseudouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q420445">http://www.wikidata.org/entity/Q420445</a>|

