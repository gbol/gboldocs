# RankLevel3 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 3|
|parentRank|<a href="http://gbol.life/0.1/RankLevel2">http://gbol.life/0.1/RankLevel2</a>|
|rdfs:label|Rank Level 3|

