# VariableSegment a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|variable segment of immunoglobulin light and heavychains, and T-cell receptor alpha, beta, and gammachains; codes for most of the variable region (V_region)and the last few amino acids of the leader peptide;|
|ddbjLabel|V_segment|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001833">http://purl.obolibrary.org/obo/SO_0001833</a>|

