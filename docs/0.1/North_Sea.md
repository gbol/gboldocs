# North_Sea a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|North Sea|
|rdfs:label|North Sea|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1693">http://www.wikidata.org/entity/Q1693</a>|

