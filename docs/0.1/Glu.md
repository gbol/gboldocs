# Glu a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|E|
|ddbjLabel|Glu|
|rdfs:label|Glutamic acid (Glutamate)|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001454">http://purl.obolibrary.org/obo/SO_0001454</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q181136">http://www.wikidata.org/entity/Q181136</a>|

