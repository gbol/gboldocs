# RankLevel25 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 25|
|parentRank|<a href="http://gbol.life/0.1/RankLevel24">http://gbol.life/0.1/RankLevel24</a>|
|rdfs:label|Rank Level 25|

