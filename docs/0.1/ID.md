# ID a ObjectProperty

## Domain

definition: Variant identification code<br>
[Variation](/0.1/Variation)

## Range

xsd:string

## Annotations


