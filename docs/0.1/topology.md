# topology a ObjectProperty

## Domain

definition: The topology of the sequence (linear, circular)<br>
[NASequence](/0.1/NASequence)

## Range

[Topology](/0.1/Topology)

## Annotations


