# satelliteType a ObjectProperty

## Domain

definition: The type of the satellite<br>
[Satellite](/0.1/Satellite)

## Range

[SatelliteType](/0.1/SatelliteType)

## Annotations


