# identity95MismatchCount a ObjectProperty

## Domain

definition: Identity 95 mismatch count<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


