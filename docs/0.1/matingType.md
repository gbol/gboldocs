# matingType a ObjectProperty

## Domain

definition: The mating type of the organism from which the sample was obtained. Mating type is used for prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes. For eukaryotic organisms that undergo meiosis with sexually dimorphic gametes please use the sex property.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|mating_type|

