# Glx a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|Z|
|ddbjLabel|Glx|
|rdfs:label|Glutamine or GLutamate|

