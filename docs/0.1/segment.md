# segment a ObjectProperty

## Domain

definition: The name or identifier of virus or phage (segment) from which the sample was obtained.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|segment|

