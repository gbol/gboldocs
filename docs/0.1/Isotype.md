# Isotype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|term used in nomenclature (duplicate of the holotype)|
|ddbjLabel|isotype|
|rdfs:label|isotype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q13188667">http://www.wikidata.org/entity/Q13188667</a>|

