# BaseX a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|x|
|rdfs:label|3-(3-amino-3-carboxypropyl)uridine, (acp3)u|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109263">http://www.wikidata.org/entity/Q27109263</a>|

