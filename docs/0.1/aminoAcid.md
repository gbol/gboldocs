# aminoAcid a ObjectProperty

## Domain

definition: The encoded amino acid<br>
[AntiCodon](/0.1/AntiCodon)

definition: The encoded amino acid<br>
[TranslExcept](/0.1/TranslExcept)

## Range

[AminoAcid](/0.1/AminoAcid)

## Annotations


