# PairedEnds a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|paired sequences from the two ends of a DNA fragment.|
|ddbjLabel|paired-ends|

