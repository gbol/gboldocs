# antiCodingRegion a ObjectProperty

## Domain

definition: The position of the anticodon within the tRNA transcript<br>
[AntiCodon](/0.1/AntiCodon)

## Range

[Region](/0.1/Region)

## Annotations


