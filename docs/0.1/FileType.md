# FileType a skos:Collection extends owl:Class

## Annotations


## skos:member

[TXT](/0.1/TXT)

[GenBank](/0.1/GenBank)

[Other](/0.1/Other)

[TSV](/0.1/TSV)

[FASTA](/0.1/FASTA)

[FASTQ](/0.1/FASTQ)

[EMBL](/0.1/EMBL)

[CSV](/0.1/CSV)

