# taxonRank a ObjectProperty

## Domain

definition: The rank of the taxon<br>
[Taxon](/0.1/Taxon)

## Range

[Rank](/0.1/Rank)

## Annotations


