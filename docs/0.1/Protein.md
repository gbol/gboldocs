# Protein a owl:Class extends [Sequence](/0.1/Sequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Represents any protein sequence|
|rdfs:comment|The IRI of a sequence of must be equal to <a href="http://gbol.life/0.1/protein/">http://gbol.life/0.1/protein/</a><SHA-384 key of protein sequence>.|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000104">http://purl.obolibrary.org/obo/SO_0000104</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q8054">http://www.wikidata.org/entity/Q8054</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[feature](/0.1/feature)|The set of features annotating the sequence|0:N|[ProteinFeature](/0.1/ProteinFeature)|
