# hiseq a skos:Concept, [SequencingPlatform](/0.1/SequencingPlatform) extends [illumina](/0.1/illumina)

## Subclasses

[hiseq2000](/0.1/hiseq2000)

[hiseq3000](/0.1/hiseq3000)

[hiseq4000](/0.1/hiseq4000)

[hiseqx](/0.1/hiseqx)

## Annotations


