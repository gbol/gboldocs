# StructureFeature a owl:Class extends [GenomicFeature](/0.1/GenomicFeature)

## Subclasses

[MiscStructure](/0.1/MiscStructure)

[StemLoop](/0.1/StemLoop)

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|

## Properties

