# TelomericRepeat a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The telomeric repeat is a repeat region, part of the chromosome, which in yeast, is a G-rich terminal sequence of the form (TG(1-3))n or more precisely ((TG)(1-6)TG(2-3))n.|
|ddbjLabel|telomeric_repeat|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001496">http://purl.obolibrary.org/obo/SO_0001496</a>|

