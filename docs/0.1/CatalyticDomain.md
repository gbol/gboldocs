# CatalyticDomain a owl:Class extends [ConservedRegion](/0.1/ConservedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The regions that form the catalytic region of protein.|
|rdfs:comment|Use collection of regions to denote all elements of the active side.|
|subDomain|SequenceFeatureCore|

## Properties

