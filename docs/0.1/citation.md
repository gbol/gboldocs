# citation a ObjectProperty

## Domain

definition: Any publication associated to feature annotation<br>
[Sequence](/0.1/Sequence)

definition: Any publication associated to feature annotation<br>
[Feature](/0.1/Feature)

definition: Any publication associated to feature annotation<br>
[Sample](/0.1/Sample)

## Range

[Citation](/0.1/Citation)

## Annotations

|||
|-----|-----|
|skos:editorialNote|Replaced in GBOl to citation reference in both the feature and Sequence Object|

