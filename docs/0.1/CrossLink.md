# CrossLink a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A side that covalently cross links to another cross link side|
|rdfs:comment|The cross links are linked together, so use the location of the other cross link feature to determine location of the partnering base.|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|TODO: we could make enumeration for cross link types in the future|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001087">http://purl.obolibrary.org/obo/SO_0001087</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[crossLinkType](/0.1/crossLinkType)|Textual description of the crosslink type|0:1|xsd:string|
|[crossLinkTo](/0.1/crossLinkTo)|Link to the other cross link|1:1|[CrossLink](/0.1/CrossLink)|
