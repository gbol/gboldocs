# KnownLengthAssemblyGap a owl:Class extends [AssemblyGap](/0.1/AssemblyGap)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|gap with known gap size between two components of a genome or transcriptome assembly;|
|rdfs:comment|The length of gap can be determined based on the associated location. The sequence at the denoted position must be a series of N's.|
|ddbjLabel|assembly_gap|
|subDomain|SequenceFeatureCore|

## Properties

