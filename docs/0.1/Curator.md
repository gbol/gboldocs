# Curator a owl:Class extends [prov:Person](/ns/prov/Person)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|
|rdfs:label|Curator|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[orcid](/0.1/orcid)|The unique identifier to identify the scientist|1:1|IRI|
