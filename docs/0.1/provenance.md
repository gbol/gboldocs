# provenance a ObjectProperty

## Domain

[Qualifier](/0.1/Qualifier)

definition: The provenance of the feature<br>
[XRef](/0.1/XRef)

definition: The provenance of the feature<br>
[Experiment](/0.1/Experiment)

definition: The provenance of the feature<br>
[Feature](/0.1/Feature)

definition: The provenance of the feature<br>
[SequenceAssembly](/0.1/SequenceAssembly)

definition: The provenance of the feature<br>
[Library](/0.1/Library)

definition: The provenance of the feature<br>
[ExonList](/0.1/ExonList)

definition: The provenance of the feature<br>
[Note](/0.1/Note)

## Range

[Provenance](/0.1/Provenance)

[FeatureProvenance](/0.1/FeatureProvenance)

[XRefProvenance](/0.1/XRefProvenance)

## Annotations


