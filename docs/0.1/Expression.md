# Expression a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Expression object that has to be defined of type FPKM, RPKM, or raw counts.<br>Should be pointing to only one of the three: exon, transcript or gene.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[experiment](/0.1/experiment)|To which experiment this expression value belongs to|1:1|[Experiment](/0.1/Experiment)|
|[expressionType](/0.1/expressionType)|Expression type|1:1|[ExpressionType](/0.1/ExpressionType)|
|[annotation](/0.1/annotation)|Note on the provenance|1:1|[ProvenanceApplication](/0.1/ProvenanceApplication)|
|[value](/0.1/value)|Expression value|1:1|xsd:Double|
