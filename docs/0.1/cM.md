# cM a skos:Concept, [MapUnits](/0.1/MapUnits)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|For QTLs with centiMorgan as Map Unit|

