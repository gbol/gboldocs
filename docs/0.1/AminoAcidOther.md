# AminoAcidOther a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|aminoAcidLetter|X|
|ddbjLabel|AminoAcidOther|
|ddbjLabel|Other|

