# Allelic a skos:Concept, [PseudoGeneType](/0.1/PseudoGeneType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|a (unitary) pseudogene that is stable in the population but importantly it has a functional alternative allele also in the population. i.e., one strain may have the gene, another strain may have the pseudogene. MHC haplotypes have allelic pseudogenes.|
|ddbjLabel|allelic|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002189">http://purl.obolibrary.org/obo/SO_0002189</a>|

