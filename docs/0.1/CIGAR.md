# CIGAR a ObjectProperty

## Domain

definition: Cigar string describing alignment of alternate allele to reference<br>
[Variation](/0.1/Variation)

## Range

xsd:string

## Annotations


