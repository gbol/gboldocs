# AlignXGenus a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|alignment to a reference genome within another genus.|
|ddbjLabel|align_xgenus|

