# alleleNumber a ObjectProperty

## Domain

definition: total number of alleles in called genotypes<br>
[Variation](/0.1/Variation)

## Range

xsd:integer

## Annotations


