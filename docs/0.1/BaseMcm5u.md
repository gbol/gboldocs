# BaseMcm5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mcm5u|
|rdfs:label|5-methoxycarbonylmethyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109316">http://www.wikidata.org/entity/Q27109316</a>|

