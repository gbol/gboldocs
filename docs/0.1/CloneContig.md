# CloneContig a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|linkage is provided by a clone contig in the tiling path (TPF). For example, a gap where there is a known clone, but there is not yet sequence for that clone.|
|ddbjLabel|clone_contig|

