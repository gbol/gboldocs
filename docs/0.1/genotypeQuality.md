# genotypeQuality a ObjectProperty

## Domain

definition: Genotype quality<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


