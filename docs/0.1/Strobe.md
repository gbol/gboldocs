# Strobe a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|strobe sequencing (PacBio).|
|ddbjLabel|strobe|

