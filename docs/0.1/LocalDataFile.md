# LocalDataFile a owl:Class extends [DataFile](/0.1/DataFile)

## Subclasses

[LocalSequenceFile](/0.1/LocalSequenceFile)

## Annotations

|||
|-----|-----|
|skos:definition|A file on the local disk|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[localFileName](/0.1/localFileName)|The local file name|1:1|xsd:string|
|[retrievedOn](/0.1/retrievedOn)|Optional a time point at which the file was downloaded|0:1|xsd:DateTime|
|[localFilePath](/0.1/localFilePath)|The local file path|1:1|xsd:string|
|[md5](/0.1/md5)|MD5sum of the file, not applicable to output files|0:1|xsd:string|
|[fileType](/0.1/fileType)|Optionally set the fileType|0:1|[FileType](/0.1/FileType)|
|[size](/0.1/size)|Size of the file in bytes, not applicable to output files|0:1|xsd:Long|
|[retrievedFrom](/0.1/retrievedFrom)|Optional an URL it was downloaded from|0:1|IRI|
