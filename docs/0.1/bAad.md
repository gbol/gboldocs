# bAad a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|bAad|
|rdfs:label|3-Aminoadipic acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133206">http://www.wikidata.org/entity/Q27133206</a>|

