# RankSubGenus a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub genus|
|parentRank|<a href="http://gbol.life/0.1/RankGenus">http://gbol.life/0.1/RankGenus</a>|
|rdfs:label|sub genus|

