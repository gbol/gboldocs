# BaseMan_q a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|man q|
|rdfs:label|beta-D-mannosylqueuosine|

