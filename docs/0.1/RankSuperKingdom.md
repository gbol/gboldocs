# RankSuperKingdom a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: super kingdom|
|rdfs:label|super kingdom|

