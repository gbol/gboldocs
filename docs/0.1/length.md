# length a ObjectProperty

## Domain

definition: Length of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

xsd:long

## Annotations


