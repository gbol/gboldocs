# referenceSequence a ObjectProperty

## Domain

definition: Optionally define another reference sequence then the parent sequence object, this is for example needed for the mdg4 gene of Drosophila, which is transcript from two chromosomes. If the element to which the location belongs is defined on multiple sequence objects, this field is obligatory.<br>
[Location](/0.1/Location)

## Range

[Sequence](/0.1/Sequence)

## Annotations


