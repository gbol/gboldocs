# tRNA a owl:Class extends [MaturedRNA](/0.1/MaturedRNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|mature transfer RNA, a small RNA molecule (75-85 bases long) that mediates the translation of a nucleic acid sequence into an amino acid sequence|
|ddbjLabel|tRNA|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000253">http://purl.obolibrary.org/obo/SO_0000253</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q201448">http://www.wikidata.org/entity/Q201448</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[anticodon](/0.1/anticodon)|The anti codon of the TRNA|0:1|[AntiCodon](/0.1/AntiCodon)|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
