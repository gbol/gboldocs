# snoRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small [nuclear] RNA molecules involved in modification and processing of ribosomal RNA or transfer RNA; found in archaea and in eukaryotic species where they are localized in the nucleolus.|
|ddbjLabel|snoRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000275">http://purl.obolibrary.org/obo/SO_0000275</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q284416">http://www.wikidata.org/entity/Q284416</a>|

