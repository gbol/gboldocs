# identifiedBy a ObjectProperty

## Domain

definition: The name of the expert who identified the specimen taxonomically<br>
[Sample](/0.1/Sample)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|ddbjLabel|identified_by|

