# BaseQ a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|q|
|rdfs:label|queuosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q425950">http://www.wikidata.org/entity/Q425950</a>|

