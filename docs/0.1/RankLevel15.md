# RankLevel15 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 15|
|parentRank|<a href="http://gbol.life/0.1/RankLevel14">http://gbol.life/0.1/RankLevel14</a>|
|rdfs:label|Rank Level 15|

