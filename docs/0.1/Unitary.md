# Unitary a skos:Concept, [PseudoGeneType](/0.1/PseudoGeneType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|the pseudogene has no parent. It is the original gene, which is functional in some species but disrupted in some way (indels, mutation, recombination) in another species or strain.|
|ddbjLabel|unitary|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001759">http://purl.obolibrary.org/obo/SO_0001759</a>|

