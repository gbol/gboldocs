# Asn a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|N|
|ddbjLabel|Asn|
|rdfs:label|Asparagine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001449">http://purl.obolibrary.org/obo/SO_0001449</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q29519883">http://www.wikidata.org/entity/Q29519883</a>|

