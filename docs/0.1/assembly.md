# assembly a ObjectProperty

## Domain

definition: The optional assembly information of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

[SequenceAssembly](/0.1/SequenceAssembly)

## Annotations


