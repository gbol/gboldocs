# ncRNAClass a ObjectProperty

## Domain

definition: The type of the non coding RNA<br>
[ncRNA](/0.1/ncRNA)

## Range

[ncRNAType](/0.1/ncRNAType)

## Annotations

|||
|-----|-----|
|ddbjLabel|ncRNA_class|
|skos:editorialNote|enum added to ncRNA|

