# eff_length a ObjectProperty

## Domain

definition: Effective length refers to the number of possible start sites a feature could have generated a fragment of that particular length<br>
[Kallisto](/0.1/Kallisto)

## Range

xsd:double

## Annotations


