# Pyl a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|O|
|ddbjLabel|Pyl|
|rdfs:label|Pyrrolysine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q409687">http://www.wikidata.org/entity/Q409687</a>|

