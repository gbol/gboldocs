# Attenuator a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A sequence segment located within the five prime end of an mRNA that causes premature termination of translation.|
|ddbjLabel|attenuator|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000140">http://purl.obolibrary.org/obo/SO_0000140</a>|

