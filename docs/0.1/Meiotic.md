# Meiotic a skos:Concept, [RecombinationType](/0.1/RecombinationType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A genomic region in which there is an exchange of genetic material as a result of the repair of meiosis-specific double strand breaks that occur during meiotic prophase.|
|ddbjLabel|meiotic|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002155">http://purl.obolibrary.org/obo/SO_0002155</a>|

