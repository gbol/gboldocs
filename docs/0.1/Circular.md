# Circular a skos:Concept, [Topology](/0.1/Topology)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A circular NA molecule.|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000988">http://purl.obolibrary.org/obo/SO_0000988</a>|

