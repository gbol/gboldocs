# Kallisto a owl:Class extends [ProvenanceMapping](/0.1/ProvenanceMapping)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[eff_length](/0.1/eff_length)|Effective length refers to the number of possible start sites a feature could have generated a fragment of that particular length|1:1|xsd:Double|
