# SequenceAnnotation a owl:Class extends [NAFeature](/0.1/NAFeature)

## Subclasses

[AssemblyAnnotation](/0.1/AssemblyAnnotation)

[ModifiedBase](/0.1/ModifiedBase)

[VariationFeature](/0.1/VariationFeature)

## Annotations

|||
|-----|-----|
|skos:definition|Annotation on the sequence itself|
|subDomain|SequenceFeatureCore|

## Properties

