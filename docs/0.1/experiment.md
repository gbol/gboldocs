# experiment a ObjectProperty

## Domain

definition: To which experiment this expression value belongs to<br>
[Expression](/0.1/Expression)

## Range

[Experiment](/0.1/Experiment)

## Annotations

|||
|-----|-----|
|skos:editorialNote|Replaced with the provenance infrastructure, use the onProperty property to denote to which property it relates|

