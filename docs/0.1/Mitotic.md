# Mitotic a skos:Concept, [RecombinationType](/0.1/RecombinationType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mitotic|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002154">http://purl.obolibrary.org/obo/SO_0002154</a>|

