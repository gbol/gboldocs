# symbol a ObjectProperty

## Domain

definition: Symbol where the QTL was published under<br>
[QTL](/0.1/QTL)

## Range

xsd:string

## Annotations


