# Asp a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|D|
|ddbjLabel|Asp (Aspartate)|
|rdfs:label|Aspartic acid|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001453">http://purl.obolibrary.org/obo/SO_0001453</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q178450">http://www.wikidata.org/entity/Q178450</a>|

