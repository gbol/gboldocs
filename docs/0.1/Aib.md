# Aib a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Aib|
|rdfs:label|2-Aminoisobutyric acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4596860">http://www.wikidata.org/entity/Q4596860</a>|

