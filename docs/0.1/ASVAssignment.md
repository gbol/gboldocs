# ASVAssignment a owl:Class extends [ProvenanceClassification](/0.1/ProvenanceClassification)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|NGTax 2.0 classification provenance of ASV's<br>URL: gitlab.com/wurssb/NG-Tax|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[ratio](/0.1/ratio)|Ratio|1:1|xsd:Float|
|[taxon](/0.1/taxon)|Taxon|1:1|[Taxon](/0.1/Taxon)|
|[type](/0.1/type)||1:1|[TaxonAssignmentType](/0.1/TaxonAssignmentType)|
|[numberHits](/0.1/numberHits)|Number of hits|1:1|xsd:Integer|
|[levelCount](/0.1/levelCount)|Level count|1:1|xsd:Integer|
