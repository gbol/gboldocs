# RankLevel11 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 11|
|parentRank|<a href="http://gbol.life/0.1/RankLevel10">http://gbol.life/0.1/RankLevel10</a>|
|rdfs:label|Rank Level 11|

