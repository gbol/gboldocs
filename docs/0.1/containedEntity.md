# containedEntity a ObjectProperty

## Domain

definition: Optional list of entities that are contained within the file<br>
[DataFile](/0.1/DataFile)

## Range

[prov:Entity](/ns/prov/Entity)

## Annotations


