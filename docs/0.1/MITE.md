# MITE a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A highly repetitive and short (100-500 base pair) transposable element with terminal inverted repeats (TIR) and target site duplication (TSD). MITEs do not encode proteins.|
|ddbjLabel|MITE|
|rdfs:label|MITE|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000338">http://purl.obolibrary.org/obo/SO_0000338</a>|

