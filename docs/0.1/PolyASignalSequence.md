# PolyASignalSequence a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The recognition sequence necessary for endonuclease cleavage of an RNA transcript that is followed by polyadenylation; consensus=AATAAA.|
|ddbjLabel|polyA_signal|
|ddbjLabel|polyA_signal_sequence|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000551">http://purl.obolibrary.org/obo/SO_0000551</a>|

