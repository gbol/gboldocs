# RegulationSite a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Any region of sequence that functions in the regulation of transcription or translation;|
|rdfs:comment|This feature has replaced the following Feature Keys on 15-DEC-2014: enhancer, promoter, CAAT_signal, TATA_signal, -35_signal, -10_signal, RBS, GC_signal, polyA_signal, attenuator, terminator, misc_signal.|
|ddbjLabel|regulatory|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|type class created, only present in the RegulationSite feature|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[regulatoryClass](/0.1/regulatoryClass)|The type of regulation|0:1|[RegulatoryClass](/0.1/RegulatoryClass)|
