# Ahe a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Ahe|
|rdfs:label|2-Aminoheptanoic acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133207">http://www.wikidata.org/entity/Q27133207</a>|

