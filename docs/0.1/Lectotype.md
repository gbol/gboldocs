# Lectotype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|lectotype|
|rdfs:label|lectotype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2439719">http://www.wikidata.org/entity/Q2439719</a>|

