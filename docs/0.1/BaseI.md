# BaseI a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|i|
|rdfs:label|inosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q422564">http://www.wikidata.org/entity/Q422564</a>|

