# GapRegion a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|extra nucleotides inserted between rearranged immunoglobulin segments.|
|ddbjLabel|N_region|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000458">http://purl.obolibrary.org/obo/SO_0000458</a>|

