# includedXRefSequence a ObjectProperty

## Domain

definition: The xref from which the sequence is included<br>
[PartXRefSequence](/0.1/PartXRefSequence)

## Range

[XRef](/0.1/XRef)

## Annotations


