# ListOfRegions a owl:Class extends [CollectionOfRegions](/0.1/CollectionOfRegions)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Equal to the order in the genbank format. Ordered set of regions. Order is denoted with the rdfs:_n predicate.|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from Faldo|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#ListOfRegions">http://biohackathon.org/resource/faldo#ListOfRegions</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[members](/0.1/members)||1:N|[Region](/0.1/Region)|
