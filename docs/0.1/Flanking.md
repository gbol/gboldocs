# Flanking a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat lying outside the sequence for which it has functional significance (eg. transposon insertion target sites).|
|ddbjLabel|flanking|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002192">http://purl.obolibrary.org/obo/SO_0002192</a>|

