# members a ObjectProperty

## Domain

definition: A set of regions making up a location spreader over multiple regions. The members inherit the reference value from this CollectionOfRegions object.<br>
[CollectionOfRegions](/0.1/CollectionOfRegions)

[ListOfRegions](/0.1/ListOfRegions)

[BagOfRegions](/0.1/BagOfRegions)

## Range

[Region](/0.1/Region)

## Annotations


