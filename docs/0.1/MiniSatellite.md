# MiniSatellite a skos:Concept, [SatelliteType](/0.1/SatelliteType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat region containing tandemly repeated sequences having a unit length of 10 to 40 bp.|
|ddbjLabel|minisatellite|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000643">http://purl.obolibrary.org/obo/SO_0000643</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q418883">http://www.wikidata.org/entity/Q418883</a>|

