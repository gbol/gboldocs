# accession a ObjectProperty

## Domain

definition: The accession id of the entry referenced<br>
[Sequence](/0.1/Sequence)

definition: The accession id of the entry referenced<br>
[XRef](/0.1/XRef)

definition: The accession id of the entry referenced<br>
[Feature](/0.1/Feature)

definition: The accession id of the entry referenced<br>
[QTL](/0.1/QTL)

definition: The accession id of the entry referenced<br>
[sapp:InterProScanModels](/sapp/0.1/InterProScanModels)

## Range

xsd:string

## Annotations


