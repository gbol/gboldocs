# rRNA a owl:Class extends [MaturedRNA](/0.1/MaturedRNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|mature ribosomal RNA; RNA component of the ribonucleo protein particle (ribosome) which assembles amino acids into proteins.|
|rdfs:comment|rRNA sizes should be annotated with the /product qualifier.|
|ddbjLabel|rRNA|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000252">http://purl.obolibrary.org/obo/SO_0000252</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q215980">http://www.wikidata.org/entity/Q215980</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
