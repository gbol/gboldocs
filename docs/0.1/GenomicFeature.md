# GenomicFeature a owl:Class extends [NAFeature](/0.1/NAFeature)

## Subclasses

[Homology](/0.1/Homology)

[TranscriptionElement](/0.1/TranscriptionElement)

[StructureFeature](/0.1/StructureFeature)

[RepeatFeature](/0.1/RepeatFeature)

[RecognizedRegion](/0.1/RecognizedRegion)

## Annotations

|||
|-----|-----|
|skos:definition|All features that can be present on a genome|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[operon](/0.1/operon)|Operon to which the genetic element belongs to|0:1|[Operon](/0.1/Operon)|
