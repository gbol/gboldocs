# VariantGenotype a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Defines a genotype for a called Variation in the Variation class|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[expectedAlleleCounts](/0.1/expectedAlleleCounts)|Expected allele counts(comma separated)|0:1|xsd:string|
|[genotype](/0.1/genotype)|Genotypes of the sample the variant was called on|0:N|[VariantGenotype](/0.1/VariantGenotype)|
|[strain](/0.1/strain)|The name or identifier of the strain|0:1|xsd:string|
|[altOrganism](/0.1/altOrganism)|Alternative Organism|0:1|[Organism](/0.1/Organism)|
|[altDepth](/0.1/altDepth)|Read depth of alternate base|0:1|xsd:string|
|[phredLikelihood](/0.1/phredLikelihood)|phred scaled likelihood|0:1|xsd:Integer|
|[readDepth](/0.1/readDepth)|Read depth|0:1|xsd:string|
|[mapQuality](/0.1/mapQuality)|Mapping Quality|0:1|xsd:string|
|[depthQuality](/0.1/depthQuality)|Read depth quality|0:1|xsd:string|
|[genotypeName](/0.1/genotypeName)|Genotype name of a sample for a specific variant|0:1|xsd:string|
|[likelihood](/0.1/likelihood)|Genotype likelihoods comprised of comma separated floating point log10-scaled likelihoods for all possible genotypes given the set of alleles defined in the REF and ALT fields|0:1|xsd:string|
|[genotypeQuality](/0.1/genotypeQuality)|Genotype quality|0:1|xsd:string|
|[refDepth](/0.1/refDepth)|Read depth of reference base|0:1|xsd:string|
|[haploQuality](/0.1/haploQuality)|Haplotype qualities|0:1|xsd:string|
|[phaseSet](/0.1/phaseSet)|Phase set|0:1|xsd:string|
|[pVal](/0.1/pVal)|p-value from Fisher's Exact Test|0:1|xsd:string|
|[phaseQuality](/0.1/phaseQuality)|Phase quality|0:1|xsd:Integer|
|[filter](/0.1/filter)|filter|0:1|xsd:string|
|[phredProb](/0.1/phredProb)|phred probabilities|0:1|xsd:Float|
|[freq](/0.1/freq)|Allele frequency|0:1|xsd:string|
|[realGenotype](/0.1/realGenotype)|Actual Genotype of a sample for a specific variant|0:1|xsd:string|
|[refOrganism](/0.1/refOrganism)|Reference Organism|0:1|[Organism](/0.1/Organism)|
