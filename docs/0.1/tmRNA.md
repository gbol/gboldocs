# tmRNA a owl:Class extends [MaturedRNA](/0.1/MaturedRNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|transfer messenger RNA; tmRNA acts as a tRNA first,and then as an mRNA that encodes a peptide tag; the ribosome translates this mRNA region of tmRNA and attaches the encoded peptide tag to the C-terminus of the unfinished protein; this attached tag targets the protein for destruction or proteolysis;|
|ddbjLabel|tmRNA|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000584">http://purl.obolibrary.org/obo/SO_0000584</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q285904">http://www.wikidata.org/entity/Q285904</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[tagPeptide](/0.1/tagPeptide)|base location encoding the polypeptide for proteolysis tag of tmRNA and its termination codon;|0:1|[Region](/0.1/Region)|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
