# Gene a owl:Class extends [TranscriptionElement](/0.1/TranscriptionElement)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A region (or regions) that includes all of the sequence elements necessary to encode a functional transcript. A gene may include regulatory regions, transcribed regions and/or other functional sequence regions.|
|rdfs:comment|the gene feature describes the interval of DNA that corresponds to a genetic trait or phenotype; the feature is, by definition, not strictly bound to it's positions at the ends;  it is meant to represent a region where the gene is located.|
|gen:framing|:exon|
|ddbjLabel|gene|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo">http://purl.obolibrary.org/obo</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7187">http://www.wikidata.org/entity/Q7187</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[geneSymbolSynonym](/0.1/geneSymbolSynonym)|Alternative/old gene symbols|0:N|xsd:string|
|[exon](/0.1/exon)|All exons that are part of the gene|0:N|[Exon](/0.1/Exon)|
|[allele](/0.1/allele)|List of names of known allele variants of the gene|0:N|xsd:string|
|[pseudogene](/0.1/pseudogene)|If this gene is a pseudo gene, indicate the type|0:1|[PseudoGeneType](/0.1/PseudoGeneType)|
|[oldLocusTag](/0.1/oldLocusTag)|Old locus tag of the gene|0:1|xsd:string|
|[intron](/0.1/intron)|All introns that are part of the gene|0:N|[Intron](/0.1/Intron)|
|[transcript](/0.1/transcript)|All transcripts that are derived from the gene|0:N|[Transcript](/0.1/Transcript)|
|[locusTag](/0.1/locusTag)|Locus tag of the gene|1:1|xsd:string|
|[geneSymbol](/0.1/geneSymbol)|Symbol of the gene (not the symbol of the resulting protein)|0:1|xsd:string|
