# note a ObjectProperty

## Domain

definition: A set off some additional notes<br>
[Sequence](/0.1/Sequence)

definition: A set off some additional notes<br>
[Feature](/0.1/Feature)

definition: A set off some additional notes<br>
[Sample](/0.1/Sample)

definition: A set off some additional notes<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[Note](/0.1/Note)

## Annotations

|||
|-----|-----|
|ddbjLabel|note|
|skos:editorialNote|Each feature can have note|

