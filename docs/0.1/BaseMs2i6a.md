# BaseMs2i6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|ms2i6a|
|rdfs:label|2-methylthio-N6-isopentenyladenosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27132248">http://www.wikidata.org/entity/Q27132248</a>|

