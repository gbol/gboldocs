# Unspecified a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|rdfs:comment|used only when converting old AGPs that lack a field for linkage evidence into the new format.|
|ddbjLabel|unspecified|

