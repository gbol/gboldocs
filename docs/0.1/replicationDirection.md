# replicationDirection a ObjectProperty

## Domain

definition: The direction of the replication can be forward, reverse or both<br>
[ReplicationOrigin](/0.1/ReplicationOrigin)

## Range

[StrandPosition](/0.1/StrandPosition)

## Annotations


