# illumina a skos:Concept, [SequencingPlatform](/0.1/SequencingPlatform)

## Subclasses

[hiseq](/0.1/hiseq)

[miniseq](/0.1/miniseq)

[miseq](/0.1/miseq)

[nextseq](/0.1/nextseq)

[novaseq](/0.1/novaseq)

## Annotations


