# MiscBinding a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Site in nucleic acid which covalently or non-covalently binds another moiety that cannot be described by any other binding key (primer_bind or protein_bind);|
|rdfs:comment|note that featureKey regulatory with /regulatory_class=\"ribosome_binding_site\"should be used for ribosome binding sites.|
|ddbjLabel|misc_binding|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000409">http://purl.obolibrary.org/obo/SO_0000409</a>|

## Properties

