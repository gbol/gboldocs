# replace a ObjectProperty

## Domain

[VariationFeature](/0.1/VariationFeature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|replace|
|skos:editorialNote|Add to variation class, replace describes the new sequence.<br>This needs further improvement<br>* Old versus new or new versus old<br>* make use of A>C standard format|

