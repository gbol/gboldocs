# MiscStructure a owl:Class extends [StructureFeature](/0.1/StructureFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|any secondary or tertiary nucleotide structure or conformation that cannot be described by other Structure keys (stem_loop and D-loop);|
|ddbjLabel|misc_structure|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000002">http://purl.obolibrary.org/obo/SO_0000002</a>|

## Properties

