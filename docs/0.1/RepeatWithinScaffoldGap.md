# RepeatWithinScaffoldGap a skos:Concept, [GapType](/0.1/GapType) extends [WithinScaffoldGap](/0.1/WithinScaffoldGap)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|repeat within scaffold|

