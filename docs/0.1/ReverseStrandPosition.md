# ReverseStrandPosition a skos:Concept, [StrandPosition](/0.1/StrandPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|The feature is located/only tells something about the reverse strand.|
|skos:editorialNote|Taken from Faldo ontology|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#ReverseStrandPosition">http://biohackathon.org/resource/faldo#ReverseStrandPosition</a>|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001031">http://purl.obolibrary.org/obo/SO_0001031</a>|

