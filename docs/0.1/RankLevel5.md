# RankLevel5 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 5|
|parentRank|<a href="http://gbol.life/0.1/RankLevel4">http://gbol.life/0.1/RankLevel4</a>|
|rdfs:label|Rank Level 5|

