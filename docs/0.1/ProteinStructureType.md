# ProteinStructureType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The secondary protein structure types.|
|subDomain|SequenceFeatureCore|

## skos:member

[BetaStrand](/0.1/BetaStrand)

[AlphaHelix](/0.1/AlphaHelix)

[CoiledCoil](/0.1/CoiledCoil)

[Turn](/0.1/Turn)

