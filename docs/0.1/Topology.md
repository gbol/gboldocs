# Topology a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The topology of a nucleic acid string|
|subDomain|DocumentCore|

## skos:member

[Circular](/0.1/Circular)

[Linear](/0.1/Linear)

