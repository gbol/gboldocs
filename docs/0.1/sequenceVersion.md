# sequenceVersion a ObjectProperty

## Domain

definition: The version of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

xsd:integer

## Annotations


