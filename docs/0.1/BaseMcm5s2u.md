# BaseMcm5s2u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mcm5s2u|
|rdfs:label|5-methoxycarbonylmethyl-2-thiouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109315">http://www.wikidata.org/entity/Q27109315</a>|

