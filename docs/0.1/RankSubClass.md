# RankSubClass a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub class|
|parentRank|<a href="http://gbol.life/0.1/RankClass">http://gbol.life/0.1/RankClass</a>|
|rdfs:label|sub class|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1428357">http://www.wikidata.org/entity/Q1428357</a>|

