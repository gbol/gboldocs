# GenomeSurveySequenceEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|GSS|
|rdfs:label|Genome Survey Sequence|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q5533493">http://www.wikidata.org/entity/Q5533493</a>|

