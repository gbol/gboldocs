# MiscVariation a owl:Class extends [VariationFeature](/0.1/VariationFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Any variation other then a natural or an updated sequence variation.|
|rdfs:comment|the misc_difference featureKey should be used to describe variability that arises as a result of genetic manipulation (e.g. site directed mutagenesis); use /replace=\"\" to annotate deletion, e.g.misc_difference 412..433/replace=\"\"|
|ddbjLabel|misc_difference|
|subDomain|SequenceFeatureCore|

## Properties

