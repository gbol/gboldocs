# BaseM1g a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m1g|
|rdfs:label|1-methylguanosine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001324">http://purl.obolibrary.org/obo/SO_0001324</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q161647">http://www.wikidata.org/entity/Q161647</a>|

