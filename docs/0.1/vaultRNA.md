# vaultRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|RNA component of the vault ribonucleo protein, a complex which consists of a major vault protein (MVP), two minor vault proteins (VPARP and TEP1), and several small RNA molecules and has been suggested to be involved in drug resistance.|
|ddbjLabel|vault_RNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000404">http://purl.obolibrary.org/obo/SO_0000404</a>|

