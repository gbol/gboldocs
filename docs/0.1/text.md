# text a ObjectProperty

## Domain

definition: The textual description of the note<br>
[Note](/0.1/Note)

## Range

xsd:string

## Annotations


