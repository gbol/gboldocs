# parameterString a ObjectProperty

## Domain

definition: The parameter string as used to start the software for the this activity, parameters referencing to another dataset used must be specified with {IRI}. So for example '-threshold 0.05 -i {:datasetIn1}'. Each of referenced input datasets must be also be referenced with the prov:used property<br>
[AutomaticAnnotationActivity](/0.1/AutomaticAnnotationActivity)

## Range

xsd:string

## Annotations


