# artificialLocation a ObjectProperty

## Domain

definition: Indicate that the location is artificial because of a heterogeneous population for low quality sequence region<br>
[Feature](/0.1/Feature)

## Range

[ReasonArtificialLocation](/0.1/ReasonArtificialLocation)

## Annotations

|||
|-----|-----|
|ddbjLabel|artificial_location|
|skos:editorialNote|Indicate that a feature has a artificial location with reason, added to feature, was originally only possible for mRNA and CDS to fix frame-shifts|
|skos:editorialNote|key:mRNA, CDS|

