# TranslExcept a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[exceptRegion](/0.1/exceptRegion)|The position of region which translate to other amino acid then the default translation table|1:1|[Region](/0.1/Region)|
|[aminoAcid](/0.1/aminoAcid)|The encoded amino acid|1:1|[AminoAcid](/0.1/AminoAcid)|
