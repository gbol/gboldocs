# SatelliteType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|A list of satellite types|
|subDomain|SequenceFeatureCore|

## skos:member

[MicroSatellite](/0.1/MicroSatellite)

[MiniSatellite](/0.1/MiniSatellite)

[CommonSattellite](/0.1/CommonSattellite)

