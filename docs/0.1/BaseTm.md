# BaseTm a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|tm|
|rdfs:label|2'-O-methyl-5-methyluridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109137">http://www.wikidata.org/entity/Q27109137</a>|

