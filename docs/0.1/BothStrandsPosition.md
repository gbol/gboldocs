# BothStrandsPosition a skos:Concept, [StrandPosition](/0.1/StrandPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|The feature is located/tells something about both strands.|
|skos:editorialNote|Taken from Faldo ontology|

