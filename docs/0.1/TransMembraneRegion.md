# TransMembraneRegion a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of the protein that is within a membrane|
|subDomain|SequenceFeatureCore|

## Properties

