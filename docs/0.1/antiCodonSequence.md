# antiCodonSequence a ObjectProperty

## Domain

definition: The sequence of the anticodon use [TGAC]<br>
[AntiCodon](/0.1/AntiCodon)

## Range

xsd:string

## Annotations


