# ProPeptide a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The part of a protein that is proteolytically cleaved during the protein's maturation, e.g. the internal residues of proinsulin, the N-terminal residues of pepsinogen. If the propeptide is a signal peptide please use the SingalPeptide class.|
|subDomain|SequenceFeatureCore|

## Properties

