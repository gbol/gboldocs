# InRangePosition a owl:Class extends [FuzzyPosition](/0.1/FuzzyPosition)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Location is somewhere between the begin and end.|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from the faldo ontology as it is.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#InRangePosition">http://biohackathon.org/resource/faldo#InRangePosition</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[endPosition](/0.1/endPosition)||1:1|xsd:Long|
|[beginPosition](/0.1/beginPosition)||1:1|xsd:Long|
