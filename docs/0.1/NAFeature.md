# NAFeature a owl:Class extends [Feature](/0.1/Feature)

## Subclasses

[TranscriptFeature](/0.1/TranscriptFeature)

[MiscFeature](/0.1/MiscFeature)

[Source](/0.1/Source)

[GenomicFeature](/0.1/GenomicFeature)

[SequenceAnnotation](/0.1/SequenceAnnotation)

## Annotations

|||
|-----|-----|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[expression](/0.1/expression)|Expression feature|0:N|[Expression](/0.1/Expression)|
|[mapLocation](/0.1/mapLocation)|The map location on the chromosome from which the sample was obtained.|0:1|xsd:string|
