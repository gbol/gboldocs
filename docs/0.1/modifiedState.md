# modifiedState a ObjectProperty

## Domain

definition: Short description denoted the state of the protein if the residue in the modified state. For example 'protein in active state'<br>
[ModifiedResidue](/0.1/ModifiedResidue)

## Range

xsd:string

## Annotations


