# numberOfSamples a ObjectProperty

## Domain

definition: Number of samples with data<br>
[Variation](/0.1/Variation)

## Range

xsd:integer

## Annotations


