# map a ObjectProperty

## Domain

definition: The map location on the chromosome from which the sample was obtained (using PCR).<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|map|
|skos:editorialNote|Added to NAFeature, each DNA feature can a chromosome map location|

