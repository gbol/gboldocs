# alternativeNames a ObjectProperty

## Domain

definition: Any alternative names of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

xsd:string

## Annotations


