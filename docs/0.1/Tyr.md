# Tyr a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|Y|
|ddbjLabel|Tyr|
|rdfs:label|Tyrosine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001446">http://purl.obolibrary.org/obo/SO_0001446</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q188017">http://www.wikidata.org/entity/Q188017</a>|

