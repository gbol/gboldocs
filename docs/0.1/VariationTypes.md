# VariationTypes a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|Type of the variation entry|

## skos:member

[SSR](/0.1/SSR)

[RFLP](/0.1/RFLP)

[InDel](/0.1/InDel)

[SNP](/0.1/SNP)

