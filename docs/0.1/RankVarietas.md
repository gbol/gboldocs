# RankVarietas a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: variates|
|parentRank|<a href="http://gbol.life/0.1/RankSubSpecies">http://gbol.life/0.1/RankSubSpecies</a>|
|rdfs:label|varietas|

