# recommendedName a ObjectProperty

## Domain

definition: The recommended name of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

xsd:string

## Annotations


