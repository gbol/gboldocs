# geographicalLocation a ObjectProperty

## Domain

definition: The geographical location(country or ocean) from which the sample is collected<br>
[Sample](/0.1/Sample)

## Range

[GeographicalLocation](/0.1/GeographicalLocation)

## Annotations


