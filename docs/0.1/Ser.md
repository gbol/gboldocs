# Ser a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|S|
|ddbjLabel|Ser|
|rdfs:label|Serine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001444">http://purl.obolibrary.org/obo/SO_0001444</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q183290">http://www.wikidata.org/entity/Q183290</a>|

