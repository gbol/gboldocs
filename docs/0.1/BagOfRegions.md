# BagOfRegions a owl:Class extends [CollectionOfRegions](/0.1/CollectionOfRegions)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Equal to the join in the GenBank format. Unordered set of regions.|
|subDomain|LocationCore|
|skos:editorialNote|Taken over from Faldo|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#BagOfRegions">http://biohackathon.org/resource/faldo#BagOfRegions</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[members](/0.1/members)||1:N|[Region](/0.1/Region)|
