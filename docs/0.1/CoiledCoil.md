# CoiledCoil a skos:Concept, [ProteinStructureType](/0.1/ProteinStructureType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A coiled coil region|

