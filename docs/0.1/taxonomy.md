# taxonomy a ObjectProperty

## Domain

definition: The taxonomy of the organism from which the sequence originates<br>
[Organism](/0.1/Organism)

## Range

[TaxonomyRef](/0.1/TaxonomyRef)

## Annotations


