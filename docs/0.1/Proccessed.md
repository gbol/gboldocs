# Proccessed a skos:Concept, [PseudoGeneType](/0.1/PseudoGeneType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|the pseudogene has arisen by reverse transcription of a mRNA into cDNA, followed by reintegration into the genome. Therefore, it has lost any intron/exon structure, and it might have a pseudo-polyA-tail.|
|ddbjLabel|processed|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000900">http://purl.obolibrary.org/obo/SO_0000900</a>|

