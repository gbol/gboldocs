# TranscriptFeature a owl:Class extends [NAFeature](/0.1/NAFeature)

## Subclasses

[PolyASite](/0.1/PolyASite)

[FivePrimeUTR](/0.1/FivePrimeUTR)

[ThreePrimeUTR](/0.1/ThreePrimeUTR)

[CDS](/0.1/CDS)

## Annotations

|||
|-----|-----|
|skos:definition|Features on a transcript|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000833">http://purl.obolibrary.org/obo/SO_0000833</a>|

## Properties

