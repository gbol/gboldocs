# Chromosome a owl:Class extends [CompleteNASequence](/0.1/CompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|A chromosomal sequence|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000340">http://purl.obolibrary.org/obo/SO_0000340</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q37748">http://www.wikidata.org/entity/Q37748</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[chromosome](/0.1/chromosome)|The chromosome name or number|0:1|xsd:string|
