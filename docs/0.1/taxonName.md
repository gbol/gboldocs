# taxonName a ObjectProperty

## Domain

definition: The name of the taxon<br>
[Taxon](/0.1/Taxon)

## Range

xsd:string

## Annotations


