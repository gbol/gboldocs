# SequenceTaggedSiteEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|STS|
|rdfs:label|Sequence Tagged Site|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2068713">http://www.wikidata.org/entity/Q2068713</a>|

