# MegaHit a owl:Class extends [ProvenanceAssembly](/0.1/ProvenanceAssembly)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An example entry for assembly information|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[provenanceNote](/0.1/provenanceNote)|Note on the provenance|0:1|xsd:string|
