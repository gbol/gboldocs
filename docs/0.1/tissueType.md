# tissueType a ObjectProperty

## Domain

definition: The tissue type from which the sample was obtained, use term from the 'BRENDA Tissue and Enzyme Source Ontology' whenever possible<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|tissue_type|

