# NonLtrRetrotransposonPolymericTract a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A polymeric tract, such as poly(dA), within a non_LTR_retrotransposon.|
|ddbjLabel|non_ltr_retrotransposon_polymeric_tract|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000433">http://purl.obolibrary.org/obo/SO_0000433</a>|

