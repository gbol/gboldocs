# Position a owl:Class

## Subclasses

[FuzzyPosition](/0.1/FuzzyPosition)

[ExactPosition](/0.1/ExactPosition)

## Annotations

|||
|-----|-----|
|rdfs:comment|For nucleotide sequences positions always counting always from 5 prime to 3 prime and for protein from n terminal to 5 terminal. Counting is one-based inclusive.|
|subDomain|LocationCore|
|skos:editorialNote|Counting same as in Faldo and Genbank.|
|skos:exactMatch|<a href="http://biohackathon.org/resource/faldo#Position">http://biohackathon.org/resource/faldo#Position</a>|

## Properties

