# genotype a ObjectProperty

## Domain

definition: Genotypes of the sample the variant was called on<br>
[Variation](/0.1/Variation)

definition: Genotypes of the sample the variant was called on<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

[VariantGenotype](/0.1/VariantGenotype)

## Annotations


