# PrimerBinding a owl:Class extends [ArtificialRecognizedRegion](/0.1/ArtificialRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Non-covalent primer binding site for initiation of replication, transcription, or reverse transcription;includes site(s) for synthetic e.g., PCR primer elements;|
|rdfs:comment|Used to annotate the site on a given sequence to which a primer molecule binds - not intended to represent the sequence of the primer molecule itself|
|ddbjLabel|primer_bind|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|PCR components and reaction times maybe stored under the \"/PCR_conditions\" qualifier; since PCR reactions most often involve pairs of primers,a single primer_bind key may use the order() operator with two locations, or a pair of primer_bind keys may be used.|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0005850">http://purl.obolibrary.org/obo/SO_0005850</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7243406">http://www.wikidata.org/entity/Q7243406</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[PCRConditions](/0.1/PCRConditions)|A textual description of the PCR conditions needed for this primer|0:1|xsd:string|
