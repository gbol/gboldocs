# readLength a ObjectProperty

## Domain

[LocalSequenceFile](/0.1/LocalSequenceFile)

[ReadInformation](/0.1/ReadInformation)

[RemoteSequenceFile](/0.1/RemoteSequenceFile)

## Range

xsd:long

## Annotations


