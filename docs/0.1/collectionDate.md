# collectionDate a ObjectProperty

## Domain

definition: The date and time on which the specimen was collected.<br>
[Sample](/0.1/Sample)

## Range

xsd:dateTime

## Annotations

|||
|-----|-----|
|ddbjLabel|collection_date|

