# geneSymbol a ObjectProperty

## Domain

definition: Symbol of the gene (not the symbol of the resulting protein)<br>
[Gene](/0.1/Gene)

## Range

xsd:string

## Annotations


