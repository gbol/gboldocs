# transcript a ObjectProperty

## Domain

definition: All transcripts that are derived from the gene<br>
[Gene](/0.1/Gene)

## Range

[Transcript](/0.1/Transcript)

## Annotations


