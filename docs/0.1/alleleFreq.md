# alleleFreq a ObjectProperty

## Domain

definition: Frequency in which the alternate allele is found<br>
[Variation](/0.1/Variation)

## Range

xsd:string

## Annotations


