# ImprintingControlRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A regulatory region that controls epigenetic imprinting and affects the expression of target genes in an allele- or parent-of-origin-specific manner. Associated regulatory elements may include differentially methylated regions and non-coding RNAs.|
|ddbjLabel|imprinting_control_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002191">http://purl.obolibrary.org/obo/SO_0002191</a>|

