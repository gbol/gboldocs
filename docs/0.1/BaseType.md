# BaseType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The set of modified nucleic acids|
|subDomain|SequenceFeatureCore|

## skos:member

[BaseM3c](/0.1/BaseM3c)

[BaseM7g](/0.1/BaseM7g)

[BaseMs2i6a](/0.1/BaseMs2i6a)

[BaseMv](/0.1/BaseMv)

[BaseMan_q](/0.1/BaseMan_q)

[BaseX](/0.1/BaseX)

[BaseT](/0.1/BaseT)

[BaseDhu](/0.1/BaseDhu)

[BaseYw](/0.1/BaseYw)

[BaseP](/0.1/BaseP)

[BaseUm](/0.1/BaseUm)

[BaseQ](/0.1/BaseQ)

[BaseM5c](/0.1/BaseM5c)

[BaseS4u](/0.1/BaseS4u)

[BaseMam5u](/0.1/BaseMam5u)

[BaseGal_q](/0.1/BaseGal_q)

[BaseI](/0.1/BaseI)

[BaseM4c](/0.1/BaseM4c)

[BaseT6a](/0.1/BaseT6a)

[BaseMcm5s2u](/0.1/BaseMcm5s2u)

[BaseM22g](/0.1/BaseM22g)

[BaseM5u](/0.1/BaseM5u)

[BaseO5u](/0.1/BaseO5u)

[BaseChm5u](/0.1/BaseChm5u)

[BaseTm](/0.1/BaseTm)

[BaseI6a](/0.1/BaseI6a)

[BaseCmnm5u](/0.1/BaseCmnm5u)

[BaseM6a](/0.1/BaseM6a)

[BaseM1a](/0.1/BaseM1a)

[BaseOsyw](/0.1/BaseOsyw)

[BaseM1g](/0.1/BaseM1g)

[BaseM1f](/0.1/BaseM1f)

[BaseMo5u](/0.1/BaseMo5u)

[BaseCm](/0.1/BaseCm)

[BaseM1i](/0.1/BaseM1i)

[BaseMs2t6a](/0.1/BaseMs2t6a)

[BaseGm](/0.1/BaseGm)

[BaseMt6a](/0.1/BaseMt6a)

[BaseMam5s2u](/0.1/BaseMam5s2u)

[BaseOther](/0.1/BaseOther)

[BaseAc4c](/0.1/BaseAc4c)

[BaseS2t](/0.1/BaseS2t)

[BaseS2u](/0.1/BaseS2u)

[BaseM2a](/0.1/BaseM2a)

[BaseM2g](/0.1/BaseM2g)

[BaseMcm5u](/0.1/BaseMcm5u)

[BaseS2c](/0.1/BaseS2c)

[BaseCmnm5s2u](/0.1/BaseCmnm5s2u)

[BaseFm](/0.1/BaseFm)

