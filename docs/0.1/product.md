# product a ObjectProperty

## Domain

definition: Short description of the product resulting from the feature<br>
[tmRNA](/0.1/tmRNA)

definition: Short description of the product resulting from the feature<br>
[MiscRna](/0.1/MiscRna)

definition: Short description of the product resulting from the feature<br>
[rRNA](/0.1/rRNA)

definition: Short description of the product resulting from the feature<br>
[CDS](/0.1/CDS)

definition: Short description of the product resulting from the feature<br>
[tRNA](/0.1/tRNA)

definition: Short description of the product resulting from the feature<br>
[ncRNA](/0.1/ncRNA)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|product|
|skos:editorialNote|Product description only present in CDS|

