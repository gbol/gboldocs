# BaseMo5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|mo5u|
|rdfs:label|5-methoxyuridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632761">http://www.wikidata.org/entity/Q15632761</a>|

