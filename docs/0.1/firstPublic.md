# firstPublic a ObjectProperty

## Domain

definition: The date at which the record was first published<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:dateTime

## Annotations


