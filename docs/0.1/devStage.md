# devStage a ObjectProperty

## Domain

definition: The developmental stage at which the organism was when the sample was obtained<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|dev_stage|

