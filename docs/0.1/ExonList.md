# ExonList a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[exon](/0.1/exon)||1:N|[Exon](/0.1/Exon)|
|[phenotype](/0.1/phenotype)|Associated phenotypic property|0:1|xsd:string|
|[provenance](/0.1/provenance)|The provenance of the feature|0:1|[Provenance](/0.1/Provenance)|
