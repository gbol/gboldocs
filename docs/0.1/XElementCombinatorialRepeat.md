# XElementCombinatorialRepeat a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|An X element combinatorial repeat is a repeat region located between the X element and the telomere or adjacent Y' element.|
|ddbjLabel|x_element_combinatorial_repeat|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001484">http://purl.obolibrary.org/obo/SO_0001484</a>|

