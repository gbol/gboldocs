# sequence a ObjectProperty

## Domain

definition: The sequence, encoded with the default DNA or protein encoding<br>
[Sequence](/0.1/Sequence)

definition: The sequence, encoded with the default DNA or protein encoding<br>
[sapp:TMHMM](/sapp/0.1/TMHMM)

## Range

xsd:string

## Annotations


