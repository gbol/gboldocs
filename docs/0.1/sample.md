# sample a ObjectProperty

## Domain

definition: Sample from which this piece of sequence originates from.<br>
[Sequence](/0.1/Sequence)

definition: Sample from which this piece of sequence originates from.<br>
[Library](/0.1/Library)

definition: Sample from which this piece of sequence originates from.<br>
[Source](/0.1/Source)

## Range

[Sample](/0.1/Sample)

## Annotations


