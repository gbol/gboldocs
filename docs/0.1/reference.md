# reference a ObjectProperty

## Domain

definition: The actual reference of the citation<br>
[ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

definition: The actual reference of the citation<br>
[Citation](/0.1/Citation)

## Range

[bibo:Document](/ontology/bibo/Document)

## Annotations


