# EtAsn a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|EtAsn|
|rdfs:label|N-Ethylasparagine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133211">http://www.wikidata.org/entity/Q27133211</a>|

