# operon a ObjectProperty

## Domain

definition: Operon to which the genetic element belongs to<br>
[GenomicFeature](/0.1/GenomicFeature)

## Range

[Operon](/0.1/Operon)

## Annotations

|||
|-----|-----|
|ddbjLabel|operon|
|skos:editorialNote|Link to operon added in genomic feature|

