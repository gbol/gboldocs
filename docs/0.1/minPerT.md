# minPerT a ObjectProperty

## Domain

definition: Minimum percentage threshold<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:float

## Annotations


