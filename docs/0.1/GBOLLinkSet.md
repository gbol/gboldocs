# GBOLLinkSet a owl:Class extends [prov:Entity](/ns/prov/Entity)

## Subclasses

[AnnotationLinkSet](/0.1/AnnotationLinkSet)

## Annotations

|||
|-----|-----|
|skos:definition|Describes and contains the dataset-wise provenance of a set of XREF elements within a GBOLDataSet.|
|rdfs:comment|All databases references by the XREF entities within this LinkSet must be referenced by the target property.|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[target](/0.1/target)|The set of database that are target of the references associated to this link set|1:N|[Database](/0.1/Database)|
