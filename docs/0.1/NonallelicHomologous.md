# NonallelicHomologous a skos:Concept, [RecombinationType](/0.1/RecombinationType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A genomic region at a non-allelic position where exchange of genetic material happens as a result of homologous recombination.|
|ddbjLabel|non_allelic_homologous|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002094">http://purl.obolibrary.org/obo/SO_0002094</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7049125">http://www.wikidata.org/entity/Q7049125</a>|

