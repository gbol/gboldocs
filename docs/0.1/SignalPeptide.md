# SignalPeptide a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The sequence holding the targeting information used to post-translationally export or import a sequence into any of the organels. This amino acid terminal sequence is typically removed during or shortly after import or export.|
|ddbjLabel|sig_peptide|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000418">http://purl.obolibrary.org/obo/SO_0000418</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[signalTarget](/0.1/signalTarget)|If known specify target, use a 'cellular component' from the GO terms ontology|0:1|IRI|
