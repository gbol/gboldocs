# Met a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|M|
|ddbjLabel|Met|
|rdfs:label|Methionine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001442">http://purl.obolibrary.org/obo/SO_0001442</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q22124685">http://www.wikidata.org/entity/Q22124685</a>|

