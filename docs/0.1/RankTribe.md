# RankTribe a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: tribe|
|parentRank|<a href="http://gbol.life/0.1/RankSubFamily">http://gbol.life/0.1/RankSubFamily</a>|
|rdfs:label|tribe|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q227936">http://www.wikidata.org/entity/Q227936</a>|

