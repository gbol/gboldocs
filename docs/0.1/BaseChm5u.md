# BaseChm5u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|chm5u|
|rdfs:label|5-(carboxyhydroxylmethyl)uridine|

