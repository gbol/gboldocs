# Xle a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|J|
|ddbjLabel|Xle|
|rdfs:label|Isoleucine or Leucine|

