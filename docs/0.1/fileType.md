# fileType a ObjectProperty

## Domain

definition: Optionally set the fileType<br>
[LocalDataFile](/0.1/LocalDataFile)

definition: Optionally set the fileType<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

[FileType](/0.1/FileType)

## Annotations


