# Lys a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|K|
|ddbjLabel|Lys|
|rdfs:label|Lysine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001450">http://purl.obolibrary.org/obo/SO_0001450</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q6710212">http://www.wikidata.org/entity/Q6710212</a>|

