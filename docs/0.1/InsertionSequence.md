# InsertionSequence a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|insertion sequence|
|rdfs:label|insertion sequence|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001837">http://purl.obolibrary.org/obo/SO_0001837</a>|

