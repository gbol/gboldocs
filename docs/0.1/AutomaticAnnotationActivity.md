# AutomaticAnnotationActivity a owl:Class extends [AnnotationActivity](/0.1/AnnotationActivity)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[wasAssociatedWith](/0.1/wasAssociatedWith)|The agent which be performed this activity|0:1|[AnnotationSoftware](/0.1/AnnotationSoftware)|
|[parameterString](/0.1/parameterString)|The parameter string as used to start the software for the this activity, parameters referencing to another dataset used must be specified with {IRI}. So for example '-threshold 0.05 -i {:datasetIn1}'. Each of referenced input datasets must be also be referenced with the prov:used property|1:1|xsd:string|
