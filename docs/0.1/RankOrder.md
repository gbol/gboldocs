# RankOrder a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: order|
|parentRank|<a href="http://gbol.life/0.1/RankSuperOrder">http://gbol.life/0.1/RankSuperOrder</a>|
|rdfs:label|order|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q36602">http://www.wikidata.org/entity/Q36602</a>|

