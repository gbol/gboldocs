# ecotype a ObjectProperty

## Domain

definition: The specific name of the host from which the sample has been collected, where the name indicates to which habitat/ecological environment the host has adopted to. Example: 'Columbia', which indicate that this Arabidopsis adopted itself with a hairier phenotype to deal with the especially sunny habitat.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|ecotype|

