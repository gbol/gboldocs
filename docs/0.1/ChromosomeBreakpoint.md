# ChromosomeBreakpoint a skos:Concept, [RecombinationType](/0.1/RecombinationType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A chromosomal region that may sustain a double-strand break, resulting in a recombination event.|
|ddbjLabel|chromosome_breakpoint|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001021">http://purl.obolibrary.org/obo/SO_0001021</a>|

