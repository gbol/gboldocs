# guideRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|short 3'-uridylated RNA that can form a duplex with a stretch of mature edited mRNA|
|ddbjLabel|guide_RNA|

