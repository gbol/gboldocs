# CentromericRepeat a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat region found within the modular centromere.|
|ddbjLabel|centromeric_repeat|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001797">http://purl.obolibrary.org/obo/SO_0001797</a>|

