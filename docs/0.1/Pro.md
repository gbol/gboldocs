# Pro a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|P|
|ddbjLabel|Pro|
|rdfs:label|Proline|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001439">http://purl.obolibrary.org/obo/SO_0001439</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q20035886">http://www.wikidata.org/entity/Q20035886</a>|

