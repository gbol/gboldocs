# RNaseMRPRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|RNA molecule essential for the catalytic activity of RNase MRP, an enzymatically active ribonucleoprotein with two distinct roles in eukaryotes. In mitochondria it plays a direct role in the initiation of mitochondrial DNA replication, while in the nucleus it is involved in precursor rRNA processing.|
|ddbjLabel|RNase_MRP_RNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000385">http://purl.obolibrary.org/obo/SO_0000385</a>|

