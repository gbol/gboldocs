# Syntype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|syntype|
|rdfs:label|syntype|

