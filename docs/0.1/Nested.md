# Nested a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|nested|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001649">http://purl.obolibrary.org/obo/SO_0001649</a>|

