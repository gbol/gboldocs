# origin a ObjectProperty

## Domain

definition: Link to the data-wise provenance<br>
[Provenance](/0.1/Provenance)

[XRefProvenance](/0.1/XRefProvenance)

## Range

[AnnotationResult](/0.1/AnnotationResult)

[AnnotationLinkSet](/0.1/AnnotationLinkSet)

## Annotations


