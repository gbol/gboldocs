# British_Guiana a skos:Concept, [Country](/0.1/Country)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|British Guiana|
|rdfs:label|British Guiana|
|owl:deprecated|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|

