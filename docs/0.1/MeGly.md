# MeGly a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|MeGly|
|rdfs:label|N-Methylglycine (sarcosine)|

