# phredLikelihood a ObjectProperty

## Domain

definition: phred scaled likelihood<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:integer

## Annotations


