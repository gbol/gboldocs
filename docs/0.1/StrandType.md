# StrandType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The list of types for a nucleic acid strand|
|subDomain|SequenceFeatureCore|

## skos:member

[SingleStrandedRNA](/0.1/SingleStrandedRNA)

[ComplementaryDNA](/0.1/ComplementaryDNA)

[SingleStrandedDNA](/0.1/SingleStrandedDNA)

[DoubleStrandedDNA](/0.1/DoubleStrandedDNA)

[DoubleStrandedRNA](/0.1/DoubleStrandedRNA)

