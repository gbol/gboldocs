# piRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small RNA molecule, termed Piwi-interacting RNA, expressed in testes and forming RNA-protein complex with Piwi protein; purification of these complexes has revealed that Piwi-interacting RNA oligonucleotides are approximately 24-32 nucleotides long|
|ddbjLabel|piRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001035">http://purl.obolibrary.org/obo/SO_0001035</a>|

