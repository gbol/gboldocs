# DNaseIHypersensitiveSite a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|DNA region representing open chromatin structure that is hypersensitive to digestion by DNase I.|
|ddbjLabel|DNase_I_hypersensitive_site|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000685">http://purl.obolibrary.org/obo/SO_0000685</a>|

