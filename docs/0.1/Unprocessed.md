# Unprocessed a skos:Concept, [PseudoGeneType](/0.1/PseudoGeneType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|the pseudogene has arisen from a copy of the parent gene by duplication followed by accumulation of random mutation. The changes, compared to their functional homologue, include insertions, deletions, premature stop codons, frame-shifts and a higher proportion of non-synonymous versus synonymous substitutions.|
|ddbjLabel|unprocessed|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001760">http://purl.obolibrary.org/obo/SO_0001760</a>|

