# Transcript a owl:Class extends [NASequence](/0.1/NASequence)

## Subclasses

[MaturedRNA](/0.1/MaturedRNA)

[PrecursorRNA](/0.1/PrecursorRNA)

[MiscRna](/0.1/MiscRna)

## Annotations

|||
|-----|-----|
|skos:definition|Sequence of nucleotide molecule created after transcription|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|No provenance included here, as sequence object represent a sequence that might originate either from a sample or an exonlist. The exon list caries the provenance, as this the element that is/can be predicted.|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000673">http://purl.obolibrary.org/obo/SO_0000673</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[exonList](/0.1/exonList)|This list off exons that form this transcript, in case of prokaryote it contains one element|0:1|[ExonList](/0.1/ExonList)|
|[feature](/0.1/feature)|The set of features annotating the sequence|0:N|[TranscriptFeature](/0.1/TranscriptFeature)|
