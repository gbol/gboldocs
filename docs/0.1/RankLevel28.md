# RankLevel28 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 28|
|parentRank|<a href="http://gbol.life/0.1/RankLevel27">http://gbol.life/0.1/RankLevel27</a>|
|rdfs:label|Rank Level 28|

