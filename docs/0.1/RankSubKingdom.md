# RankSubKingdom a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub kingdom|
|parentRank|<a href="http://gbol.life/0.1/RankKingdom">http://gbol.life/0.1/RankKingdom</a>|
|rdfs:label|sub kingdom|

