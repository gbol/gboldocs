# ReplicationOrigin a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Origin of replication; starting site for duplication of nucleic acid to give two identical copies;|
|rdfs:comment|/direction has valid values: RIGHT, LEFT, or BOTH.|
|ddbjLabel|rep_origin|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000296">http://purl.obolibrary.org/obo/SO_0000296</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[replicationDirection](/0.1/replicationDirection)|The direction of the replication can be forward, reverse or both|0:1|[StrandPosition](/0.1/StrandPosition)|
