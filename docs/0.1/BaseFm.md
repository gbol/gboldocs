# BaseFm a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|fm|
|rdfs:label|2'-O-methylpseudouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109154">http://www.wikidata.org/entity/Q27109154</a>|

