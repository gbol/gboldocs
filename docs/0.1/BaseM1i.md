# BaseM1i a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m1i|
|rdfs:label|1-methylinosine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001278">http://purl.obolibrary.org/obo/SO_0001278</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q161650">http://www.wikidata.org/entity/Q161650</a>|

