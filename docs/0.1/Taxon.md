# Taxon a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Simple taxon class to define a taxon in a lineage|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rdfs:subClassOf](/2000/01/rdf-schema/subClassOf)|Parental rank|0:1|[Taxon](/0.1/Taxon)|
|[taxonName](/0.1/taxonName)|The name of the taxon|1:1|xsd:string|
|[taxonRank](/0.1/taxonRank)|The rank of the taxon|1:1|[Rank](/0.1/Rank)|
