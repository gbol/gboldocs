# markIfMoreThen1 a ObjectProperty

## Domain

definition: Mark if more than one<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:boolean

## Annotations


