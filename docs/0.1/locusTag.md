# locusTag a ObjectProperty

## Domain

definition: Locus tag of the gene<br>
[Gene](/0.1/Gene)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|locus_tag|
|skos:editorialNote|Added to gene class, any sub elements related to gene should have the same locus tag in the genbank file|

