# RankLevel26 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 26|
|parentRank|<a href="http://gbol.life/0.1/RankLevel25">http://gbol.life/0.1/RankLevel25</a>|
|rdfs:label|Rank Level 26|

