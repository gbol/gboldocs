# ProteinStructure a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Defines the secondary structure of the protein for the given region|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[structureType](/0.1/structureType)|The type of secondary structure|1:1|[ProteinStructureType](/0.1/ProteinStructureType)|
