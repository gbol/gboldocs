# ProteinDomain a owl:Class extends [ConservedRegion](/0.1/ConservedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A conserved domain or family within a protein|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q898273">http://www.wikidata.org/entity/Q898273</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[secondarySignature](/0.1/secondarySignature)|Secondary signature of the domain typically the interproscan ID|0:N|IRI|
|[signatureDesc](/0.1/signatureDesc)|Optional short description of the signature|0:1|xsd:string|
|[domainType](/0.1/domainType)|Option to denote the type of domain|0:1|[ProteinDomainType](/0.1/ProteinDomainType)|
|[signature](/0.1/signature)|The reference to signature of the domain (Gene3d, Pfam, etc)|1:1|IRI|
