# homozygousSamples a ObjectProperty

## Domain

definition: Number of Homozygous variant samples<br>
[Variation](/0.1/Variation)

## Range

xsd:integer

## Annotations


