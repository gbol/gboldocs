# Experiment a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Insilico experiment performed such as RNAseq mapping|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q101965">http://www.wikidata.org/entity/Q101965</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[inputReads](/0.1/inputReads)|Read data used as input|1:1|[ReadInformation](/0.1/ReadInformation)|
|[provenance](/0.1/provenance)|The provenance of the feature|1:1|[Provenance](/0.1/Provenance)|
