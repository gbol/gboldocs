# XRefProvenance a owl:Class extends [Provenance](/0.1/Provenance)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|ElementWiseProv|
|rdfs:label|XRefProvenance|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[origin](/0.1/origin)||1:1|[AnnotationLinkSet](/0.1/AnnotationLinkSet)|
