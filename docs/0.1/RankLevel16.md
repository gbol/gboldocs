# RankLevel16 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 16|
|parentRank|<a href="http://gbol.life/0.1/RankLevel15">http://gbol.life/0.1/RankLevel15</a>|
|rdfs:label|Rank Level 16|

