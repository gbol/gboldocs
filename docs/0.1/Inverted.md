# Inverted a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The sequence is complementarily repeated on the opposite strand. It is a palindrome, and it may, or may not be hyphenated. Examples: GCTGATCAGC, or GCTGA-----TCAGC.|
|ddbjLabel|inverted|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000294">http://purl.obolibrary.org/obo/SO_0000294</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4852694">http://www.wikidata.org/entity/Q4852694</a>|

