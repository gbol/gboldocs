# proviralExtraction a ObjectProperty

## Domain

definition: Indicate that this sample is an proviral sequence extracted from another organism using PCR.<br>
[Sample](/0.1/Sample)

## Range

xsd:boolean

## Annotations


