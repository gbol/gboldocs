# autocatalyticallySplicedIntron a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A self spliced intron.|
|skos:definition|self-splicing intron|
|ddbjLabel|autocatalytically_spliced_intron|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000588">http://purl.obolibrary.org/obo/SO_0000588</a>|

