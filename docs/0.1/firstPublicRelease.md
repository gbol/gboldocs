# firstPublicRelease a ObjectProperty

## Domain

definition: The version of the record when it was first published<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:integer

## Annotations


