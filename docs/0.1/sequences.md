# sequences a ObjectProperty

## Domain

definition: All dna objects included within the GBOL data set<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[Sequence](/0.1/Sequence)

## Annotations


