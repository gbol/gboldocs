# NonLTRRetrotransposon a skos:Concept, [MobileElementType](/0.1/MobileElementType) extends [Retrotransposon](/0.1/Retrotransposon)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A retrotransposon without long terminal repeat sequences.|
|ddbjLabel|non-LTR retrotransposon|
|rdfs:label|non-LTR retrotransposon|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000189">http://purl.obolibrary.org/obo/SO_0000189</a>|

