# macronuclear a ObjectProperty

## Domain

definition: Indicates that this sequence from the macro-nuclear DNA from an organism which undergoes chromosomal differentiation between macro-nuclear and micro-nuclear stages.<br>
[NASequence](/0.1/NASequence)

## Range

xsd:boolean

## Annotations

|||
|-----|-----|
|ddbjLabel|macronuclear|

