# miRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small, ~22-nt, RNA molecule, termed microRNA, produced from precursor molecules that can form local hairpin structures, which ordinarily are processed (via the Dicer pathway) such that a single miRNA molecule accumulates from one arm of a hairpin precursor molecule. MicroRNAs may trigger the cleavage of their target molecules or act as translational repressors.|
|ddbjLabel|miRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000276">http://purl.obolibrary.org/obo/SO_0000276</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q310899">http://www.wikidata.org/entity/Q310899</a>|

