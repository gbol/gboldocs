# FourHyp a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|FourHyp|
|rdfs:label|4-Hydroxyproline|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q411237">http://www.wikidata.org/entity/Q411237</a>|

