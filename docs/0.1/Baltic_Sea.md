# Baltic_Sea a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Baltic Sea|
|rdfs:label|Baltic Sea|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q545">http://www.wikidata.org/entity/Q545</a>|

