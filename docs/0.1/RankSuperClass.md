# RankSuperClass a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: super class|
|parentRank|<a href="http://gbol.life/0.1/RankSubPhylum">http://gbol.life/0.1/RankSubPhylum</a>|
|rdfs:label|super class|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q3504061">http://www.wikidata.org/entity/Q3504061</a>|

