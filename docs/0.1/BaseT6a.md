# BaseT6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|t6a|
|rdfs:label|N-((9-beta-D-ribofuranosylpurine-6-yl)carbamoyl)threonine|

