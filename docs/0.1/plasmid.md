# plasmid a ObjectProperty

## Domain

definition: The name or identifier of plasmid from which the sample was obtained (using PCR).<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|plasmid|
|skos:editorialNote|Present on the plasmid as 'plasmidName' and plasmid on the sample.|

