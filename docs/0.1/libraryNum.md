# libraryNum a ObjectProperty

## Domain

definition: Library number<br>
[Library](/0.1/Library)

## Range

xsd:integer

## Annotations


