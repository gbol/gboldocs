# secondarySignature a ObjectProperty

## Domain

definition: Secondary signature of the domain typically the interproscan ID<br>
[ProteinDomain](/0.1/ProteinDomain)

## Range

xsd:anyURI

## Annotations


