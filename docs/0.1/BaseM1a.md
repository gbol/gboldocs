# BaseM1a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m1a|
|rdfs:label|1-methyladenosine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001295">http://purl.obolibrary.org/obo/SO_0001295</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q161643">http://www.wikidata.org/entity/Q161643</a>|

