# Southern_Ocean a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Southern Ocean|
|rdfs:label|Southern Ocean|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7354">http://www.wikidata.org/entity/Q7354</a>|

