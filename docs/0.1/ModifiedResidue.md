# ModifiedResidue a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Denotes a modified residue or residue that is typically modified within a protein|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[modifiedState](/0.1/modifiedState)|Short description denoted the state of the protein if the residue in the modified state. For example 'protein in active state'|0:1|xsd:string|
|[residue](/0.1/residue)|The residue type as defined within the 'Protein Modification Ontology' (http://www.psidev.info/MOD)|1:1|IRI|
|[unmodifiedState](/0.1/unmodifiedState)|Short description denoted the state of the protein if the residue in the unmodified state. For example 'protein in inactive state'|0:1|xsd:string|
|[modificationFunction](/0.1/modificationFunction)|If applicable, the biological process in which this modification is involved. Use a Biological Process from the GO ontology.|0:1|IRI|
