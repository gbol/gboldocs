# MiscRecomb a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Site of any generalized, site-specific or replicativere combination event where there is a breakage and reunion of duplex DNA that cannot be described by other recombination keys or qualifiers of source key(/proviral);|
|ddbjLabel|misc_recomb|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001021">http://purl.obolibrary.org/obo/SO_0001021</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[recombinationType](/0.1/recombinationType)|The type of recombination|1:1|[RecombinationType](/0.1/RecombinationType)|
