# RankLevel6 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 6|
|parentRank|<a href="http://gbol.life/0.1/RankLevel5">http://gbol.life/0.1/RankLevel5</a>|
|rdfs:label|Rank Level 6|

