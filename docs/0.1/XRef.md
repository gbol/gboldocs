# XRef a owl:Class extends [Qualifier](/0.1/Qualifier)

## Subclasses

[TaxonomyRef](/0.1/TaxonomyRef)

## Annotations

|||
|-----|-----|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[provenance](/0.1/provenance)|The provenance of the feature|1:1|[XRefProvenance](/0.1/XRefProvenance)|
|[db](/0.1/db)|The database to with the links link to|1:1|[Database](/0.1/Database)|
|[secondaryAccession](/0.1/secondaryAccession)|The optional secondary accession ids|0:N|xsd:string|
|[accession](/0.1/accession)|The accession id of the entry referenced|1:1|xsd:string|
