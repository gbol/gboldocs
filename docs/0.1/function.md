# function a ObjectProperty

## Domain

definition: Short description of the function<br>
[Sequence](/0.1/Sequence)

definition: Short description of the function<br>
[Feature](/0.1/Feature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|function|
|skos:editorialNote|Added feature, every feature can have a textual function description, this is more broad then what genbank actually has|

