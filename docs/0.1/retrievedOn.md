# retrievedOn a ObjectProperty

## Domain

definition: Optional a time point at which the file was downloaded<br>
[LocalDataFile](/0.1/LocalDataFile)

definition: Optional a time point at which the file was downloaded<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

xsd:dateTime

## Annotations


