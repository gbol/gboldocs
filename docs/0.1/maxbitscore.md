# maxbitscore a ObjectProperty

## Domain

definition: The maximum bit score obtained<br>
[sapp:ENZDP](/sapp/0.1/ENZDP)

## Range

xsd:float

## Annotations


