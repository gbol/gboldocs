# BindingSite a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

[MoietyBindingSite](/0.1/MoietyBindingSite)

[ProteinInteractionSite](/0.1/ProteinInteractionSite)

[NABindingSite](/0.1/NABindingSite)

## Annotations

|||
|-----|-----|
|skos:definition|Region(s) of the protein that binds some other entity.|
|rdfs:comment|Use collection of regions to denote all elements that bind the same element.|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000409">http://purl.obolibrary.org/obo/SO_0000409</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q616005">http://www.wikidata.org/entity/Q616005</a>|

## Properties

