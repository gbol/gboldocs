# AnnotationSoftware a owl:Class extends [prov:SoftwareAgent](/ns/prov/SoftwareAgent)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[commitId](/0.1/commitId)|The String that uniquely identifies the last commit of the code used.|0:1|xsd:string|
|[version](/0.1/version)|The version of the resource|1:1|xsd:string|
|[codeRepository](/0.1/codeRepository)|The (GIT) repository containing the code of the tool|0:1|IRI|
|[foaf:homepage](/foaf/0.1/homepage)|A homepage for some thing.|0:1|xsd:string|
