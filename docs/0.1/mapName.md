# mapName a ObjectProperty

## Domain

definition: Name of the map the QTL were mapped on<br>
[QTLMap](/0.1/QTLMap)

## Range

xsd:string

## Annotations


