# SSR a skos:Concept, [VariationTypes](/0.1/VariationTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Simple Sequence Repeat|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q265193">http://www.wikidata.org/entity/Q265193</a>|

