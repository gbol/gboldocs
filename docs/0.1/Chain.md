# Chain a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A chain within the mature protein|
|subDomain|SequenceFeatureCore|

## Properties

