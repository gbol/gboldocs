# includedRegion a ObjectProperty

## Domain

definition: The included region of referenced sequence, only exact position must be used to denote the start en end region, reference property should be empty. Use the strand property to include forward or reverse complement strand.<br>
[PartSequence](/0.1/PartSequence)

## Range

[Region](/0.1/Region)

## Annotations


