# feature a ObjectProperty

## Domain

definition: The set of features annotating the sequence<br>
[Sequence](/0.1/Sequence)

definition: The set of features annotating the sequence<br>
[Transcript](/0.1/Transcript)

definition: The set of features annotating the sequence<br>
[Protein](/0.1/Protein)

definition: The set of features annotating the sequence<br>
[NASequence](/0.1/NASequence)

## Range

[Feature](/0.1/Feature)

[TranscriptFeature](/0.1/TranscriptFeature)

[ProteinFeature](/0.1/ProteinFeature)

[NAFeature](/0.1/NAFeature)

## Annotations


