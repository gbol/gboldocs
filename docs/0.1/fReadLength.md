# fReadLength a ObjectProperty

## Domain

definition: Forward read length<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


