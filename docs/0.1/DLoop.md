# DLoop a owl:Class extends [BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Displacement loop; a region within mitochondrial DNA in which a short stretch of RNA is paired with one strand of DNA, displacing the original partner DNA strand in this region; also used to describe the displacement of a region of one strand of duplex DNA by a single stranded invader in the reaction catalyzed by RecA protein.|
|ddbjLabel|D-loop|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000297">http://purl.obolibrary.org/obo/SO_0000297</a>|

## Properties

