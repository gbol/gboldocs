# lncRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|long non-coding RNA; such molecules are generally defined as having a length greater than 200bp and do not fit into any other ncRNA class.|
|ddbjLabel|lncRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001877">http://purl.obolibrary.org/obo/SO_0001877</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15087973">http://www.wikidata.org/entity/Q15087973</a>|

