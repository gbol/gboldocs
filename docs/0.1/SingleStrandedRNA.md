# SingleStrandedRNA a skos:Concept, [StrandType](/0.1/StrandType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A single stranded RNA molecule|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q55978912">http://www.wikidata.org/entity/Q55978912</a>|

