# ComplementaryDNA a skos:Concept, [StrandType](/0.1/StrandType) extends [DoubleStrandedDNA](/0.1/DoubleStrandedDNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A double stranded DNA molecule that is a complementary DNA of an RNA molecule.|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q283478">http://www.wikidata.org/entity/Q283478</a>|

