# haplotype a ObjectProperty

## Domain

definition: For the host from which the sample was obtained the name for a combination of alleles that are linked together on the same physical chromosome. In the absence of recombination, each haplotype is inherited as a unit, and may be used to track gene flow in populations.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|haplotype|

