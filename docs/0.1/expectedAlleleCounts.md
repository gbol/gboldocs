# expectedAlleleCounts a ObjectProperty

## Domain

definition: Expected allele counts(comma separated)<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


