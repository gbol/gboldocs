# keywords a ObjectProperty

## Domain

definition: A list of keyword describing the content of the GBOL data set<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

xsd:string

## Annotations


