# MaturedRNA a owl:Class extends [Transcript](/0.1/Transcript)

## Subclasses

[tmRNA](/0.1/tmRNA)

[rRNA](/0.1/rRNA)

[mRNA](/0.1/mRNA)

[tRNA](/0.1/tRNA)

[ncRNA](/0.1/ncRNA)

## Annotations

|||
|-----|-----|
|skos:definition|A sequence of a matured transcript|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000233">http://purl.obolibrary.org/obo/SO_0000233</a>|

## Properties

