# xref a ObjectProperty

## Domain

definition: Reference to other entries holding entries with related information in other databases<br>
[Sequence](/0.1/Sequence)

definition: Reference to other entries holding entries with related information in other databases<br>
[Feature](/0.1/Feature)

definition: Reference to other entries holding entries with related information in other databases<br>
[Sample](/0.1/Sample)

definition: Reference to other entries holding entries with related information in other databases<br>
[GBOLDataSet](/0.1/GBOLDataSet)

definition: Reference to other entries holding entries with related information in other databases<br>
[sapp:InterProScanModels](/sapp/0.1/InterProScanModels)

## Range

[XRef](/0.1/XRef)

## Annotations


