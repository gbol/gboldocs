# chromosome a ObjectProperty

## Domain

definition: The chromosome name or number<br>
[Sample](/0.1/Sample)

definition: The chromosome name or number<br>
[Chromosome](/0.1/Chromosome)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|chromosome|
|skos:editorialNote|Both present on the sample and the chromosome object.|

