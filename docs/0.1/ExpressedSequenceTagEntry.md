# ExpressedSequenceTagEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|EST|
|rdfs:label|Expressed Sequence Tag|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1384835">http://www.wikidata.org/entity/Q1384835</a>|

