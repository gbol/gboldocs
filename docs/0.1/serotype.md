# serotype a ObjectProperty

## Domain

definition: The serological typing of the species within the sample by its antigenic properties<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|serotype|

