# MicroSatellite a skos:Concept, [SatelliteType](/0.1/SatelliteType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A repeat_region containing repeat_units of 2 to 10 bp repeated in tandem.|
|ddbjLabel|microsatellite|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000289">http://purl.obolibrary.org/obo/SO_0000289</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q265193">http://www.wikidata.org/entity/Q265193</a>|

