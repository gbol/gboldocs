# CAATSignal a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Part of a conserved sequence located about 75-bp upstream of the start point of eukaryotic transcription units which may be involved in RNA polymerase binding; consensus=GG(C|T)CAATCT.|
|ddbjLabel|CAAT_signal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000172">http://purl.obolibrary.org/obo/SO_0000172</a>|

