# taxonType a ObjectProperty

## Domain

definition: The type of taxon<br>
[ASVSet](/0.1/ASVSet)

## Range

[TaxonAssignmentType](/0.1/TaxonAssignmentType)

## Annotations


