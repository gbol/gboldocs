# intron a ObjectProperty

## Domain

definition: All introns that are part of the gene<br>
[Gene](/0.1/Gene)

## Range

[Intron](/0.1/Intron)

## Annotations


