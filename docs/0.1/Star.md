# Star a owl:Class extends [ProvenanceMapping](/0.1/ProvenanceMapping)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[percentageOfReadsUnmappedTooManyMismatches](/0.1/percentageOfReadsUnmappedTooManyMismatches)||1:1|xsd:Float|
|[numberOfSplicesAnnotated_sjdb](/0.1/numberOfSplicesAnnotated_sjdb)||1:1|xsd:Integer|
|[percentageOfReadsUnmappedTooShort](/0.1/percentageOfReadsUnmappedTooShort)||1:1|xsd:Float|
|[finishedMappingOn](/0.1/finishedMappingOn)||1:1|xsd:string|
|[percentageOfReadsMappedToMultipleLoci](/0.1/percentageOfReadsMappedToMultipleLoci)||1:1|xsd:Float|
|[numberOfSplicesAT_AC](/0.1/numberOfSplicesAT_AC)||1:1|xsd:Integer|
|[numberOfReadsMappedToTooManyLoci](/0.1/numberOfReadsMappedToTooManyLoci)||1:1|xsd:Integer|
|[insertionAverageLength](/0.1/insertionAverageLength)||1:1|xsd:Float|
|[numberOfSplicesNonCanonical](/0.1/numberOfSplicesNonCanonical)||1:1|xsd:Integer|
|[numberOfChimericReads](/0.1/numberOfChimericReads)||1:1|xsd:Integer|
|[percentageOfChimericReads](/0.1/percentageOfChimericReads)||1:1|xsd:Float|
|[averageInputReadLength](/0.1/averageInputReadLength)||1:1|xsd:Float|
|[averageMappedLength](/0.1/averageMappedLength)||1:1|xsd:Float|
|[numberOfReadsMappedToMultipleLoci](/0.1/numberOfReadsMappedToMultipleLoci)||1:1|xsd:Integer|
|[deletionAverageLength](/0.1/deletionAverageLength)||1:1|xsd:Float|
|[percentageOfReadsUnmappedOther](/0.1/percentageOfReadsUnmappedOther)||1:1|xsd:Float|
|[mappingSpeedMillionOfReadsPerHour](/0.1/mappingSpeedMillionOfReadsPerHour)||1:1|xsd:Float|
|[mismatchRatePerBasePercentage](/0.1/mismatchRatePerBasePercentage)||1:1|xsd:Float|
|[numberOfSplicesTotal](/0.1/numberOfSplicesTotal)||1:1|xsd:Integer|
|[numberOfSplicesGC_AG](/0.1/numberOfSplicesGC_AG)||1:1|xsd:Integer|
|[numberOfSplicesGT_AG](/0.1/numberOfSplicesGT_AG)||1:1|xsd:Integer|
|[deletionRatePerBase](/0.1/deletionRatePerBase)||1:1|xsd:Float|
|[insertionRatePerBase](/0.1/insertionRatePerBase)||1:1|xsd:Float|
|[numberOfInputReads](/0.1/numberOfInputReads)||1:1|xsd:Integer|
|[uniquelyMappedReadsNumber](/0.1/uniquelyMappedReadsNumber)||1:1|xsd:Integer|
|[startedMappingOn](/0.1/startedMappingOn)||1:1|xsd:string|
|[uniquelyMappedReadsPercentage](/0.1/uniquelyMappedReadsPercentage)||1:1|xsd:Float|
|[percentageOfReadsMappedToTooManyLoci](/0.1/percentageOfReadsMappedToTooManyLoci)||1:1|xsd:Float|
