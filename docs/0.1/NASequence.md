# NASequence a owl:Class extends [Sequence](/0.1/Sequence)

## Subclasses

[CompleteNASequence](/0.1/CompleteNASequence)

[PCRPrimer](/0.1/PCRPrimer)

[Barcode](/0.1/Barcode)

[Transcript](/0.1/Transcript)

[ASVSequence](/0.1/ASVSequence)

[UncompleteNASequence](/0.1/UncompleteNASequence)

## Annotations

|||
|-----|-----|
|rdfs:comment|Physical nucleotide sequence can be a DNA or RNA molecule being either single or double stranded.|
|rdfs:comment|Represents a physical nucleotide sequence|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[topology](/0.1/topology)|The topology of the sequence (linear, circular)|0:1|[Topology](/0.1/Topology)|
|[clone](/0.1/clone)|If a clone library was used for sequencing, the sequence can originate from one or more clones|0:N|xsd:string|
|[expression](/0.1/expression)|Expression feature|0:N|[Expression](/0.1/Expression)|
|[rearranged](/0.1/rearranged)|Indicates if the sequence has undergone somatic rearrangement as part of an adaptive immune response. If equal to false it is the un-rearranged sequence that was inherited from the parental germ line. If equal to true the sequence has undergone somatic rearrangement as part of an adaptive immune response. If not defined it is not applicable to the sequence or unknown.|0:1|xsd:Boolean|
|[translTable](/0.1/translTable)|The translation table of the nucleotide sequence, if not present its inherited from the parent NASequence|0:1|xsd:Integer|
|[macronuclear](/0.1/macronuclear)|Indicates that this sequence from the macro-nuclear DNA from an organism which undergoes chromosomal differentiation between macro-nuclear and micro-nuclear stages.|0:1|xsd:Boolean|
|[feature](/0.1/feature)|The set of features annotating the sequence|0:N|[NAFeature](/0.1/NAFeature)|
|[integratedInto](/0.1/integratedInto)|If this NA object is integrated into another NA object, for example a phage genome which is integrated into a bacterial genome or a plasmid which is integrated into a yeast genome.|0:1|[NASequence](/0.1/NASequence)|
|[proviral](/0.1/proviral)|Indicates that this sequence is obtained from a virus or phage that is integrated into the genome of another organism|0:1|xsd:Boolean|
|[cloneLib](/0.1/cloneLib)|If a clone library was used for sequencing, specify the clone library|0:1|xsd:string|
