# minOTUSizeT a ObjectProperty

## Domain

definition: Minimum size of rejected OTU to be classify<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


