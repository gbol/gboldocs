# UncompleteNASequence a owl:Class extends [NASequence](/0.1/NASequence)

## Subclasses

[Contig](/0.1/Contig)

[Read](/0.1/Read)

[Scaffold](/0.1/Scaffold)

## Annotations

|||
|-----|-----|
|skos:definition|Incomplete sequence of a physical nucleotide molecule|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[strandType](/0.1/strandType)|The type of the strand|1:1|[StrandType](/0.1/StrandType)|
