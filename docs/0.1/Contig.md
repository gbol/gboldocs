# Contig a owl:Class extends [UncompleteNASequence](/0.1/UncompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|An assembly of separate reads|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000149">http://purl.obolibrary.org/obo/SO_0000149</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1128751">http://www.wikidata.org/entity/Q1128751</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[sourceReads](/0.1/sourceReads)||0:N|[Read](/0.1/Read)|
