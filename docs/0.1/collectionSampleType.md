# collectionSampleType a ObjectProperty

## Domain

definition: Type of the collection sample<br>
[MaterialSource](/0.1/MaterialSource)

## Range

[CollectionSampleType](/0.1/CollectionSampleType)

## Annotations


