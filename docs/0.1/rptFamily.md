# rptFamily a ObjectProperty

## Domain

definition: The family of the repeat<br>
[RepeatFeature](/0.1/RepeatFeature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|rpt_family|
|skos:editorialNote|Repeat family free string, no ontology yet connected|

