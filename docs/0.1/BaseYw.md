# BaseYw a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|yw|
|rdfs:label|wybutosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1310878">http://www.wikidata.org/entity/Q1310878</a>|

