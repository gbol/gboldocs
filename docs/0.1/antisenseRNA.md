# antisenseRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|RNA molecule that is transcribed from the coding, rather than the template, strand of DNA and that is therefore complementary to mRNA.|
|ddbjLabel|antisense_RNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000644">http://purl.obolibrary.org/obo/SO_0000644</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q423832">http://www.wikidata.org/entity/Q423832</a>|

