# Riboswitch a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A riboswitch is a part of an mRNA that can act as a direct sensor of small molecules to control their own expression. A riboswitch is a cis element in the 5' end of an mRNA, that acts as a direct sensor of metabolites.|
|ddbjLabel|riboswitch|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000035">http://purl.obolibrary.org/obo/SO_0000035</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q418840">http://www.wikidata.org/entity/Q418840</a>|

