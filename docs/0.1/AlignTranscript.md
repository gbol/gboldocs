# AlignTranscript a skos:Concept, [LinkageEvidence](/0.1/LinkageEvidence)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|alignment to a transcript from the same species.|
|ddbjLabel|align_trnscpt|

