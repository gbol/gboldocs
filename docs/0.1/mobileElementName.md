# mobileElementName a ObjectProperty

## Domain

definition: The name of the mobile element<br>
[MobileElement](/0.1/MobileElement)

## Range

xsd:string

## Annotations


