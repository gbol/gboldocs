# PCRPrimers a ObjectProperty

## Domain

[Sample](/0.1/Sample)

## Range

[PCRPrimerSet](/0.1/PCRPrimerSet)

## Annotations

|||
|-----|-----|
|ddbjLabel|PCR_primers|

