# QTLTypes a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The type of a QTL, this is the method used to map the QTL|

## skos:member

[MetaboliteQTL](/0.1/MetaboliteQTL)

[StandardQTL](/0.1/StandardQTL)

[ExpressionQTL](/0.1/ExpressionQTL)

[ProteinQTL](/0.1/ProteinQTL)

