# rptUnitSeq a ObjectProperty

## Domain

definition: Pattern of the repeat. For example (NAG)8(WN)2<br>
[ProteinRepeat](/0.1/ProteinRepeat)

definition: Pattern of the repeat. For example (NAG)8(WN)2<br>
[RepeatFeature](/0.1/RepeatFeature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|rpt_unit_seq|
|skos:editorialNote|Property of the repeat feature, use string to note pattern|

