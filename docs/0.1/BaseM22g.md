# BaseM22g a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m22g|
|rdfs:label|2,2-dimethylguanosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109160">http://www.wikidata.org/entity/Q27109160</a>|

