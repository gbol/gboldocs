# satelliteName a ObjectProperty

## Domain

definition: Specific name of the satellite<br>
[Satellite](/0.1/Satellite)

## Range

xsd:string

## Annotations


