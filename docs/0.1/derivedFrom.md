# derivedFrom a ObjectProperty

## Domain

definition: Optionally give a list of features upon which it is based<br>
[ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Range

[Feature](/0.1/Feature)

## Annotations


