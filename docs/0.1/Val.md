# Val a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|V|
|ddbjLabel|Val|
|rdfs:label|Valine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001436">http://purl.obolibrary.org/obo/SO_0001436</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q483752">http://www.wikidata.org/entity/Q483752</a>|

