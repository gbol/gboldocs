# RNasePRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|RNA component of Ribonuclease P (RNase P), a ubiquitous endoribonuclease.|
|ddbjLabel|RNase_P_RNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001639">http://purl.obolibrary.org/obo/SO_0001639</a>|

