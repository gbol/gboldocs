# organism a ObjectProperty

## Domain

definition: The organism from which the sequence originates from<br>
[CompleteNASequence](/0.1/CompleteNASequence)

definition: The organism from which the sequence originates from<br>
[Sequence](/0.1/Sequence)

definition: The organism from which the sequence originates from<br>
[Source](/0.1/Source)

## Range

[Organism](/0.1/Organism)

## Annotations


