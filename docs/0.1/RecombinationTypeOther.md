# RecombinationTypeOther a skos:Concept, [RecombinationType](/0.1/RecombinationType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|ddbjLabel|other|

