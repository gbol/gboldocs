# MiscFeature a owl:Class extends [NAFeature](/0.1/NAFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of biological interest which cannot be described by any other featureKey; a new or rare feature;|
|rdfs:comment|This key should not be used when the need is merely to mark a region in order to comment on it or to use it in another feature's location|
|ddbjLabel|misc_feature|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001411">http://purl.obolibrary.org/obo/SO_0001411</a>|

## Properties

