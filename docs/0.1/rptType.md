# rptType a ObjectProperty

## Domain

definition: The type of the repeat<br>
[RepeatFeature](/0.1/RepeatFeature)

## Range

[RepeatType](/0.1/RepeatType)

## Annotations

|||
|-----|-----|
|ddbjLabel|rpt_type|
|skos:editorialNote|RepeatType added, only present in the RepeatFeature class and subclasses|

