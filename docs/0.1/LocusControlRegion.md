# LocusControlRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A DNA region that includes DNAse hypersensitive sites located 5' to a gene that confers the high-level, position-independent, and copy number-dependent expression to that gene.|
|ddbjLabel|locus_control_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000037">http://purl.obolibrary.org/obo/SO_0000037</a>|

