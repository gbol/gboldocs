# BaseS4u a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|s4u|
|rdfs:label|4-thiouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q20890501">http://www.wikidata.org/entity/Q20890501</a>|

