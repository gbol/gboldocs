# operonId a ObjectProperty

## Domain

definition: Tag identifier of the operon<br>
[Operon](/0.1/Operon)

## Range

xsd:string

## Annotations


