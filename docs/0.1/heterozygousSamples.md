# heterozygousSamples a ObjectProperty

## Domain

definition: Number of Heterozygous variant samples<br>
[Variation](/0.1/Variation)

## Range

xsd:integer

## Annotations


