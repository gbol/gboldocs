# ncRNA a owl:Class extends [MaturedRNA](/0.1/MaturedRNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|a non-protein-coding gene, other than ribosomal RNA and transfer RNA, the functional molecule of which is the RNAtranscript;|
|rdfs:comment|the ncRNA feature is not used for ribosomal and transferRNA annotation, for which the rRNA and tRNA featureKeysshould be used, respectively;|
|ddbjLabel|ncRNA|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000655">http://purl.obolibrary.org/obo/SO_0000655</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q427087">http://www.wikidata.org/entity/Q427087</a>|
|skos:example|/ncRNA_class=\"miRNA\"/ncRNA_class=\"siRNA\"/ncRNA_class=\"scRNA\"|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[product](/0.1/product)|Short description of the product resulting from the feature|0:1|xsd:string|
|[ncRNAClass](/0.1/ncRNAClass)|The type of the non coding RNA|1:1|[ncRNAType](/0.1/ncRNAType)|
