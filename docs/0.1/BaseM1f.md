# BaseM1f a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m1f|
|rdfs:label|1-methylpseudouridine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001347">http://purl.obolibrary.org/obo/SO_0001347</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109108">http://www.wikidata.org/entity/Q27109108</a>|

