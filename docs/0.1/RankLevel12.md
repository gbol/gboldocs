# RankLevel12 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 12|
|parentRank|<a href="http://gbol.life/0.1/RankLevel11">http://gbol.life/0.1/RankLevel11</a>|
|rdfs:label|Rank Level 12|

