# UpdatedSequence a owl:Class extends [VariationFeature](/0.1/VariationFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The presented variation revises a previous version of the sequence at the given location;|
|rdfs:comment|/replace=\"\" is used to annotate deletion, e.g.old_sequence 12..15/replace=\"\"NOTE: This featureKey is not valid in entries/recordscreated from 15-Oct-2007.|
|ddbjLabel|old_sequence|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[compare](/0.1/compare)|The functional name|1:1|IRI|
