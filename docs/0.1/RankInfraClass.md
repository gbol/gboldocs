# RankInfraClass a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: infra class|
|parentRank|<a href="http://gbol.life/0.1/RankSubClass">http://gbol.life/0.1/RankSubClass</a>|
|rdfs:label|infra class|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2007442">http://www.wikidata.org/entity/Q2007442</a>|

