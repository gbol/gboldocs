# annotationResults a ObjectProperty

## Domain

definition: All annotation result within the GBOL data set<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[AnnotationResult](/0.1/AnnotationResult)

## Annotations


