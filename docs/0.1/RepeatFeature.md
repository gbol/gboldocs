# RepeatFeature a owl:Class extends [GenomicFeature](/0.1/GenomicFeature)

## Subclasses

[Centromere](/0.1/Centromere)

[CRISPRCassette](/0.1/CRISPRCassette)

[RepeatRegion](/0.1/RepeatRegion)

[Telomere](/0.1/Telomere)

[MobileElement](/0.1/MobileElement)

## Annotations

|||
|-----|-----|
|skos:definition|Feature for which a repetitive unit can be defined|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000657">http://purl.obolibrary.org/obo/SO_0000657</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rptUnitSeq](/0.1/rptUnitSeq)|Pattern of the repeat. For example (NAG)8(WN)2|0:1|xsd:string|
|[satellite](/0.1/satellite)|If repeat contains on or more satellites specify with this property the types of the satellites|0:N|[Satellite](/0.1/Satellite)|
|[rptType](/0.1/rptType)|The type of the repeat|0:1|[RepeatType](/0.1/RepeatType)|
|[rptFamily](/0.1/rptFamily)|The family of the repeat|0:1|xsd:string|
|[rptUnitRange](/0.1/rptUnitRange)|Base range of the sequence that constitutes a repeated sequence|0:1|[Region](/0.1/Region)|
