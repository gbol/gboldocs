# alignmentLength a ObjectProperty

## Domain

definition: The alignment length of two sequence objects<br>
[sapp:Blast](/sapp/0.1/Blast)

## Range

xsd:integer

## Annotations


