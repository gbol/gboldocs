# StandardEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|rdfs:comment|all entries not classified as any of the other classifications|
|ddbjLabel|STD|
|rdfs:label|Standard|

