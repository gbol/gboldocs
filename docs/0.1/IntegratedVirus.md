# IntegratedVirus a owl:Class extends [TranscriptionElement](/0.1/TranscriptionElement)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A virus genome integrated into another genome|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[integratedRegion](/0.1/integratedRegion)|The region of the virus genome that is integrated|1:1|[Location](/0.1/Location)|
|[virusGenome](/0.1/virusGenome)|Reference to the sequence of the virus, genes of the integrated virus can be annotated directly on genome as well as on the virus genome|0:1|[NASequence](/0.1/NASequence)|
