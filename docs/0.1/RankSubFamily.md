# RankSubFamily a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub family|
|parentRank|<a href="http://gbol.life/0.1/RankFamily">http://gbol.life/0.1/RankFamily</a>|
|rdfs:label|sub family|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2455704">http://www.wikidata.org/entity/Q2455704</a>|

