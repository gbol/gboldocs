# NeotypeStrain a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|neotype strain|
|rdfs:label|neotype strain|

