# Burma a skos:Concept, [Country](/0.1/Country)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Burma|
|rdfs:label|Burma|
|owl:deprecated|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|

