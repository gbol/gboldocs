# Homology a owl:Class extends [GenomicFeature](/0.1/GenomicFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Denote a region to be homologous to another entity, which is either another na sequence or cluster of na sequences|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q224180">http://www.wikidata.org/entity/Q224180</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[homologousTo](/0.1/homologousTo)|The entity to which it is homologous to, being either another na sequence or cluster of na sequences|1:1|IRI|
