# rearranged a ObjectProperty

## Domain

definition: Indicates if the sequence has undergone somatic rearrangement as part of an adaptive immune response. If equal to false it is the un-rearranged sequence that was inherited from the parental germ line. If equal to true the sequence has undergone somatic rearrangement as part of an adaptive immune response. If not defined it is not applicable to the sequence or unknown.<br>
[NASequence](/0.1/NASequence)

## Range

xsd:boolean

## Annotations

|||
|-----|-----|
|ddbjLabel|rearranged|

