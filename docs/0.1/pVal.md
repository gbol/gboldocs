# pVal a ObjectProperty

## Domain

definition: p-value from Fisher's Exact Test<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


