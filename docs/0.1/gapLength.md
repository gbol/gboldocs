# gapLength a ObjectProperty

## Domain

definition: Denote the length of the gap<br>
[PartGapKnownLength](/0.1/PartGapKnownLength)

## Range

xsd:integer

## Annotations


