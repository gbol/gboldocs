# TranscriptionElement a owl:Class extends [GenomicFeature](/0.1/GenomicFeature)

## Subclasses

[IntegratedVirus](/0.1/IntegratedVirus)

[Intron](/0.1/Intron)

[Gene](/0.1/Gene)

[Exon](/0.1/Exon)

[Operon](/0.1/Operon)

## Annotations

|||
|-----|-----|
|skos:definition|Elements related to transcribed regions|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

