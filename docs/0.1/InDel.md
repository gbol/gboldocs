# InDel a skos:Concept, [VariationTypes](/0.1/VariationTypes)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Insertion or Deletion|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_1000032">http://purl.obolibrary.org/obo/SO_1000032</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1576681">http://www.wikidata.org/entity/Q1576681</a>|

