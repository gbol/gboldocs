# RibosomeBindingSite a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A region in the 5' UTR that pairs with the 16S rRNA during formation of the preinitiation complex.|
|ddbjLabel|RBS|
|ddbjLabel|ribosome_binding_site|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000552">http://purl.obolibrary.org/obo/SO_0000552</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q16948788">http://www.wikidata.org/entity/Q16948788</a>|

