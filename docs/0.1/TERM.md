# TERM a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|*|
|ddbjLabel|TERM|
|rdfs:label|termination codon|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q28354">http://www.wikidata.org/entity/Q28354</a>|

