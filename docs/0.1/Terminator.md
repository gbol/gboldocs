# Terminator a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The sequence of DNA located either at the end of the transcript that causes RNA polymerase to terminate transcription.|
|ddbjLabel|terminator|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000141">http://purl.obolibrary.org/obo/SO_0000141</a>|

