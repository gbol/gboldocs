# dblinks a ObjectProperty

## Domain

definition: Cross references to other databases containing the same record<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

[XRef](/0.1/XRef)

## Annotations


