# BaseM2g a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m2g|
|rdfs:label|2-methylguanosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1419713">http://www.wikidata.org/entity/Q1419713</a>|

