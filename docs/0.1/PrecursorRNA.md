# PrecursorRNA a owl:Class extends [Transcript](/0.1/Transcript)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|any RNA species that is not yet the mature RNA product, which needs to be described;may have associated transcripts.|
|rdfs:comment|used for RNA which may be the result of post-transcriptional processing;  if the RNA in question is known not to have been processed, use theprim_transcript key.|
|ddbjLabel|precursor_RNA|
|ddbjLabel|prim_transcript|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000185">http://purl.obolibrary.org/obo/SO_0000185</a>|

## Properties

