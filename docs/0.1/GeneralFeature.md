# GeneralFeature a owl:Class extends [Feature](/0.1/Feature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|General feature used to add general notes|
|rdfs:comment|Use this feature to add a citation to a given (sub)sequence|
|subDomain|SequenceFeatureCore|

## Properties

