# TaxonomyRef a owl:Class extends [XRef](/0.1/XRef)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A xref which point to a database containing taxomic classification, which is typically the uniprot taxonomic database.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[nomenclaturalType](/0.1/nomenclaturalType)||0:1|[NomenclaturalType](/0.1/NomenclaturalType)|
