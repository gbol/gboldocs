# SequencePart a owl:Class

## Subclasses

[PartGap](/0.1/PartGap)

[PartSequence](/0.1/PartSequence)

## Annotations

|||
|-----|-----|
|skos:definition|A part within a sequence assembly.|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|

## Properties

