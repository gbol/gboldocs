# allele a ObjectProperty

## Domain

definition: List of names of known allele variants of the gene<br>
[Gene](/0.1/Gene)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|allele|

