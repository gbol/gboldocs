# RejectedAsChimera a owl:Class extends [ASVSet](/0.1/ASVSet)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[pReverseId](/0.1/pReverseId)|P reverse id|1:1|xsd:Integer|
|[pForwardId](/0.1/pForwardId)|P forward id|1:1|xsd:Integer|
|[pForwardRatio](/0.1/pForwardRatio)|P forward ratio|1:1|xsd:Float|
|[pReverseRatio](/0.1/pReverseRatio)|P reverse ratio|1:1|xsd:Float|
