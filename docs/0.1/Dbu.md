# Dbu a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Dbu|
|rdfs:label|2,4-Diaminobutyric acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27094699">http://www.wikidata.org/entity/Q27094699</a>|

