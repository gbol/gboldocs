# identity92MismatchCount a ObjectProperty

## Domain

definition: Identity 92 mismatch count<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


