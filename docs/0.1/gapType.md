# gapType a ObjectProperty

## Domain

definition: The type of the gap<br>
[AssemblyGap](/0.1/AssemblyGap)

## Range

[GapType](/0.1/GapType)

## Annotations

|||
|-----|-----|
|ddbjLabel|gap_type|
|skos:editorialNote|Added to the Gap class|

