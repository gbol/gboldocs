# ncRNATypeOther a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:definition|ncRNA_class not included in any other term.|
|ddbjLabel|ncRNATypeOther|

