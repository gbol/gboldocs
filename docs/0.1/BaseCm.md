# BaseCm a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|cm|
|rdfs:label|2'-O-methylcytidine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632793">http://www.wikidata.org/entity/Q15632793</a>|

