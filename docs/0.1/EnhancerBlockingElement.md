# EnhancerBlockingElement a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A transcriptional cis regulatory region that when located between an enhancer and a gene's promoter prevents the enhancer from modulating the expression of the gene. Sometimes referred to as an insulator but may not include the barrier function of an insulator.|
|ddbjLabel|enhancer_blocking_element|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0002190">http://purl.obolibrary.org/obo/SO_0002190</a>|

