# ThreeHyp a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|ThreeHyp|
|rdfs:label|3-Hydroxyproline|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4030651">http://www.wikidata.org/entity/Q4030651</a>|

