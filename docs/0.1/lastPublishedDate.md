# lastPublishedDate a ObjectProperty

## Domain

definition: The last time the this GBOL data has been published<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:dateTime

## Annotations


