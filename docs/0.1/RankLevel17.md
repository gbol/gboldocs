# RankLevel17 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 17|
|parentRank|<a href="http://gbol.life/0.1/RankLevel16">http://gbol.life/0.1/RankLevel16</a>|
|rdfs:label|Rank Level 17|

