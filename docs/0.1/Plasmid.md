# Plasmid a owl:Class extends [CompleteNASequence](/0.1/CompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|A plasmid sequence|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000155">http://purl.obolibrary.org/obo/SO_0000155</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q172778">http://www.wikidata.org/entity/Q172778</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[plasmidName](/0.1/plasmidName)|The name of the plasmid|0:1|xsd:string|
