# RankFamily a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: family|
|parentRank|<a href="http://gbol.life/0.1/RankSuperFamily">http://gbol.life/0.1/RankSuperFamily</a>|
|rdfs:label|family|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q35409">http://www.wikidata.org/entity/Q35409</a>|

