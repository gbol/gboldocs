# wasAssociatedWith a ObjectProperty

## Domain

definition: The agent which be performed this activity<br>
[ManualAnnotationActivity](/0.1/ManualAnnotationActivity)

definition: The agent which be performed this activity<br>
[AutomaticAnnotationActivity](/0.1/AutomaticAnnotationActivity)

definition: The agent which be performed this activity<br>
[prov:Activity](/ns/prov/Activity)

## Range

[Curator](/0.1/Curator)

[AnnotationSoftware](/0.1/AnnotationSoftware)

[prov:Agent](/ns/prov/Agent)

## Annotations


