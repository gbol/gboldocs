# Hyl a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Hyl|
|rdfs:label|Hydroxylysine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27104861">http://www.wikidata.org/entity/Q27104861</a>|

