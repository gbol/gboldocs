# PairedEnd a owl:Class extends [ReadInformation](/0.1/ReadInformation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[file2](/0.1/file2)||1:1|[LocalSequenceFile](/0.1/LocalSequenceFile)|
|[insertSize](/0.1/insertSize)||0:1|xsd:Integer|
