# structureType a ObjectProperty

## Domain

definition: The type of secondary structure<br>
[ProteinStructure](/0.1/ProteinStructure)

## Range

[ProteinStructureType](/0.1/ProteinStructureType)

## Annotations


