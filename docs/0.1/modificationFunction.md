# modificationFunction a ObjectProperty

## Domain

definition: If applicable, the biological process in which this modification is involved. Use a Biological Process from the GO ontology.<br>
[ModifiedBase](/0.1/ModifiedBase)

definition: If applicable, the biological process in which this modification is involved. Use a Biological Process from the GO ontology.<br>
[ModifiedResidue](/0.1/ModifiedResidue)

## Range

xsd:anyURI

## Annotations


