# RankParvOrder a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: parv order|
|parentRank|<a href="http://gbol.life/0.1/RankInfraOrder">http://gbol.life/0.1/RankInfraOrder</a>|
|rdfs:label|parv order|

