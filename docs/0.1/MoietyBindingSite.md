# MoietyBindingSite a owl:Class extends [BindingSite](/0.1/BindingSite)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Region of the protein that bind to some moiety|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bindingMoiety](/0.1/bindingMoiety)|Name of the binding moiety|1:1|xsd:string|
