# PCRPrimer a owl:Class extends [NASequence](/0.1/NASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A single primer.|
|subDomain|DocumentCore|

## Properties

