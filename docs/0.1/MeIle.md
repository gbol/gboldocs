# MeIle a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|MeIle|
|rdfs:label|N-Methylisoleucine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133231">http://www.wikidata.org/entity/Q27133231</a>|

