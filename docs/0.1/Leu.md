# Leu a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|L|
|ddbjLabel|Leu|
|rdfs:label|Leucine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001437">http://purl.obolibrary.org/obo/SO_0001437</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q483745">http://www.wikidata.org/entity/Q483745</a>|

