# satellite a ObjectProperty

## Domain

definition: If repeat contains on or more satellites specify with this property the types of the satellites<br>
[RepeatFeature](/0.1/RepeatFeature)

## Range

[Satellite](/0.1/Satellite)

## Annotations

|||
|-----|-----|
|ddbjLabel|satellite|
|skos:editorialNote|Satellite class added to describe the satellites, optional property of the repeat region|

