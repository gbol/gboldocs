# RankSpeciesGroup a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: species group|
|parentRank|<a href="http://gbol.life/0.1/RankSubGenus">http://gbol.life/0.1/RankSubGenus</a>|
|rdfs:label|species group|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7574964">http://www.wikidata.org/entity/Q7574964</a>|

