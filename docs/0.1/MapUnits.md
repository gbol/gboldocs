# MapUnits a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|Defines the units used to map the QTLs to the genome|

## skos:member

[bp](/0.1/bp)

[cM](/0.1/cM)

