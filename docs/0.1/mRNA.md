# mRNA a owl:Class extends [MaturedRNA](/0.1/MaturedRNA)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|messenger RNA; includes 5'untranslated region (5'UTR),coding sequences (CDS, exon) and 3'untranslated region(3'UTR);|
|ddbjLabel|mRNA|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000234">http://purl.obolibrary.org/obo/SO_0000234</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q188928">http://www.wikidata.org/entity/Q188928</a>|

## Properties

