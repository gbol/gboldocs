# RegulatoryClass a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|A list type for regulatory elements.|
|subDomain|SequenceFeatureCore|

## skos:member

[PolyASignalSequence](/0.1/PolyASignalSequence)

[Riboswitch](/0.1/Riboswitch)

[RecodingStimulatoryRegion](/0.1/RecodingStimulatoryRegion)

[Terminator](/0.1/Terminator)

[ReplicationRegulatoryRegion](/0.1/ReplicationRegulatoryRegion)

[Promoter](/0.1/Promoter)

[GCSignal](/0.1/GCSignal)

[Minus10Signal](/0.1/Minus10Signal)

[Minus35Signal](/0.1/Minus35Signal)

[DNaseIHypersensitiveSite](/0.1/DNaseIHypersensitiveSite)

[Silencer](/0.1/Silencer)

[LocusControlRegion](/0.1/LocusControlRegion)

[CAATSignal](/0.1/CAATSignal)

[ResponseElement](/0.1/ResponseElement)

[RibosomeBindingSite](/0.1/RibosomeBindingSite)

[Enhancer](/0.1/Enhancer)

[TATABox](/0.1/TATABox)

[TranscriptionalCisRegulatoryRegion](/0.1/TranscriptionalCisRegulatoryRegion)

[ImprintingControlRegion](/0.1/ImprintingControlRegion)

[Minus12Signal](/0.1/Minus12Signal)

[Insulator](/0.1/Insulator)

[MatrixAttachmentRegion](/0.1/MatrixAttachmentRegion)

[Attenuator](/0.1/Attenuator)

[EnhancerBlockingElement](/0.1/EnhancerBlockingElement)

[OtherRegulation](/0.1/OtherRegulation)

