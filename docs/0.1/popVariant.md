# popVariant a ObjectProperty

## Domain

definition: The name of the subpopulation or phenotype from which the sample was collected.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|pop_variant|

