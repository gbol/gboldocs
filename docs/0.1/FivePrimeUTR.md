# FivePrimeUTR a owl:Class extends [TranscriptFeature](/0.1/TranscriptFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|region at the 5' end of a mature transcript (preceding the initiation codon) that is not translated into a protein;|
|ddbjLabel|5'UTR|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000204">http://purl.obolibrary.org/obo/SO_0000204</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q937624">http://www.wikidata.org/entity/Q937624</a>|

## Properties

