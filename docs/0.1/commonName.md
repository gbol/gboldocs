# commonName a ObjectProperty

## Domain

definition: The common name of the sequence<br>
[Sequence](/0.1/Sequence)

## Range

xsd:string

## Annotations


