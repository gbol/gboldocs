# unmodifiedState a ObjectProperty

## Domain

definition: Short description denoted the state of the protein if the residue in the unmodified state. For example 'protein in inactive state'<br>
[ModifiedResidue](/0.1/ModifiedResidue)

## Range

xsd:string

## Annotations


