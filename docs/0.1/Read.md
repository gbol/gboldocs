# Read a owl:Class extends [UncompleteNASequence](/0.1/UncompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A single read from a sequencer.|
|subDomain|SequenceFeatureCore|

## Properties

