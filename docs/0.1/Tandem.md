# Tandem a skos:Concept, [RepeatType](/0.1/RepeatType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Two or more adjacent copies of a region (of length greater than 1).|
|ddbjLabel|tandem|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000705">http://purl.obolibrary.org/obo/SO_0000705</a>|

