# Dpr a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Dpr|
|rdfs:label|2,3-Diaminoproprionic acid|

