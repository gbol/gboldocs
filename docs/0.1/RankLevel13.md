# RankLevel13 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 13|
|parentRank|<a href="http://gbol.life/0.1/RankLevel12">http://gbol.life/0.1/RankLevel12</a>|
|rdfs:label|Rank Level 13|

