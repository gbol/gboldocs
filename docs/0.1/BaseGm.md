# BaseGm a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|gm|
|rdfs:label|2'-O-methylguanosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q15632796">http://www.wikidata.org/entity/Q15632796</a>|

