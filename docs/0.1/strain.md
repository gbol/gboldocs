# strain a ObjectProperty

## Domain

definition: The name or identifier of the strain<br>
[Sample](/0.1/Sample)

definition: The name or identifier of the strain<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|strain|

