# BaseOsyw a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|osyw|
|rdfs:label|wybutoxosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27133221">http://www.wikidata.org/entity/Q27133221</a>|

