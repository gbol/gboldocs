# RankLevel14 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 14|
|parentRank|<a href="http://gbol.life/0.1/RankLevel13">http://gbol.life/0.1/RankLevel13</a>|
|rdfs:label|Rank Level 14|

