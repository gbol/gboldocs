# Interleaved a owl:Class extends [ReadInformation](/0.1/ReadInformation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[insertSize](/0.1/insertSize)||0:1|xsd:Integer|
