# virusGenome a ObjectProperty

## Domain

definition: Reference to the sequence of the virus, genes of the integrated virus can be annotated directly on genome as well as on the virus genome<br>
[IntegratedVirus](/0.1/IntegratedVirus)

## Range

[NASequence](/0.1/NASequence)

## Annotations


