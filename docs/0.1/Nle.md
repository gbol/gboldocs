# Nle a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Nle|
|rdfs:label|Norleucine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27116817">http://www.wikidata.org/entity/Q27116817</a>|

