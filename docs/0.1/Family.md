# Family a skos:Concept, [ProteinDomainType](/0.1/ProteinDomainType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A preserved protein family, protein families typically span the whole protein.|

