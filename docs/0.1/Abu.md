# Abu a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Abu|
|rdfs:label|2-Aminobutyric acid|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27457174">http://www.wikidata.org/entity/Q27457174</a>|

