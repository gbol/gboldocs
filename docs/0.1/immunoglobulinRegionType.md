# immunoglobulinRegionType a ObjectProperty

## Domain

definition: The type of the immunoglobulin region<br>
[ImmunoglobulinFeature](/0.1/ImmunoglobulinFeature)

## Range

[ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Annotations


