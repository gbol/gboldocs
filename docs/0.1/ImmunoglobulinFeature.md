# ImmunoglobulinFeature a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[immunoglobulinRegionType](/0.1/immunoglobulinRegionType)|The type of the immunoglobulin region|1:1|[ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)|
