# end a ObjectProperty

## Domain

definition: The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.<br>
[Variation](/0.1/Variation)

definition: The begin position comes always first then the the end position. So for a gene on the reverse complementary strain the begin position is equal to the stop position. We use the same positioning definition as in GenBank and Faldo.<br>
[Region](/0.1/Region)

## Range

[Position](/0.1/Position)

## Annotations


