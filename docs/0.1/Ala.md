# Ala a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|A|
|ddbjLabel|Ala|
|rdfs:label|Alanine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001435">http://purl.obolibrary.org/obo/SO_0001435</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q218642">http://www.wikidata.org/entity/Q218642</a>|

