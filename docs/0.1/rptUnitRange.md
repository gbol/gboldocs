# rptUnitRange a ObjectProperty

## Domain

definition: Base range of the sequence that constitutes a repeated sequence<br>
[ProteinRepeat](/0.1/ProteinRepeat)

definition: Base range of the sequence that constitutes a repeated sequence<br>
[RepeatFeature](/0.1/RepeatFeature)

## Range

[Region](/0.1/Region)

## Annotations

|||
|-----|-----|
|ddbjLabel|rpt_unit_range|
|skos:editorialNote|Property of the repeat region|

