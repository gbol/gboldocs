# BarcodeSet a owl:Class extends [SequenceSet](/0.1/SequenceSet)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The definition of a barcode pair that can be used for amplicon sequencing. Multiple forward and/or reverse barcodes might be defined.|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[forwardBarcode](/0.1/forwardBarcode)|The forward primer(s)|0:1|[Barcode](/0.1/Barcode)|
|[reverseBarcode](/0.1/reverseBarcode)|The reverse primer(s)|0:1|[Barcode](/0.1/Barcode)|
