# score a ObjectProperty

## Domain

definition: Score given by this method<br>
[sapp:WoLFPSort](/sapp/0.1/WoLFPSort)

definition: Score given by this method<br>
[sapp:RNAmmer](/sapp/0.1/RNAmmer)

definition: Score given by this method<br>
[sapp:InterProScan](/sapp/0.1/InterProScan)

[sapp:Prodigal](/sapp/0.1/Prodigal)

## Range

xsd:double

## Annotations


