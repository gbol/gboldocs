# BaseDhu a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|dhu|
|rdfs:label|dihydrouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q413063">http://www.wikidata.org/entity/Q413063</a>|

