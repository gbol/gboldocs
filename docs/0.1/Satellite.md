# Satellite a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A DNA satellite|
|subDomain|DocumentCore|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q425319">http://www.wikidata.org/entity/Q425319</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[satelliteName](/0.1/satelliteName)|Specific name of the satellite|1:1|xsd:string|
|[satelliteType](/0.1/satelliteType)|The type of the satellite|1:1|[SatelliteType](/0.1/SatelliteType)|
