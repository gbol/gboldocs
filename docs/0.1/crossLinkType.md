# crossLinkType a ObjectProperty

## Domain

definition: Textual description of the crosslink type<br>
[CrossLink](/0.1/CrossLink)

## Range

xsd:string

## Annotations


