# RankLevel23 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 23|
|parentRank|<a href="http://gbol.life/0.1/RankLevel22">http://gbol.life/0.1/RankLevel22</a>|
|rdfs:label|Rank Level 23|

