# Allotype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|specimen of opposite sex to the holotype|
|ddbjLabel|allotype|
|rdfs:label|allotype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q19353437">http://www.wikidata.org/entity/Q19353437</a>|

