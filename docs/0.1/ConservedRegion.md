# ConservedRegion a owl:Class extends [ProteinFeature](/0.1/ProteinFeature)

## Subclasses

[ProteinDomain](/0.1/ProteinDomain)

[CatalyticDomain](/0.1/CatalyticDomain)

## Annotations

|||
|-----|-----|
|skos:definition|Region within a protein that is conserved|
|subDomain|SequenceFeatureCore|

## Properties

