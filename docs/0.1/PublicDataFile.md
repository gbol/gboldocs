# PublicDataFile a owl:Class extends [DataFile](/0.1/DataFile)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A public file that is available on the INTERNET|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[url](/0.1/url)|Optional an URL it was downloaded from|0:1|IRI|
|[accessedOn](/0.1/accessedOn)|The time point at which the file was downloaded|0:1|xsd:DateTime|
