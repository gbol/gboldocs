# signature a ObjectProperty

## Domain

definition: The reference to signature of the domain (Gene3d, Pfam, etc)<br>
[ProteinDomain](/0.1/ProteinDomain)

## Range

xsd:anyURI

## Annotations


