# AnnotationResult a owl:Class extends [prov:Entity](/ns/prov/Entity)

## Subclasses

[AnnotationLinkSet](/0.1/AnnotationLinkSet)

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|
|rdfs:label|AnnotationResult|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[prov:wasGeneratedBy](/ns/prov/wasGeneratedBy)|The annotation activity that created this annotation result|1:1|[AnnotationActivity](/0.1/AnnotationActivity)|
|[prov:wasAttributedTo](/ns/prov/wasAttributedTo)|The agent that performed or initiated the activity that generated this entity|1:1|[prov:Agent](/ns/prov/Agent)|
