# associatedFeature a ObjectProperty

## Domain

definition: associated markers linked to the QTL region<br>
[QTL](/0.1/QTL)

## Range

[Feature](/0.1/Feature)

## Annotations


