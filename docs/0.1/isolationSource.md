# isolationSource a ObjectProperty

## Domain

definition: Describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived. Use geographicalLocation to denote the geographical location (examples: 'permanent Antarctic sea ice', 'rumen isolates from standard Pelleted ration-fed steer #67', 'denitrifying activated sludge from carbon limited continuous reactor')<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|isolation_source|

