# exon a ObjectProperty

## Domain

definition: All exons that are part of the gene<br>
[Gene](/0.1/Gene)

[ExonList](/0.1/ExonList)

## Range

[Exon](/0.1/Exon)

## Annotations


