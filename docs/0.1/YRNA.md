# YRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|component of the Ro ribonucleoprotein particle (Ro RNP), in association with Ro60 and La proteins.|
|ddbjLabel|Y_RNA|

