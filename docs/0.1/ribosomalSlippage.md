# ribosomalSlippage a ObjectProperty

## Domain

definition: Indicate the presence of ribosomal slippage, the slippage region are excluded from the CDS using a CollectionOfRegions location for the CDS on the transcript.<br>
[CDS](/0.1/CDS)

## Range

xsd:boolean

## Annotations

|||
|-----|-----|
|ddbjLabel|ribosomal_slippage|
|skos:editorialNote|Boolean value indicating the presence of ribosomal slippage, slippage region are excluded from CDS using CollectionOfRegions location|

