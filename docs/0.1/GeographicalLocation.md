# GeographicalLocation a owl:Class

## Subclasses

[LandBasedGeographicalLocation](/0.1/LandBasedGeographicalLocation)

[WaterBasedGeographicalLocation](/0.1/WaterBasedGeographicalLocation)

## Annotations

|||
|-----|-----|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[locality](/0.1/locality)|The locality (aka the name of a village)|0:1|xsd:string|
|[region](/0.1/region)|The region within the country or coast along the ocean|0:1|xsd:string|
