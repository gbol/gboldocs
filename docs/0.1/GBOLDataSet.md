# GBOLDataSet a owl:Class extends [prov:Entity](/ns/prov/Entity), [void:Dataset](/ns/void/Dataset)

## Subclasses

[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Annotations

|||
|-----|-----|
|skos:definition|A complete annotation record stored in GBOL file|
|rdfs:comment|This class acts as root object of all the data stored within a GBOL file|
|gen:framing|ROOT<br>:xref<br>:samples<br>:organisms<br>:annotationResults<br>:linkedDataBases<br>:sequences|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[references](/0.1/references)|A set of references associated to the GBOLDataSet|0:N|[Citation](/0.1/Citation)|
|[entryType](/0.1/entryType)|The type of this GBOLDataSet|1:1|[EntryType](/0.1/EntryType)|
|[sequences](/0.1/sequences)|All dna objects included within the GBOL data set|0:N|[Sequence](/0.1/Sequence)|
|[linkedDataBases](/0.1/linkedDataBases)|All databases that are linked by any of the contained XRefs|0:N|[Database](/0.1/Database)|
|[samples](/0.1/samples)|All samples from which the sequence objects are collected|0:N|[Sample](/0.1/Sample)|
|[dataSetVersion](/0.1/dataSetVersion)|The version of the GBOL Data set, increase by 1 for every (set of) update(s)|1:1|xsd:Integer|
|[annotationResults](/0.1/annotationResults)|All annotation result within the GBOL data set|0:N|[AnnotationResult](/0.1/AnnotationResult)|
|[organisms](/0.1/organisms)|All associated organisms|0:N|[Organism](/0.1/Organism)|
|[keywords](/0.1/keywords)|A list of keyword describing the content of the GBOL data set|0:N|xsd:string|
|[xref](/0.1/xref)|Reference to other entries holding entries with related information in other databases|0:N|[XRef](/0.1/XRef)|
|[note](/0.1/note)|A set off some additional notes|0:N|[Note](/0.1/Note)|
