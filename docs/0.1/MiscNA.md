# MiscNA a owl:Class extends [CompleteNASequence](/0.1/CompleteNASequence)

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|Anything that is not a miscellaneous transcript, not a chromosome, not a plasmid or uncomplete NA sequence|

## Properties

