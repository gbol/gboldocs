# AntiCodon a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Complete description of an AntiCodon|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001174">http://purl.obolibrary.org/obo/SO_0001174</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q1414881">http://www.wikidata.org/entity/Q1414881</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[antiCodonSequence](/0.1/antiCodonSequence)|The sequence of the anticodon use [TGAC]|1:1|xsd:string|
|[aminoAcid](/0.1/aminoAcid)|The encoded amino acid|1:1|[AminoAcid](/0.1/AminoAcid)|
|[antiCodingRegion](/0.1/antiCodingRegion)|The position of the anticodon within the tRNA transcript|1:1|[Region](/0.1/Region)|
