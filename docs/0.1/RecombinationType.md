# RecombinationType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|

## skos:member

[Meiotic](/0.1/Meiotic)

[RecombinationTypeOther](/0.1/RecombinationTypeOther)

[ChromosomeBreakpoint](/0.1/ChromosomeBreakpoint)

[NonallelicHomologous](/0.1/NonallelicHomologous)

[Mitotic](/0.1/Mitotic)

