# VariableRegion a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|variable region of immunoglobulin light and heavychains, and T-cell receptor alpha, beta, and gammachains;  codes for the variable amino terminal portion;can be composed of V_segments, D_segments, N_regions,and J_segments;|
|ddbjLabel|V_region|
|subDomain|SequenceFeatureCore|

