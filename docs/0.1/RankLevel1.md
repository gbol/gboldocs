# RankLevel1 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 1|
|parentRank|<a href="http://gbol.life/0.1/RankForma">http://gbol.life/0.1/RankForma</a>|
|rdfs:label|Rank Level 1|

