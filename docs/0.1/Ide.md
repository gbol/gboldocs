# Ide a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Ide|
|rdfs:label|Isodesmosine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q4198750">http://www.wikidata.org/entity/Q4198750</a>|

