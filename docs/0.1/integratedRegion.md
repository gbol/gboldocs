# integratedRegion a ObjectProperty

## Domain

definition: The region of the virus genome that is integrated<br>
[IntegratedVirus](/0.1/IntegratedVirus)

## Range

[Location](/0.1/Location)

## Annotations


