# translExcept a ObjectProperty

## Domain

definition: Indicate all codons, which have translation other then the default translation of the translation table<br>
[CDS](/0.1/CDS)

## Range

[TranslExcept](/0.1/TranslExcept)

## Annotations

|||
|-----|-----|
|ddbjLabel|transl_except|
|skos:editorialNote|TranslException object describes the translation exceptions on the CDS feature.|

