# translTable a ObjectProperty

## Domain

definition: The translation table of the nucleotide sequence, if not present its inherited from the parent NASequence<br>
[NASequence](/0.1/NASequence)

## Range

xsd:integer

## Annotations

|||
|-----|-----|
|ddbjLabel|transl_table|
|skos:editorialNote|Added to the NAObject class, all features under this object use this translation table|

