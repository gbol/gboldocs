# PseudoGeneType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|A list of reasons for gene to be a pseudo gene.|
|subDomain|SequenceFeatureCore|

## skos:member

[Proccessed](/0.1/Proccessed)

[Unprocessed](/0.1/Unprocessed)

[Allelic](/0.1/Allelic)

[PseudoGeneTypeUnknown](/0.1/PseudoGeneTypeUnknown)

[Unitary](/0.1/Unitary)

