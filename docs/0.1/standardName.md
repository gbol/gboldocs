# standardName a ObjectProperty

## Domain

definition: The standard name of the feature<br>
[Sequence](/0.1/Sequence)

definition: The standard name of the feature<br>
[Feature](/0.1/Feature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|standard_name|
|skos:editorialNote|Added to gene feature, besides the gene property|

