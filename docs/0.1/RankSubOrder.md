# RankSubOrder a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: sub order|
|parentRank|<a href="http://gbol.life/0.1/RankOrder">http://gbol.life/0.1/RankOrder</a>|
|rdfs:label|sub order|

