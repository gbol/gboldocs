# CollectionSampleType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|skos:definition|The types of collections in which a sample can be stored|
|subDomain|DocumentCore|

## skos:member

[SpecimenVoucher](/0.1/SpecimenVoucher)

[CultureCollection](/0.1/CultureCollection)

[OtherCollection](/0.1/OtherCollection)

