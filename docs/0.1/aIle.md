# aIle a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|aIle|
|rdfs:label|allo-Isoleucine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27092902">http://www.wikidata.org/entity/Q27092902</a>|

