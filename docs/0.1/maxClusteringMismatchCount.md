# maxClusteringMismatchCount a ObjectProperty

## Domain

definition: Max clustering mismatch count<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:integer

## Annotations


