# OtherMobileElementType a skos:Concept, [MobileElementType](/0.1/MobileElementType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|ddbjLabel|other|
|rdfs:label|other|

