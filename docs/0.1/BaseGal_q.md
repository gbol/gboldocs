# BaseGal_q a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|gal q|
|rdfs:label|beta-D-galactosylqueuosine|

