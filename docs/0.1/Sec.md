# Sec a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|U|
|ddbjLabel|Sec|
|rdfs:label|Selenocysteine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001455">http://purl.obolibrary.org/obo/SO_0001455</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q408663">http://www.wikidata.org/entity/Q408663</a>|

