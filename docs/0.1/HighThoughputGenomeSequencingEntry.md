# HighThoughputGenomeSequencingEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|HTG|
|rdfs:label|High Throughput Genome sequencing|

