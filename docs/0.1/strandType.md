# strandType a ObjectProperty

## Domain

definition: The type of the strand<br>
[CompleteNASequence](/0.1/CompleteNASequence)

definition: The type of the strand<br>
[UncompleteNASequence](/0.1/UncompleteNASequence)

## Range

[StrandType](/0.1/StrandType)

## Annotations


