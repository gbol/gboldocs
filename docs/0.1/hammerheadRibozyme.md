# hammerheadRibozyme a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small catalytic RNA motif that catalyzes a self-cleavage reaction. Its name comes from its secondary structure which resembles a carpenter's hammer. The hammerhead ribozyme is involved in the replication of some viroid and satellite RNAs.|
|ddbjLabel|hammerhead_ribozyme|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q3817690">http://www.wikidata.org/entity/Q3817690</a>|

