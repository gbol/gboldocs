# GapType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|subDomain|SequenceFeatureCore|
|skos:editorialNote|Belongs to the gapType property, full definition of the AGP Specification need to be made.|

## skos:member

[HeterochromatinGap](/0.1/HeterochromatinGap)

[WithinScaffoldGap](/0.1/WithinScaffoldGap)

[UnknownGap](/0.1/UnknownGap)

[RepeatBetweeenScaffoldsGap](/0.1/RepeatBetweeenScaffoldsGap)

[BetweenScaffoldsGap](/0.1/BetweenScaffoldsGap)

[TelemereGap](/0.1/TelemereGap)

[RepeatWithinScaffoldGap](/0.1/RepeatWithinScaffoldGap)

[CentromereGap](/0.1/CentromereGap)

[ShortArmGap](/0.1/ShortArmGap)

