# haploQuality a ObjectProperty

## Domain

definition: Haplotype qualities<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


