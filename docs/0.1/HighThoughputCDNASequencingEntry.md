# HighThoughputCDNASequencingEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|HTC|
|rdfs:label|High Throughput CDNA sequencing|

