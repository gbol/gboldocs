# ThirdPArtySequenceDataRecord a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|TPA|
|rdfs:label|Third PArty sequence data record|

