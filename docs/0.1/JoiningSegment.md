# JoiningSegment a skos:Concept, [ImmunoglobulinRegionType](/0.1/ImmunoglobulinRegionType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|joining segment of immunoglobulin light and heavy-chains, and T-cell receptor alpha, beta, and gamma chains;|
|ddbjLabel|J_segment|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000470">http://purl.obolibrary.org/obo/SO_0000470</a>|

