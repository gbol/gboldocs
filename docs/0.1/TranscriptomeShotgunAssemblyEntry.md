# TranscriptomeShotgunAssemblyEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|TSA|
|rdfs:label|Transcriptome Shotgun Assembly|

