# QTLMap a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A map that bundles QTLs which were mapped using the same method|
|rdfs:label|QTLMap|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[mapAccession](/0.1/mapAccession)|Unique map accession Identifier|0:1|xsd:string|
|[units](/0.1/units)|Units used for the mapping e.g. Basepairs, centiMorgan|1:1|[MapUnits](/0.1/MapUnits)|
|[mapName](/0.1/mapName)|Name of the map the QTL were mapped on|0:1|xsd:string|
|[parent](/0.1/parent)|Parental strains|0:N|[Organism](/0.1/Organism)|
