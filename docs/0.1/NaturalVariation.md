# NaturalVariation a owl:Class extends [VariationFeature](/0.1/VariationFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A related sequence or allele contains stable mutations from the same gene (e.g., RFLPs, polymorphisms, etc.) which differ from the parent sequence at the given location;|
|rdfs:comment|used to describe alleles, RFLP's,and other naturally occurring mutations and  polymorphisms; variability arising as a result of genetic manipulation (e.g. site directed mutagenesis) should be described with the misc_difference feature;use /replace=\"\" to annotate deletion, e.g.variation   4..5 /replace=\"\"|
|ddbjLabel|variation|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001147">http://purl.obolibrary.org/obo/SO_0001147</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[frequency](/0.1/frequency)|The frequency of this variation within the population|0:1|xsd:Double|
