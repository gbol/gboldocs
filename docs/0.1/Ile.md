# Ile a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|I|
|ddbjLabel|Ile|
|rdfs:label|Isoleucine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001438">http://purl.obolibrary.org/obo/SO_0001438</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q484940">http://www.wikidata.org/entity/Q484940</a>|

