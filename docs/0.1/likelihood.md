# likelihood a ObjectProperty

## Domain

definition: Genotype likelihoods comprised of comma separated floating point log10-scaled likelihoods for all possible genotypes given the set of alleles defined in the REF and ALT fields<br>
[VariantGenotype](/0.1/VariantGenotype)

## Range

xsd:string

## Annotations


