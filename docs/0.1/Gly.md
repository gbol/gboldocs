# Gly a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|G|
|rdfs:label|Glycine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001443">http://purl.obolibrary.org/obo/SO_0001443</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q620730">http://www.wikidata.org/entity/Q620730</a>|

