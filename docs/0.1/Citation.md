# Citation a owl:Class extends [Qualifier](/0.1/Qualifier)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A citation added to a feature|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[reference](/0.1/reference)|The actual reference of the citation|1:1|[bibo:Document](/ontology/bibo/Document)|
|[highlight](/0.1/highlight)|The highlighted text of paper which is important|0:1|xsd:string|
