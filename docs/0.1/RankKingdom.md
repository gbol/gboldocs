# RankKingdom a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: kingdom|
|parentRank|<a href="http://gbol.life/0.1/RankSuperKingdom">http://gbol.life/0.1/RankSuperKingdom</a>|
|rdfs:label|kingdom|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q36732">http://www.wikidata.org/entity/Q36732</a>|

