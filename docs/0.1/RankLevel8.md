# RankLevel8 a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Exact rank unknown: rank level 8|
|parentRank|<a href="http://gbol.life/0.1/RankLevel7">http://gbol.life/0.1/RankLevel7</a>|
|rdfs:label|Rank Level 8|

