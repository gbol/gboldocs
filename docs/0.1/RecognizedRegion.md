# RecognizedRegion a owl:Class extends [GenomicFeature](/0.1/GenomicFeature)

## Subclasses

[BiologicalRecognizedRegion](/0.1/BiologicalRecognizedRegion)

[ArtificialRecognizedRegion](/0.1/ArtificialRecognizedRegion)

## Annotations

|||
|-----|-----|
|skos:definition|A region on genomic sequence that is recognized by another entity or process.|
|subDomain|SequenceFeatureCore|

## Properties

