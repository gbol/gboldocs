# AssemblyGap a owl:Class extends [AssemblyAnnotation](/0.1/AssemblyAnnotation)

## Subclasses

[KnownLengthAssemblyGap](/0.1/KnownLengthAssemblyGap)

[UnknownLengthAssemblyGap](/0.1/UnknownLengthAssemblyGap)

## Annotations

|||
|-----|-----|
|skos:definition|A gap within the sequence|
|rdfs:comment|The gap must be denoted with a series of N's in the sequence, if the assembly gap is of unknown length then number of N must be equal to the estimated length. If that is unknown it default to 100 and so should be denoted with 100 N's in the sequence. No upper or lower limit is set on the size of the gap.|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000730">http://purl.obolibrary.org/obo/SO_0000730</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[linkageEvidence](/0.1/linkageEvidence)|The linkage evidence use to link the sequences on both sides|0:1|[LinkageEvidence](/0.1/LinkageEvidence)|
|[gapType](/0.1/gapType)|The type of the gap|1:1|[GapType](/0.1/GapType)|
