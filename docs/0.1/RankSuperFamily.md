# RankSuperFamily a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: super family|
|parentRank|<a href="http://gbol.life/0.1/RankParvOrder">http://gbol.life/0.1/RankParvOrder</a>|
|rdfs:label|super family|

