# remoteFileName a ObjectProperty

## Domain

definition: The local file name<br>
[RemoteDataFile](/0.1/RemoteDataFile)

## Range

xsd:string

## Annotations


