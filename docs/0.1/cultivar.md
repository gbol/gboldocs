# cultivar a ObjectProperty

## Domain

definition: The cultivar (cultivated variety) of plant from which sample was obtained.<br>
[Sample](/0.1/Sample)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|cultivar|

