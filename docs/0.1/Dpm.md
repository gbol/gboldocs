# Dpm a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Dpm|
|rdfs:label|2,2-Diaminopimelic acid|

