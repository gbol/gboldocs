# DoubleStrandedRNA a skos:Concept, [StrandType](/0.1/StrandType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A double stranded RNA molecule|

