# PatentEntry a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|PAT|
|rdfs:label|Patent|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q253623">http://www.wikidata.org/entity/Q253623</a>|

