# matrixElementType a ObjectProperty

## Domain

definition: Matrix element type<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:string

## Annotations


