# ProvenanceAssembly a owl:Class extends [ProvenanceApplication](/0.1/ProvenanceApplication)

## Subclasses

[MegaHit](/0.1/MegaHit)

## Annotations

|||
|-----|-----|
|subDomain|ElementWiseProv|

## Properties

