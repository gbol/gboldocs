# phenotype a ObjectProperty

## Domain

definition: Associated phenotypic property<br>
[Feature](/0.1/Feature)

definition: Associated phenotypic property<br>
[ExonList](/0.1/ExonList)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|phenotype|
|skos:editorialNote|Added to feature, every feature can have an associated phenotypic effect|

