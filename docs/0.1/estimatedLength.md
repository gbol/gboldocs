# estimatedLength a ObjectProperty

## Domain

definition: The estimated gap length, if not given its unknown and defaults to 100<br>
[UnknownLengthAssemblyGap](/0.1/UnknownLengthAssemblyGap)

## Range

xsd:integer

## Annotations

|||
|-----|-----|
|skos:editorialNote|Added to unknown assembly gap, to indicate the estimated length of the gap|

