# SingleStrandedDNA a skos:Concept, [StrandType](/0.1/StrandType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A single stranded DNA molecule|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q10906438">http://www.wikidata.org/entity/Q10906438</a>|

