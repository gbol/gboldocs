# Arctic_Ocean a skos:Concept, [Ocean](/0.1/Ocean)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|Arctic Ocean|
|rdfs:label|Arctic Ocean|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q788">http://www.wikidata.org/entity/Q788</a>|

