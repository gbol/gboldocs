# PartGapKnownLength a owl:Class extends [PartGap](/0.1/PartGap)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A gap within the assembly with a known length|
|subDomain|SequenceFeatureCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[gapLength](/0.1/gapLength)|Denote the length of the gap|1:1|xsd:Integer|
