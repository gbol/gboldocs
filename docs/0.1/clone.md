# clone a ObjectProperty

## Domain

definition: If a clone library was used for sequencing, the sequence can originate from one or more clones<br>
[NASequence](/0.1/NASequence)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|clone|

