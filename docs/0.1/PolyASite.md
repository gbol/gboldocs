# PolyASite a owl:Class extends [TranscriptFeature](/0.1/TranscriptFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Site on an RNA transcript to which will be added adenine residues by post-transcriptional polyadenylation;|
|ddbjLabel|polyA_site|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000553">http://purl.obolibrary.org/obo/SO_0000553</a>|

## Properties

