# mapLocation a ObjectProperty

## Domain

definition: The map location on the chromosome from which the sample was obtained.<br>
[NAFeature](/0.1/NAFeature)

## Range

xsd:string

## Annotations


