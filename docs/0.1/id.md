# id a ObjectProperty

## Domain

definition: The Id used, typically typed as a  prefix<br>
[Database](/0.1/Database)

definition: The Id used, typically typed as a  prefix<br>
[ssb:NGTax](/0.1/NGTax)

## Range

xsd:string

## Annotations


