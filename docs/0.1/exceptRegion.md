# exceptRegion a ObjectProperty

## Domain

definition: The position of region which translate to other amino acid then the default translation table<br>
[TranslExcept](/0.1/TranslExcept)

## Range

[Region](/0.1/Region)

## Annotations


