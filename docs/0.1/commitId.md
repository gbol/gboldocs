# commitId a ObjectProperty

## Domain

definition: The String that uniquely identifies the last commit of the code used.<br>
[AnnotationSoftware](/0.1/AnnotationSoftware)

## Range

xsd:string

## Annotations


