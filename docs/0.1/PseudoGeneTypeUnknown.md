# PseudoGeneTypeUnknown a skos:Concept, [PseudoGeneType](/0.1/PseudoGeneType)

## Subclasses

## Annotations

|||
|-----|-----|
|gen:defaultEnumerationValue|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|skos:definition|the submitter does not know the method of pseudogenisation.|
|ddbjLabel|unknown|

