# AnnotationActivity a owl:Class extends [prov:Activity](/ns/prov/Activity)

## Subclasses

[ManualAnnotationActivity](/0.1/ManualAnnotationActivity)

[AutomaticAnnotationActivity](/0.1/AutomaticAnnotationActivity)

## Annotations

|||
|-----|-----|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[prov:startedAtTime](/ns/prov/startedAtTime)|The time at which the activity is started|1:1|xsd:DateTime|
|[prov:endedAtTime](/ns/prov/endedAtTime)|The time at which the activity is ended, if unknown set equal to startedAt.|1:1|xsd:DateTime|
