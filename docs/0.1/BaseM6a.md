# BaseM6a a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|m6a|
|rdfs:label|N6-methyladenosine|

