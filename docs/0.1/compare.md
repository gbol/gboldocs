# compare a ObjectProperty

## Domain

definition: The functional name<br>
[UpdatedSequence](/0.1/UpdatedSequence)

## Range

xsd:anyURI

## Annotations

|||
|-----|-----|
|ddbjLabel|compare|
|skos:editorialNote|Only present in Updated sequence feature, with a reference to the old sequence|

