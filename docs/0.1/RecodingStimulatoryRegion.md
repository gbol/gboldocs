# RecodingStimulatoryRegion a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A site in an mRNA sequence that stimulates the recoding of a region in the same mRNA.|
|ddbjLabel|recoding_stimulatory_region|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_1001268">http://purl.obolibrary.org/obo/SO_1001268</a>|

