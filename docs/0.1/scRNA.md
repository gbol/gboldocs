# scRNA a skos:Concept, [ncRNAType](/0.1/ncRNAType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|small cytoplasmic RNA; any one of several small cytoplasmic RNA molecules present in the cytoplasm and (sometimes) nucleus of a eukaryote.|
|ddbjLabel|scRNA|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000013">http://purl.obolibrary.org/obo/SO_0000013</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q9334138">http://www.wikidata.org/entity/Q9334138</a>|

