# BaseAc4c a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|ac4c|
|rdfs:label|4-acetylcytidine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109291">http://www.wikidata.org/entity/Q27109291</a>|

