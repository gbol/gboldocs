# Sample a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|Description of the biological sample. Each sequence should define from which sample they are derived.|
|subDomain|DocumentCore|
|skos:editorialNote|# removed & moved<br><br>#bioMaterial xsd:string?;<br>#cultureCollection xsd:string?;<br>#specimenVoucher xsd:string?;<br>#--> merged to sourceMaterial, use collectionSampleType to identify type<br>#country type::Country?; -> replaced with geographicalLocation<br>#latLon-> replaced with geographicalCoordinate<br>#serovar xsd:string?; -> killed store under seroType<br>#focus xsd:boolean?; --> replaced with the integratedInto property in the sequence object<br>#transgenic xsd:boolean?; --> replaced with the integratedInto property in the sequence object<br>#mol_type --> removed replaced with strandType & NASequence subclasses<br><br>#germline xsd:boolean?; --> moved to sequence & removed replace with rearranged = false<br>#rearranged xsd:boolean?; --> moved to sequence<br><br>#clone xsd:string?;  --> move to sequence, is a bit outdated now a days<br>#cloneLib xsd:string?; --> move to sequence, is a bit outdated now a days<br>#proviral xsd:boolean?; --> moved to sequence & set proviralExtraction = true on sample<br>#division --> removed information already captures by the taxonomy property<br>#organism --> replaced with taxonomy in organism object linked to sequence object<br>#type_material --> replaced with the nomenclaturalType in the TaxonomyRef under the Sequence object|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[proviralExtraction](/0.1/proviralExtraction)|Indicate that this sample is an proviral sequence extracted from another organism using PCR.|0:1|xsd:Boolean|
|[map](/0.1/map)|The map location on the chromosome from which the sample was obtained (using PCR).|0:1|xsd:string|
|[metadata](/0.1/metadata)|Additional sample metadata|0:N|IRI|
|[strain](/0.1/strain)|The name or identifier of the strain|0:1|xsd:string|
|[ecotype](/0.1/ecotype)|The specific name of the host from which the sample has been collected, where the name indicates to which habitat/ecological environment the host has adopted to. Example: 'Columbia', which indicate that this Arabidopsis adopted itself with a hairier phenotype to deal with the especially sunny habitat.|0:1|xsd:string|
|[subClone](/0.1/subClone)|The name or identifier of the sub-clone from which the sample was obtained (using PCR).|0:1|xsd:string|
|[isolate](/0.1/isolate)|Individual isolate from which the sequence was obtained. For example ('Patient #152','DGGE band PSBAC-13')|0:1|xsd:string|
|[haplogroup](/0.1/haplogroup)|For the host from which the sample was obtained the name for a group of similar haplotypes that share some sequence variation. Haplogroups are often used to track migration of population groups.|0:1|xsd:string|
|[devStage](/0.1/devStage)|The developmental stage at which the organism was when the sample was obtained|0:1|xsd:string|
|[sex](/0.1/sex)|The sex of the organism from which the sample was obtained. Sex is used for eukaryotic organisms that undergo meiosis with sexually dimorphic gametes. For prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes please use the mathingType property.|0:1|xsd:string|
|[plasmid](/0.1/plasmid)|The name or identifier of plasmid from which the sample was obtained (using PCR).|0:1|xsd:string|
|[barcodes](/0.1/barcodes)|Barcode sequence set|0:1|[BarcodeSet](/0.1/BarcodeSet)|
|[popVariant](/0.1/popVariant)|The name of the subpopulation or phenotype from which the sample was collected.|0:1|xsd:string|
|[haplotype](/0.1/haplotype)|For the host from which the sample was obtained the name for a combination of alleles that are linked together on the same physical chromosome. In the absence of recombination, each haplotype is inherited as a unit, and may be used to track gene flow in populations.|0:1|xsd:string|
|[name](/0.1/name)|A name for the sample|1:1|xsd:string|
|[isolationSource](/0.1/isolationSource)|Describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived. Use geographicalLocation to denote the geographical location (examples: 'permanent Antarctic sea ice', 'rumen isolates from standard Pelleted ration-fed steer #67', 'denitrifying activated sludge from carbon limited continuous reactor')|0:1|xsd:string|
|[labHost](/0.1/labHost)|Scientific name of the laboratory host used to propagate the source organism from which the sample was obtained. The full binomial scientific name of the host organism should be used when known. Extra conditional information relating to the host may also be included. For example '12 year old human with IBD' for a gut bacteria.|0:1|xsd:string|
|[geographicalCoordinate](/0.1/geographicalCoordinate)|The geographical coordinates(latitude + longitude) of the location where the specimen was collected|0:1|[GeographicalCoordinate](/0.1/GeographicalCoordinate)|
|[tissueLib](/0.1/tissueLib)|The tissue library from which the sample was obtained|0:1|xsd:string|
|[organelle](/0.1/organelle)|Type of membrane-bound intracellular structure from which the sample was obtained. Use a 'cellular component' term from the GO ontology.|0:1|IRI|
|[cellType](/0.1/cellType)|The cell type from which the sequence was obtained, please use term from 'Cell Ontology' (http://cellontology.org/) where possible|0:1|xsd:string|
|[subSpecies](/0.1/subSpecies)|The name or identifier of the sub-species from which the sample was obtained (using PCR).|0:1|xsd:string|
|[altitude](/0.1/altitude)|Altitude of the location from which the sample was collected in meters above or below nominal sea level|0:1|xsd:Double|
|[cellLine](/0.1/cellLine)|The cell line from which the sequence was obtained, please use term from 'Cell Line Ontology' (http://www.clo-ontology.org/) where possible|0:1|xsd:string|
|[tissueType](/0.1/tissueType)|The tissue type from which the sample was obtained, use term from the 'BRENDA Tissue and Enzyme Source Ontology' whenever possible|0:1|xsd:string|
|[cultivar](/0.1/cultivar)|The cultivar (cultivated variety) of plant from which sample was obtained.|0:1|xsd:string|
|[matingType](/0.1/matingType)|The mating type of the organism from which the sample was obtained. Mating type is used for prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes. For eukaryotic organisms that undergo meiosis with sexually dimorphic gametes please use the sex property.|0:1|xsd:string|
|[citation](/0.1/citation)|Any publication associated to feature annotation|0:N|[Citation](/0.1/Citation)|
|[sourceMaterial](/0.1/sourceMaterial)|Identification of the material in a collection from which the sample is derived. Entries with more than one sampleSource indicates that the sequence was obtained from a sample that was deposited (by the submitter or a collaborator) in more than one collection.|0:N|[MaterialSource](/0.1/MaterialSource)|
|[subStrain](/0.1/subStrain)|The name or identifier of a genetically or otherwise modified strain from which the sample was obtained (using PCR). The parental strain from which it is derived should be annotated with the strain property.|0:1|xsd:string|
|[description](/0.1/description)|An optional short description|0:1|xsd:string|
|[chromosome](/0.1/chromosome)|The chromosome name or number|0:1|xsd:string|
|[collectedBy](/0.1/collectedBy)|The persons or institute who collected the specimen|0:N|[prov:Agent](/ns/prov/Agent)|
|[identifiedBy](/0.1/identifiedBy)|The name of the expert who identified the specimen taxonomically|0:1|[prov:Agent](/ns/prov/Agent)|
|[asv](/0.1/asv)|ASV sequence set|0:N|[ASVSet](/0.1/ASVSet)|
|[xref](/0.1/xref)|Reference to other entries holding entries with related information in other databases|0:N|[XRef](/0.1/XRef)|
|[variety](/0.1/variety)|The variety of the host from which the sample was derived. Use cultivar if the host is a cultivated plant.|0:1|xsd:string|
|[host](/0.1/host)|The host of the organism (from which the sample was obtained), which it lives in, upon or attached to in its natural environment.|0:1|[TaxonomyRef](/0.1/TaxonomyRef)|
|[PCRPrimers](/0.1/PCRPrimers)||0:1|[PCRPrimerSet](/0.1/PCRPrimerSet)|
|[collectionDate](/0.1/collectionDate)|The date and time on which the specimen was collected.|0:1|xsd:DateTime|
|[geographicalLocation](/0.1/geographicalLocation)|The geographical location(country or ocean) from which the sample is collected|0:1|[GeographicalLocation](/0.1/GeographicalLocation)|
|[segment](/0.1/segment)|The name or identifier of virus or phage (segment) from which the sample was obtained.|0:1|xsd:string|
|[environmentalSample](/0.1/environmentalSample)|This denotes that this sample is the result of a bulk NA extraction method, therefore the organism of the sample and associated sequence(s) are not (exactly) known.|0:1|xsd:Boolean|
|[serotype](/0.1/serotype)|The serological typing of the species within the sample by its antigenic properties|0:1|xsd:string|
|[note](/0.1/note)|A set off some additional notes|0:N|[Note](/0.1/Note)|
