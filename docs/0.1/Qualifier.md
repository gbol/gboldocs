# Qualifier a owl:Class

## Subclasses

[XRef](/0.1/XRef)

[Note](/0.1/Note)

[Citation](/0.1/Citation)

## Annotations

|||
|-----|-----|
|rdfs:comment|The origin point the entity you which the qualifier belong, such a LinkSet or AnnotationResult. Whereas the provenance point to the supporting evidence of the qualifier|
|subDomain|ElementWiseProv|
|rdfs:label|Qualifier|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[provenance](/0.1/provenance)||1:1|[Provenance](/0.1/Provenance)|
