# FeatureProvenance a owl:Class extends [Provenance](/0.1/Provenance)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|All subclasses of FeatureProvenance belong to the extensional part of the GBOL ontology. Each subclass is specific to a tool.|
|subDomain|ElementWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[onProperty](/0.1/onProperty)|Provenance applies to the given properties or the existence of the feature it self (use gbol:Existence), if not defined it all applies to all properties.|0:N|IRI|
