# HighLevelAssemblyInformation a skos:Concept, [EntryType](/0.1/EntryType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|CON|
|rdfs:label|High level assembly information|

