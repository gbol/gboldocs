# onProperty a ObjectProperty

## Domain

definition: Provenance applies to the given properties or the existence of the feature it self (use gbol:Existence), if not defined it all applies to all properties.<br>
[FeatureProvenance](/0.1/FeatureProvenance)

## Range

xsd:anyURI

## Annotations


