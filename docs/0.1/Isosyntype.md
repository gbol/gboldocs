# Isosyntype a skos:Concept, [NomenclaturalType](/0.1/NomenclaturalType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|duplicate of a syntype|
|ddbjLabel|isosyntype|
|rdfs:label|isosyntype|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q55195195">http://www.wikidata.org/entity/Q55195195</a>|

