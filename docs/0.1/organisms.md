# organisms a ObjectProperty

## Domain

definition: All associated organisms<br>
[GBOLDataSet](/0.1/GBOLDataSet)

## Range

[Organism](/0.1/Organism)

## Annotations


