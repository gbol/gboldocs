# Location a owl:Class

## Subclasses

[Base](/0.1/Base)

[Region](/0.1/Region)

[CollectionOfRegions](/0.1/CollectionOfRegions)

[InBetween](/0.1/InBetween)

## Annotations

|||
|-----|-----|
|rdfs:comment|If reference is not given it is equal to the reference of the parent RegionOfRegion object or is equal to parent Sequence Object.|
|subDomain|LocationCore|
|skos:editorialNote|We put the reference property in the LocatableObject class and not in the Position class as in the Faldo ontology. A position within sequence A and within sequence B does not make any sense, it is possible to determine what's in between, therefore the reference should be on the region object. However a region can be collection of other regions spread over different reference sequences, such as the mdg4 gene of Drosophila.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[referenceSequence](/0.1/referenceSequence)|Optionally define another reference sequence then the parent sequence object, this is for example needed for the mdg4 gene of Drosophila, which is transcript from two chromosomes. If the element to which the location belongs is defined on multiple sequence objects, this field is obligatory.|0:1|[Sequence](/0.1/Sequence)|
