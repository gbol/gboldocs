# releaseStatus a ObjectProperty

## Domain

definition: A free string to keep track of the release status (for private use, not present on public entries)<br>
[PublishedGBOLDataSet](/0.1/PublishedGBOLDataSet)

## Range

xsd:string

## Annotations


