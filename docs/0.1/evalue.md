# evalue a ObjectProperty

## Domain

definition: The probability of an alignment<br>
[sapp:Priam](/sapp/0.1/Priam)

definition: The probability of an alignment<br>
[sapp:Blast](/sapp/0.1/Blast)

definition: The probability of an alignment<br>
[sapp:InterProScan](/sapp/0.1/InterProScan)

## Range

xsd:double

## Annotations


