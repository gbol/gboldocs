# Library a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[accpetedSameBarcodeRatio](/0.1/accpetedSameBarcodeRatio)|Accepted same barcode ration|1:1|xsd:Float|
|[sample](/0.1/sample)|Sample from which this piece of sequence originates from.|1:N|[Sample](/0.1/Sample)|
|[libraryNum](/0.1/libraryNum)|Library number|1:1|xsd:Integer|
|[barcodeHitsAccepted](/0.1/barcodeHitsAccepted)|Barcode hits accepted|1:1|xsd:Integer|
|[fBarcodeFile](/0.1/fBarcodeFile)|Forward barcode file name|0:1|xsd:string|
|[barcodeHitsAcceptedRatio](/0.1/barcodeHitsAcceptedRatio)|Barcode hits accepted ratio|1:1|xsd:Float|
|[primerHitsAcceptedRatio](/0.1/primerHitsAcceptedRatio)|Primer hits accepted ratio|1:1|xsd:Float|
|[provenance](/0.1/provenance)|The provenance of the feature|1:1|[Provenance](/0.1/Provenance)|
|[rBarcodeFile](/0.1/rBarcodeFile)|Reverse barcode file name|0:1|xsd:string|
|[fBarcodeLength](/0.1/fBarcodeLength)|Forward barcode length|1:1|xsd:Integer|
|[rFile](/0.1/rFile)|Reverse file name|1:1|xsd:string|
|[totalReads](/0.1/totalReads)|Total reads|1:1|xsd:Integer|
|[primerHitsAccepted](/0.1/primerHitsAccepted)|Primer hits accepted|1:1|xsd:Integer|
|[fFile](/0.1/fFile)|Forward file name|1:1|xsd:string|
|[rBarcodeLength](/0.1/rBarcodeLength)|Reverse barcode length|1:1|xsd:Integer|
