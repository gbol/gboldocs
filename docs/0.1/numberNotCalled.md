# numberNotCalled a ObjectProperty

## Domain

definition: Number of samples not called<br>
[Variation](/0.1/Variation)

## Range

xsd:integer

## Annotations


