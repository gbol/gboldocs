# Domain a skos:Concept, [ProteinDomainType](/0.1/ProteinDomainType)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A preserved domain, domain a typical building blocks of a protein or protein families.|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000417">http://purl.obolibrary.org/obo/SO_0000417</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q898273">http://www.wikidata.org/entity/Q898273</a>|

