# Minus10Signal a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A conserved region about 10-bp upstream of the start point of bacterial transcription units which may be involved in binding RNA polymerase; consensus=TAtAaT. This region is associated with sigma factor 70.|
|ddbjLabel|-10_signal|
|ddbjLabel|minus_10_signal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000175">http://purl.obolibrary.org/obo/SO_0000175</a>|

