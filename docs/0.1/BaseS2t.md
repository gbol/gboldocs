# BaseS2t a skos:Concept, [BaseType](/0.1/BaseType)

## Subclasses

## Annotations

|||
|-----|-----|
|ddbjLabel|s2t|
|rdfs:label|5-methyl-2-thiouridine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27109319">http://www.wikidata.org/entity/Q27109319</a>|

