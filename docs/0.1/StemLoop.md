# StemLoop a owl:Class extends [StructureFeature](/0.1/StructureFeature)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|hairpin; a double-helical region formed by base-pairing between adjacent (inverted) complementary sequences in a single strand of RNA or DNA.|
|ddbjLabel|stem_loop|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000313">http://purl.obolibrary.org/obo/SO_0000313</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q418900">http://www.wikidata.org/entity/Q418900</a>|

## Properties

