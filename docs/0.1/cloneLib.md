# cloneLib a ObjectProperty

## Domain

definition: If a clone library was used for sequencing, specify the clone library<br>
[NASequence](/0.1/NASequence)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|ddbjLabel|clone_lib|

