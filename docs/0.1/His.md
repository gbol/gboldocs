# His a skos:Concept, [AminoAcid](/0.1/AminoAcid)

## Subclasses

## Annotations

|||
|-----|-----|
|aminoAcidLetter|H|
|ddbjLabel|His|
|rdfs:label|Histidine|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001452">http://purl.obolibrary.org/obo/SO_0001452</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q485277">http://www.wikidata.org/entity/Q485277</a>|

