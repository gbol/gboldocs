# modBase a ObjectProperty

## Domain

definition: The type of modification<br>
[ModifiedBase](/0.1/ModifiedBase)

## Range

[BaseType](/0.1/BaseType)

## Annotations

|||
|-----|-----|
|ddbjLabel|mod_base|
|skos:editorialNote|Enumeration type added, property is in modified base feature|

