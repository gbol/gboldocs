# scientificName a ObjectProperty

## Domain

definition: The taxonomic name of the organism from which the sequence originates, should match to the name of the referenced taxonomy<br>
[Organism](/0.1/Organism)

## Range

xsd:string

## Annotations


