# Minus12Signal a skos:Concept, [RegulatoryClass](/0.1/RegulatoryClass)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A conserved region about 12-bp upstream of the start point of bacterial transcription units, involved with sigma factor 54.|
|ddbjLabel|-12_signal|
|ddbjLabel|minus_12_signal|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0001673">http://purl.obolibrary.org/obo/SO_0001673</a>|

