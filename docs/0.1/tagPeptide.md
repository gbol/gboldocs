# tagPeptide a ObjectProperty

## Domain

definition: base location encoding the polypeptide for proteolysis tag of tmRNA and its termination codon;<br>
[tmRNA](/0.1/tmRNA)

## Range

[Region](/0.1/Region)

## Annotations

|||
|-----|-----|
|ddbjLabel|tag_peptide|
|skos:editorialNote|property of tmRNA, points to a Region|

