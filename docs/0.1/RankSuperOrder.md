# RankSuperOrder a skos:Concept, [Rank](/0.1/Rank)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|The taxonomic rank: super order|
|parentRank|<a href="http://gbol.life/0.1/RankInfraClass">http://gbol.life/0.1/RankInfraClass</a>|
|rdfs:label|super order|

