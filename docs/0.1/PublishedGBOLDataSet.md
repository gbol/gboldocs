# PublishedGBOLDataSet a owl:Class extends [GBOLDataSet](/0.1/GBOLDataSet)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|A GBOLDataSet that has been published|
|rdfs:comment|This class can also describe converted GenBank or EMBL entries that have been published|
|gen:framing|:dblinks|
|subDomain|DocumentCore|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[holddate](/0.1/holddate)|The date until which the GBOL dataset should not published (only present on non public entries)|0:1|xsd:Date|
|[lastPublishedDate](/0.1/lastPublishedDate)|The last time the this GBOL data has been published|0:1|xsd:Date|
|[abstract](/0.1/abstract)|The abstract of the publication|0:1|xsd:string|
|[releaseStatus](/0.1/releaseStatus)|A free string to keep track of the release status (for private use, not present on public entries)|0:1|xsd:string|
|[lastPublishedRelease](/0.1/lastPublishedRelease)|The version of the version that has been last published|0:1|xsd:Integer|
|[dblinks](/0.1/dblinks)|Cross references to other databases containing the same record|0:N|[XRef](/0.1/XRef)|
|[firstPublic](/0.1/firstPublic)|The date at which the record was first published|0:1|xsd:Date|
|[firstPublicRelease](/0.1/firstPublicRelease)|The version of the record when it was first published|0:1|xsd:Integer|
