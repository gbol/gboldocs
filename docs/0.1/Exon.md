# Exon a owl:Class extends [TranscriptionElement](/0.1/TranscriptionElement)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|region of genome that codes for portion of spliced mRNA,rRNA and tRNA; may contain 5'UTR, all CDSs and 3' UTR;|
|ddbjLabel|exon|
|subDomain|SequenceFeatureCore|
|skos:exactMatch|<a href="http://purl.obolibrary.org/obo/SO_0000147">http://purl.obolibrary.org/obo/SO_0000147</a>|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q373027">http://www.wikidata.org/entity/Q373027</a>|

## Properties

