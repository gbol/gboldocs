## Obtaining GBOL
All projects related to GBOL (ontology and API's) are located in the GBOL Group at [GitLab](https://gitlab.com/gbol)

The ontology is written in a combination of OWL and ShEx and is located [here](https://gitlab.com/gbol/GBOLOntology) in the gbol-ontology.ttl file. 

Additional ontology files in the ShEx and OWL format are present in the [generated](https://gitlab.com/gbol/GBOLOntology/tree/master/generated) folder in the *GBOLOntology* project. 

## API
We have generated an R and JAVA api and are located in the corresponding repositories ([RGBOLapi](https://gitlab.com/gbol/RGBOLApi), [GBOLapi](https://gitlab.com/gbol/GBOLapi)). The JAVA api is written using the Gradle build system and can either be used directly as a project or as a dependency. 