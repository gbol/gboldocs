# dc:title a ObjectProperty

## Domain

definition: The name of the dataset.<br>
[void:Dataset](/ns/void/Dataset)

[bibo:Literature](/ontology/bibo/Literature)

## Range

xsd:string

## Annotations


