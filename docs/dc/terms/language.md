# dc:language a ObjectProperty

## Domain

definition: The language of this resource.<br>
[void:Dataset](/ns/void/Dataset)

[bibo:Literature](/ontology/bibo/Literature)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to link a bibliographic resource to the language used to express it.|

