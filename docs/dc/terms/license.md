# dc:license a ObjectProperty

## Domain

definition: A legal document giving official permission to do something with this dataset. It must be an IRI referencing to an resolvable URI returning the text of the license.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


