# dc:publisher a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Organization](/ns/prov/Organization)

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to link a bibliographic item to its publisher.|

