# dc:hasVersion a ObjectProperty

## Domain

definition: The textual description of the version of the document, please not the version property in GBOLDataSet<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:string

## Annotations


