# dc:description a ObjectProperty

## Domain

definition: A textual description of the dataset.<br>
[void:Dataset](/ns/void/Dataset)

[bibo:Literature](/ontology/bibo/Literature)

## Range

xsd:string

## Annotations


