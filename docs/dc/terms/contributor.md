# dc:contributor a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to link a bibliographic item to one of its contributor: can be an author, an editor, a publisher, etc.|

