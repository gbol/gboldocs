# dc:format a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[dc:MediaTypeOrExtent](/dc/terms/MediaTypeOrExtent)

## Annotations

|||
|-----|-----|
|skos:example|<dc:format><br>   <dc:MediaTypeOrExtent><br>     <rdf:value>text/html</rdf:value><br>     <rdfs:label>HTML</rdfs:label><br>   </dc:MediaTypeOrExtent><br> </dc:format>|
|skos:scopeNote|Used to describe the format of a bibliographic resource.|

