# dc:MediaTypeOrExtent a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|subDomain|BIBO|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rdf:value](/1999/02/22-rdf-syntax-ns/value)|Used in describing structured values.|0:1|xsd:string|
|[rdfs:label](/2000/01/rdf-schema/label)|Used to provide a human-readable version of a resource's name|1:1|xsd:string|
