# Queries

To use the full power of GBOL SPARQL is essential. Here we post some of the queries that might come in handy and that have been used in the past (in combination with SAPP or without)

#### Retrieval of protein domains
```
PREFIX gbol: <http://gbol.life/0.1/>
select DISTINCT ?gene ?accession where { 
	?gene a gbol:Gene .
	?gene gbol:transcript ?transcript .
    ?transcript gbol:feature ?cds .
    ?cds gbol:protein ?protein .
    ?protein gbol:feature ?domain .
    ?domain gbol:xref ?xref .
    ?xref gbol:db <http://gbol.life/0.1/db/pfam>  .
    ?xref gbol:accession ?accession .
}
```

<!-- To be enabled when transcriptomics is available
```sql
PREFIX gbol: <http://gbol.life/0.1/>
select DISTINCT ?name ?gene ?readCounts where { 
	?gene a gbol:Gene .
	?rawcounts gbol:gene ?gene .
    ?rawcounts a gbol:RawCounts . 
    ?rawcounts gbol:readCounts ?readCounts .
    FILTER(?readCounts > 0)
    ?rawcounts gbol:experiment ?experiment .
    ?experiment gbol:inputReads ?input .
	?input gbol:file1 ?file1 . 
    ?file1 gbol:name ?name .
}
```-->