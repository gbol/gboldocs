# foaf:depiction a ObjectProperty

## Domain

definition: Image of the person use link to image<br>
[foaf:Person](/foaf/0.1/Person)

definition: Image of the person use link to image<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:anyURI

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to link an agent with an image that depict it.|

