# foaf:familyName a ObjectProperty

## Domain

definition: The familyName of the person, preferable use the surName property<br>
[foaf:Person](/foaf/0.1/Person)

definition: The familyName of the person, preferable use the surName property<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations


