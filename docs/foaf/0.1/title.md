# foaf:title a ObjectProperty

## Domain

definition: Title of the person  (Mr, Mrs, Ms, Dr. etc)<br>
[foaf:Person](/foaf/0.1/Person)

definition: Title of the person  (Mr, Mrs, Ms, Dr. etc)<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations


