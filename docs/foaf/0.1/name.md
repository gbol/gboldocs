# foaf:name a ObjectProperty

## Domain

definition: A human readable (user) name of the agent<br>
[foaf:Agent](/foaf/0.1/Agent)

definition: Name of the agent<br>
[prov:Agent](/ns/prov/Agent)

## Range

xsd:string

## Annotations


