# foaf:phone a ObjectProperty

## Domain

definition: The phone number of the entity<br>
[prov:Organization](/ns/prov/Organization)

definition: The phone number of the entity<br>
[foaf:Person](/foaf/0.1/Person)

definition: The phone number of the entity<br>
[foaf:Organization](/foaf/0.1/Organization)

definition: The phone number of the entity<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations


