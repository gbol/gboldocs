# foaf:logo a ObjectProperty

## Domain

definition: Link to logo of organization<br>
[prov:Organization](/ns/prov/Organization)

definition: Link to logo of organization<br>
[foaf:Organization](/foaf/0.1/Organization)

## Range

xsd:anyURI

## Annotations


