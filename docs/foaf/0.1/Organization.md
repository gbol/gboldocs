# foaf:Organization a owl:Class extends [foaf:Agent](/foaf/0.1/Agent)

## Subclasses

## Annotations

|||
|-----|-----|
|skos:definition|social entity (not necessarily commercial) uniting people into a structured group managing shared means to meet some needs, or to pursue collective goals|
|subDomain|DocumentWiseProv|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q43229">http://www.wikidata.org/entity/Q43229</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rov:legalName](/ns/regorg/legalName)|The legal name of the organization|0:1|xsd:string|
|[foaf:logo](/foaf/0.1/logo)|Link to logo of organization|0:1|IRI|
|[foaf:phone](/foaf/0.1/phone)|The phone number of the entity|0:1|xsd:string|
|[foaf:homepage](/foaf/0.1/homepage)|A homepage for some thing.|0:1|xsd:string|
|[foaf:mbox](/foaf/0.1/mbox)|mail address use  URI scheme (see RFC 2368).|0:1|IRI|
|[foaf:based_near](/foaf/0.1/based_near)|Location to which the organization is based near|0:1|xsd:string|
