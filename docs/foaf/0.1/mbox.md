# foaf:mbox a ObjectProperty

## Domain

definition: mail address use  URI scheme (see RFC 2368).<br>
[prov:Organization](/ns/prov/Organization)

definition: mail address use  URI scheme (see RFC 2368).<br>
[foaf:Person](/foaf/0.1/Person)

definition: mail address use  URI scheme (see RFC 2368).<br>
[foaf:Organization](/foaf/0.1/Organization)

definition: mail address use  URI scheme (see RFC 2368).<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:anyURI

## Annotations


