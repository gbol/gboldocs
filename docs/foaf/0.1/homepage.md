# foaf:homepage a ObjectProperty

## Domain

definition: A homepage for some thing.<br>
[prov:Organization](/ns/prov/Organization)

definition: A homepage for some thing.<br>
[foaf:Person](/foaf/0.1/Person)

definition: A homepage for some thing.<br>
[AnnotationSoftware](/0.1/AnnotationSoftware)

definition: A homepage for some thing.<br>
[foaf:Organization](/foaf/0.1/Organization)

definition: A homepage for some thing.<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|skos:scopeNote|Used to link an agent to its homepage (which is a web page accessible using a URL).|

