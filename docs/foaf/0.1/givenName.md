# foaf:givenName a ObjectProperty

## Domain

definition: The given name of a person<br>
[foaf:Person](/foaf/0.1/Person)

definition: The given name of a person<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations


