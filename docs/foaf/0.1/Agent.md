# foaf:Agent a owl:Class

## Subclasses

[foaf:Person](/foaf/0.1/Person)

[foaf:Organization](/foaf/0.1/Organization)

## Annotations

|||
|-----|-----|
|skos:definition|individual and identifiable entity capable of performing actions<br>edit|
|subDomain|DocumentWiseProv|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q24229398">http://www.wikidata.org/entity/Q24229398</a>|
|skos:scopeNote|Used to describe any \"agent\" related to bibliographic items. Such agents can be persons, organizations or groups of any kind.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[foaf:name](/foaf/0.1/name)|A human readable (user) name of the agent|1:1|xsd:string|
