# wv:waiver a ObjectProperty

## Domain

definition: Best practice is use the URI of a waiver legal document as the value of this property.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:string

## Annotations


