# wv:norms a ObjectProperty

## Domain

definition: Norms are not legally binding but represent the general principles or "code of conduct" adopted by a community for access and use of resources<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


