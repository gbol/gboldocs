# bibo:Brief a owl:Class extends [bibo:LegalCaseDocument](/ontology/bibo/LegalCaseDocument)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A written argument submitted to a court.|
|subDomain|BIBO|
|rdfs:label|Brief|

## Properties

