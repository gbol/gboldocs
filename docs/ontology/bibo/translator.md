# bibo:translator a ObjectProperty rdfs:subPropetyOf [dc:contributor](/dc/terms/contributor)

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|A person who translates written document from one language to another.|
|rdfs:label|translator|

