# bibo:suffixName a ObjectProperty

## Domain

definition: The suffix of a name<br>
[foaf:Person](/foaf/0.1/Person)

definition: The suffix of a name<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|rdfs:comment|The suffix of a name|
|rdfs:label|suffix name|

