# bibo:AcademicArticle a owl:Class extends [bibo:Article](/ontology/bibo/Article)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A scholarly academic article, typically published in a journal.|
|subDomain|BIBO|
|rdfs:label|Academic Article|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q18918145">http://www.wikidata.org/entity/Q18918145</a>|

## Properties

