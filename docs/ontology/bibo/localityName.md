# bibo:localityName a ObjectProperty

## Domain

definition: Used to name the locality of a publisher, an author, etc.<br>
[foaf:Person](/foaf/0.1/Person)

definition: Used to name the locality of a publisher, an author, etc.<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations


