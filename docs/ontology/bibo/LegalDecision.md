# bibo:LegalDecision a owl:Class extends [bibo:LegalCaseDocument](/ontology/bibo/LegalCaseDocument)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A document containing an authoritative determination (as a decree or judgment) made after consideration of facts or law.|
|subDomain|BIBO|
|rdfs:label|Decision|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:affirmedBy](/ontology/bibo/affirmedBy)||0:1|[bibo:LegalDecision](/ontology/bibo/LegalDecision)|
|[bibo:subsequentLegalDecision](/ontology/bibo/subsequentLegalDecision)||0:1|[bibo:LegalDecision](/ontology/bibo/LegalDecision)|
|[bibo:reversedBy](/ontology/bibo/reversedBy)||0:1|[bibo:LegalDecision](/ontology/bibo/LegalDecision)|
