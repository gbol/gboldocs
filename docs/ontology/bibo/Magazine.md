# bibo:Magazine a owl:Class extends [bibo:Periodical](/ontology/bibo/Periodical)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A periodical of magazine Articles. A magazine is a publication that is issued periodically, usually bound in a paper cover, and typically contains essays, stories, poems, etc., by many writers, and often photographs and drawings, frequently specializing in a particular subject or area, as hobbies, news, or sports.|
|subDomain|BIBO|
|rdfs:label|Magazine|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q41298">http://www.wikidata.org/entity/Q41298</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Issue](/ontology/bibo/Issue)|
