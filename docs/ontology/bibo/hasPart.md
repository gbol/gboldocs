# bibo:hasPart a ObjectProperty

## Domain

[bibo:Website](/ontology/bibo/Website)

[bibo:CollectedDocument](/ontology/bibo/CollectedDocument)

[bibo:Collection](/ontology/bibo/Collection)

[bibo:Magazine](/ontology/bibo/Magazine)

[bibo:Code](/ontology/bibo/Code)

[bibo:Slideshow](/ontology/bibo/Slideshow)

[bibo:CourtReporter](/ontology/bibo/CourtReporter)

[bibo:Newspaper](/ontology/bibo/Newspaper)

[bibo:Issue](/ontology/bibo/Issue)

[bibo:Series](/ontology/bibo/Series)

[bibo:MultiVolumeBook](/ontology/bibo/MultiVolumeBook)

[bibo:Journal](/ontology/bibo/Journal)

## Range

[bibo:Article](/ontology/bibo/Article)

[bibo:Slide](/ontology/bibo/Slide)

[bibo:Legislation](/ontology/bibo/Legislation)

[bibo:Document](/ontology/bibo/Document)

[bibo:Webpage](/ontology/bibo/Webpage)

[bibo:Issue](/ontology/bibo/Issue)

[bibo:Book](/ontology/bibo/Book)

[bibo:LegalDocument](/ontology/bibo/LegalDocument)

[bibo:Literature](/ontology/bibo/Literature)

## Annotations


