# bibo:Workshop a owl:Class extends [bibo:Event](/ontology/bibo/Event)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A seminar, discussion group, or the like, that emphasizes exchange of ideas and the demonstration and application of techniques, skills, etc.|
|subDomain|BIBO|
|rdfs:label|Workshop|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q27556165">http://www.wikidata.org/entity/Q27556165</a>|

## Properties

