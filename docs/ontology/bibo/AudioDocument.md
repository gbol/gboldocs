# bibo:AudioDocument a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An audio document; aka record.|
|subDomain|BIBO|
|rdfs:label|audio document|

## Properties

