# bibo:Proceedings a owl:Class extends [bibo:Book](/ontology/bibo/Book)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A compilation of documents published from an event, such as a conference.|
|subDomain|BIBO|
|rdfs:label|Proceedings|

## Properties

