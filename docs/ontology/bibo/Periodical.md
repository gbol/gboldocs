# bibo:Periodical a owl:Class extends [bibo:Collection](/ontology/bibo/Collection)

## Subclasses

[bibo:Magazine](/ontology/bibo/Magazine)

[bibo:Code](/ontology/bibo/Code)

[bibo:CourtReporter](/ontology/bibo/CourtReporter)

[bibo:Newspaper](/ontology/bibo/Newspaper)

[bibo:Journal](/ontology/bibo/Journal)

## Annotations

|||
|-----|-----|
|rdfs:comment|A group of related documents issued at regular intervals.|
|subDomain|BIBO|
|rdfs:label|Periodical|
|skos:editorialNote|Error in original BIBO ontology LegalDocument is not an Issue; so removed bibo:hasPart @bibo:Issue+;|

## Properties

