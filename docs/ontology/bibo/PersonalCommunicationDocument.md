# bibo:PersonalCommunicationDocument a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

[bibo:Email](/ontology/bibo/Email)

[bibo:Letter](/ontology/bibo/Letter)

## Annotations

|||
|-----|-----|
|rdfs:comment|A personal communication manifested in some document.|
|subDomain|BIBO|
|rdfs:label|Personal Communication Document|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:recipient](/ontology/bibo/recipient)||0:N|[foaf:Agent](/foaf/0.1/Agent)|
