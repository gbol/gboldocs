# bibo:Document a owl:Class extends [bibo:Literature](/ontology/bibo/Literature)

## Subclasses

[bibo:Image](/ontology/bibo/Image)

[bibo:Report](/ontology/bibo/Report)

[bibo:CollectedDocument](/ontology/bibo/CollectedDocument)

[bibo:Thesis](/ontology/bibo/Thesis)

[bibo:Standard](/ontology/bibo/Standard)

[bibo:PersonalCommunicationDocument](/ontology/bibo/PersonalCommunicationDocument)

[bibo:Note](/ontology/bibo/Note)

[bibo:AudioDocument](/ontology/bibo/AudioDocument)

[bibo:DocumentPart](/ontology/bibo/DocumentPart)

[bibo:LegalDocument](/ontology/bibo/LegalDocument)

[bibo:Manual](/ontology/bibo/Manual)

[bibo:AudioVisualDocument](/ontology/bibo/AudioVisualDocument)

[bibo:Article](/ontology/bibo/Article)

[bibo:Slideshow](/ontology/bibo/Slideshow)

[bibo:Manuscript](/ontology/bibo/Manuscript)

[bibo:Book](/ontology/bibo/Book)

[bibo:Webpage](/ontology/bibo/Webpage)

[bibo:Patent](/ontology/bibo/Patent)

[bibo:ReferenceSource](/ontology/bibo/ReferenceSource)

## Annotations

|||
|-----|-----|
|rdfs:comment|A document (noun) is a bounded physical representation of body of information designed with the capacity (and usually intent) to communicate. A document may manifest symbolic, diagrammatic or sensory-representational information.|
|subDomain|BIBO|
|rdfs:label|Document|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q49848">http://www.wikidata.org/entity/Q49848</a>|

## Properties

