# bibo:DocumentPart a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

[bibo:BookSection](/ontology/bibo/BookSection)

[bibo:Slide](/ontology/bibo/Slide)

[bibo:Excerpt](/ontology/bibo/Excerpt)

## Annotations

|||
|-----|-----|
|rdfs:comment|a distinct part of a larger document or collected document.|
|subDomain|BIBO|
|rdfs:label|document part|

## Properties

