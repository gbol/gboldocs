# bibo:issuer a ObjectProperty rdfs:subPropetyOf [dc:publisher](/dc/terms/publisher)

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|An entity responsible for issuing often informally published documents such as press releases, reports, etc.|
|rdfs:label|issuer|

