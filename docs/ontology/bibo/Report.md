# bibo:Report a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A document describing an account or statement describing in detail an event, situation, or the like, usually as the result of observation, inquiry, etc..|
|subDomain|BIBO|
|rdfs:label|Report|

## Properties

