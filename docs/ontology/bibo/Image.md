# bibo:Image a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

[bibo:Map](/ontology/bibo/Map)

## Annotations

|||
|-----|-----|
|rdfs:comment|A document that presents visual or diagrammatic information.|
|subDomain|BIBO|
|rdfs:label|Image|

## Properties

