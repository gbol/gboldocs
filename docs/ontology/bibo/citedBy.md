# bibo:citedBy a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[bibo:Document](/ontology/bibo/Document)

## Annotations

|||
|-----|-----|
|rdfs:comment|Relates a document to another document that cites the<br>first document.|
|rdfs:label|cited by|

