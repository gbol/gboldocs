# bibo:Newspaper a owl:Class extends [bibo:Periodical](/ontology/bibo/Periodical)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A periodical of documents, usually issued daily or weekly, containing current news, editorials, feature articles, and usually advertising.|
|subDomain|BIBO|
|rdfs:label|Newspaper|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q11032">http://www.wikidata.org/entity/Q11032</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Issue](/ontology/bibo/Issue)|
