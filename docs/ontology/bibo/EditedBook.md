# bibo:EditedBook a owl:Class extends [bibo:CollectedDocument](/ontology/bibo/CollectedDocument)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An edited book.|
|subDomain|BIBO|
|rdfs:label|Edited Book|

## Properties

