# bibo:LegalCaseDocument a owl:Class extends [bibo:LegalDocument](/ontology/bibo/LegalDocument)

## Subclasses

[bibo:Brief](/ontology/bibo/Brief)

[bibo:LegalDecision](/ontology/bibo/LegalDecision)

## Annotations

|||
|-----|-----|
|rdfs:comment|A document accompanying a legal case.|
|subDomain|BIBO|
|rdfs:label|Legal Case Document|

## Properties

