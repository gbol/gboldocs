# bibo:Code a owl:Class extends [bibo:Periodical](/ontology/bibo/Periodical)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A collection of statutes.|
|subDomain|BIBO|
|rdfs:label|Code|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Legislation](/ontology/bibo/Legislation)|
