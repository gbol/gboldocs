# bibo:Manuscript a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An unpublished Document, which may also be submitted to a publisher for publication.|
|subDomain|BIBO|
|rdfs:label|Manuscript|

## Properties

