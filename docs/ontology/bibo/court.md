# bibo:court a ObjectProperty

## Domain

[bibo:LegalDocument](/ontology/bibo/LegalDocument)

## Range

[foaf:Organization](/foaf/0.1/Organization)

## Annotations

|||
|-----|-----|
|rdfs:comment|A court associated with a legal document; for example, that which issues a decision.|
|rdfs:label|court|

