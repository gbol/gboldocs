# bibo:Manual a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A small reference book, especially one giving instructions.|
|subDomain|BIBO|
|rdfs:label|Manual|

## Properties

