# bibo:Quote a owl:Class extends [bibo:Excerpt](/ontology/bibo/Excerpt)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An excerpted collection of words.|
|subDomain|BIBO|
|rdfs:label|Quote|

## Properties

