# bibo:Website a owl:Class extends [bibo:Collection](/ontology/bibo/Collection)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A group of Webpages accessible on the Web.|
|subDomain|BIBO|
|rdfs:label|Website|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Webpage](/ontology/bibo/Webpage)|
