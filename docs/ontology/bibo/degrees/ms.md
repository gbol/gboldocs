# degrees:ms a skos:Concept, [bibo:ThesisDegree](/ontology/bibo/ThesisDegree)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|masters degree in science|
|rdfs:label|M.S.|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q950900">http://www.wikidata.org/entity/Q950900</a>|

