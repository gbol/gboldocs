# degrees:ma a skos:Concept, [bibo:ThesisDegree](/ontology/bibo/ThesisDegree)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|masters degree in arts|
|rdfs:label|M.A.|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q2091008">http://www.wikidata.org/entity/Q2091008</a>|

