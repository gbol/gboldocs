# degrees:phd a skos:Concept, [bibo:ThesisDegree](/ontology/bibo/ThesisDegree)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|PhD degree|
|rdfs:label|PhD degree|

