# bibo:Issue a owl:Class extends [bibo:CollectedDocument](/ontology/bibo/CollectedDocument)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|something that is printed or published and distributed, esp. a given number of a periodical|
|subDomain|BIBO|
|rdfs:label|Issue|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Article](/ontology/bibo/Article)|
|[bibo:issue](/ontology/bibo/issue)||0:1|xsd:string|
