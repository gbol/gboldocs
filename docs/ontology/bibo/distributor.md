# bibo:distributor a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|Distributor of a document or a collection of documents.|
|rdfs:label|distributor|

