# bibo:ThesisDegree a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|rdfs:comment|The academic degree of a Thesis|
|subDomain|BIBO|
|rdfs:label|Thesis degree|

## skos:member

[degrees:ms](/ontology/bibo/degrees/ms)

[degrees:phd](/ontology/bibo/degrees/phd)

[degrees:ma](/ontology/bibo/degrees/ma)

