# bibo:Thesis a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A document created to summarize research findings associated with the completion of an academic degree.|
|subDomain|BIBO|
|rdfs:label|Thesis|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:degree](/ontology/bibo/degree)||0:1|[bibo:ThesisDegree](/ontology/bibo/ThesisDegree)|
