# bibo:CourtReporter a owl:Class extends [bibo:Periodical](/ontology/bibo/Periodical)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A collection of legal cases.|
|subDomain|BIBO|
|rdfs:label|Court Reporter|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:LegalDocument](/ontology/bibo/LegalDocument)|
