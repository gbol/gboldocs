# bibo:Slide a owl:Class extends [bibo:DocumentPart](/ontology/bibo/DocumentPart)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A slide in a slideshow|
|subDomain|BIBO|
|rdfs:label|Slide|

## Properties

