# bibo:Bill a owl:Class extends [bibo:Legislation](/ontology/bibo/Legislation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|Draft legislation presented for discussion to a legal body.|
|subDomain|BIBO|
|rdfs:label|Bill|

## Properties

