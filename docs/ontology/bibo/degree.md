# bibo:degree a ObjectProperty

## Domain

[bibo:Thesis](/ontology/bibo/Thesis)

## Range

[bibo:ThesisDegree](/ontology/bibo/ThesisDegree)

## Annotations

|||
|-----|-----|
|rdfs:comment|The thesis degree.|
|rdfs:label|degree|
|skos:editorialNote|We are not defining, using an enumeration, the range of the bibo:degree to the defined list of bibo:ThesisDegree. We won't do it because we want people to be able to define new degrees if needed by some special use cases. Creating such an enumeration would restrict this to happen.|
|skos:editorialNote|We are not defining, using an enumeration, the range of the bibo:degree to the defined list of bibo:ThesisDegree. We won't do it because we want people to be able to define new degrees if needed by some special use cases. Creating such an enumeration would restrict this to happen.|

