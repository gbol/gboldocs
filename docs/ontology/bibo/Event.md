# bibo:Event a owl:Class

## Subclasses

[bibo:PersonalCommunication](/ontology/bibo/PersonalCommunication)

[bibo:Conference](/ontology/bibo/Conference)

[bibo:Workshop](/ontology/bibo/Workshop)

[bibo:Interview](/ontology/bibo/Interview)

[bibo:Performance](/ontology/bibo/Performance)

[bibo:Hearing](/ontology/bibo/Hearing)

## Annotations

|||
|-----|-----|
|skos:definition|An arbitrary classification of a space/time region, by a cognitive agent. An event may have actively participating agents, passive factors, products, and a location in space/time.|
|subDomain|BIBO|
|skos:exactMatch|<a href="http://purl.org/NET/c4dm/event.owl#Event">http://purl.org/NET/c4dm/event.owl#Event</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:presents](/ontology/bibo/presents)||0:N|[bibo:Document](/ontology/bibo/Document)|
|[bibo:organizer](/ontology/bibo/organizer)||0:N|[foaf:Agent](/foaf/0.1/Agent)|
|[bibo:place](/ontology/bibo/place)||0:N|xsd:string|
