# bibo:Chapter a owl:Class extends [bibo:BookSection](/ontology/bibo/BookSection)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A chapter of a book.|
|subDomain|BIBO|
|rdfs:label|Chapter|

## Properties

