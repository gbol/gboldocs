# bibo:Map a owl:Class extends [bibo:Image](/ontology/bibo/Image)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A graphical depiction of geographic features.|
|subDomain|BIBO|
|rdfs:label|Map|

## Properties

