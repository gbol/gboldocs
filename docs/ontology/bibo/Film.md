# bibo:Film a owl:Class extends [bibo:AudioVisualDocument](/ontology/bibo/AudioVisualDocument)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|aka movie.|
|subDomain|BIBO|
|rdfs:label|Film|

## Properties

