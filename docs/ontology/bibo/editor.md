# bibo:editor a ObjectProperty rdfs:subPropetyOf [dc:contributor](/dc/terms/contributor)

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|A person having managerial and sometimes policy-making responsibility for the editorial part of a publishing firm or of a newspaper, magazine, or other publication.|
|rdfs:label|editor|

