# bibo:authorList a ObjectProperty rdfs:subPropetyOf [bibo:contributorList](/ontology/bibo/contributorList)

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|An ordered list of authors. Normally, this list is seen as a priority list that order authors by importance.|
|rdfs:label|list of authors|

