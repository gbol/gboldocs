# bibo:Interview a owl:Class extends [bibo:Event](/ontology/bibo/Event)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A formalized discussion between two or more people.|
|subDomain|BIBO|
|rdfs:label|Interview|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q178651">http://www.wikidata.org/entity/Q178651</a>|

## Properties

