# bibo:owner a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|Owner of a document or a collection of documents.|
|rdfs:label|owner|

