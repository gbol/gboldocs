# bibo:Performance a owl:Class extends [bibo:Event](/ontology/bibo/Event)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A public performance.|
|subDomain|BIBO|
|rdfs:label|Performance|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q35140">http://www.wikidata.org/entity/Q35140</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:performer](/ontology/bibo/performer)||0:N|[foaf:Agent](/foaf/0.1/Agent)|
