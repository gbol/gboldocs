# bibo:Standard a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A document describing a standard|
|subDomain|BIBO|
|rdfs:label|Standard|

## Properties

