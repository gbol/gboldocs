# bibo:ReferenceSource a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A document that presents authoritative reference information, such as a dictionary or encyclopedia .|
|subDomain|BIBO|
|rdfs:label|Reference Source|

## Properties

