# bibo:contributorList a ObjectProperty

## Domain

[bibo:Literature](/ontology/bibo/Literature)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations

|||
|-----|-----|
|rdfs:comment|An ordered list of contributors. Normally, this list is seen as a priority list that order contributors by importance.|
|rdfs:label|list of contributors|

