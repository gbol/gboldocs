# bibo:Collection a owl:Class extends [bibo:Literature](/ontology/bibo/Literature)

## Subclasses

[bibo:Website](/ontology/bibo/Website)

[bibo:Periodical](/ontology/bibo/Periodical)

[bibo:Series](/ontology/bibo/Series)

[bibo:MultiVolumeBook](/ontology/bibo/MultiVolumeBook)

## Annotations

|||
|-----|-----|
|rdfs:comment|A collection of Documents or Collections|
|subDomain|BIBO|
|rdfs:label|Collection|
|skos:editorialNote|Properties a defined on the root, this is just a temporary note that can be used for that task<br><br>#bibo:distributor @foaf:Agent?;<br>#bibo:editor @foaf:Agent?;<br>#bibo:issuer @foaf:Agent?;<br>#bibo:owner @foaf:Agent?;<br>#bibo:producer @foaf:Agent?;<br>#bibo:translator @foaf:Agent?;<br>#bibo:doi xsd:string?;<br>#bibo:eissn xsd:string?; <br>#bibo:issn xsd:string?; <br>#bibo:asin xsd:string?; <br>#bibo:identifier xsd:string?; <br>#bibo:coden xsd:string?; <br>#bibo:eanucc13 xsd:string?; <br>#bibo:gtin14 xsd:string?; <br>#bibo:handle xsd:string?; <br>#bibo:isbn10 xsd:string?; <br>#bibo:isbn13 xsd:string?; <br>#bibo:lccn xsd:string?; <br>#bibo:numVolumes xsd:string?; <br>#bibo:oclcnum xsd:string?; <br>#bibo:pmid xsd:string?; <br>#bibo:sici xsd:string?; <br>#bibo:upc xsd:string?; <br>#bibo:uri xsd:string?;|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||0:N|[bibo:Literature](/ontology/bibo/Literature)|
|[bibo:numVolumes](/ontology/bibo/numVolumes)||0:1|xsd:string|
|[bibo:eissn](/ontology/bibo/eissn)||0:1|xsd:string|
|[bibo:issn](/ontology/bibo/issn)||0:1|xsd:string|
