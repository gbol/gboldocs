# bibo:Excerpt a owl:Class extends [bibo:DocumentPart](/ontology/bibo/DocumentPart)

## Subclasses

[bibo:Quote](/ontology/bibo/Quote)

## Annotations

|||
|-----|-----|
|rdfs:comment|A passage selected from a larger work.|
|subDomain|BIBO|
|rdfs:label|Excerpt|

## Properties

