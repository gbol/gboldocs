# bibo:CollectedDocument a owl:Class extends [bibo:Document](/ontology/bibo/Document)

## Subclasses

[bibo:EditedBook](/ontology/bibo/EditedBook)

[bibo:Issue](/ontology/bibo/Issue)

## Annotations

|||
|-----|-----|
|rdfs:comment|A document that simultaneously contains other documents.|
|subDomain|BIBO|
|rdfs:label|Collected Document|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||1:N|[bibo:Document](/ontology/bibo/Document)|
