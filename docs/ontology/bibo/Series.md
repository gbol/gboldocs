# bibo:Series a owl:Class extends [bibo:Collection](/ontology/bibo/Collection)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A loose, thematic, collection of Documents, often Books.|
|subDomain|BIBO|
|rdfs:label|Series|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:hasPart](/ontology/bibo/hasPart)||0:N|[bibo:Document](/ontology/bibo/Document)|
