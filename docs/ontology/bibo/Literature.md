# bibo:Literature a owl:Class

## Subclasses

[bibo:Collection](/ontology/bibo/Collection)

[bibo:Document](/ontology/bibo/Document)

## Annotations

|||
|-----|-----|
|gen:virtual|true^^<a href="http://www.w3.org/2001/XMLSchema#boolean">http://www.w3.org/2001/XMLSchema#boolean</a>|
|subDomain|BIBO|
|rdfs:label|Literature|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q7187">http://www.wikidata.org/entity/Q7187</a>|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[bibo:gtin14](/ontology/bibo/gtin14)||0:1|xsd:string|
|[bibo:reviewOf](/ontology/bibo/reviewOf)||0:N|[bibo:Literature](/ontology/bibo/Literature)|
|[bibo:edition](/ontology/bibo/edition)||0:1|xsd:string|
|[bibo:status](/ontology/bibo/status)||0:1|[bibo:DocumentStatus](/ontology/bibo/DocumentStatus)|
|[rdf:value](/1999/02/22-rdf-syntax-ns/value)|Used in describing structured values.|0:1|xsd:string|
|[bibo:pageStart](/ontology/bibo/pageStart)||0:1|xsd:string|
|[bibo:content](/ontology/bibo/content)||0:1|xsd:string|
|[dc:description](/dc/terms/description)||0:1|xsd:string|
|[bibo:handle](/ontology/bibo/handle)||0:1|xsd:string|
|[bibo:numPages](/ontology/bibo/numPages)||0:1|xsd:Integer|
|[bibo:uri](/ontology/bibo/uri)||0:1|xsd:string|
|[dc:subject](/dc/terms/subject)||0:1|xsd:string|
|[bibo:pages](/ontology/bibo/pages)||0:1|xsd:string|
|[bibo:producer](/ontology/bibo/producer)||0:1|[prov:Agent](/ns/prov/Agent)|
|[bibo:authorList](/ontology/bibo/authorList)||0:N|[prov:Agent](/ns/prov/Agent)|
|[bibo:issuer](/ontology/bibo/issuer)||0:1|[prov:Agent](/ns/prov/Agent)|
|[bibo:distributor](/ontology/bibo/distributor)||0:1|[prov:Agent](/ns/prov/Agent)|
|[bibo:pageEnd](/ontology/bibo/pageEnd)||0:1|xsd:string|
|[dc:relation](/dc/terms/relation)||0:N|IRI|
|[dc:publisher](/dc/terms/publisher)||0:1|[prov:Organization](/ns/prov/Organization)|
|[bibo:contributorList](/ontology/bibo/contributorList)||0:N|[prov:Agent](/ns/prov/Agent)|
|[bibo:citedBy](/ontology/bibo/citedBy)||0:N|[bibo:Document](/ontology/bibo/Document)|
|[bibo:identifier](/ontology/bibo/identifier)||0:1|xsd:string|
|[bibo:coden](/ontology/bibo/coden)||0:1|xsd:string|
|[bibo:shortDescription](/ontology/bibo/shortDescription)||0:1|xsd:string|
|[bibo:doi](/ontology/bibo/doi)||0:1|xsd:string|
|[bibo:isbn10](/ontology/bibo/isbn10)||0:1|xsd:string|
|[bibo:pmid](/ontology/bibo/pmid)||0:1|xsd:string|
|[dc:abstract](/dc/terms/abstract)||0:1|xsd:string|
|[bibo:isbn13](/ontology/bibo/isbn13)||0:1|xsd:string|
|[bibo:reproducedIn](/ontology/bibo/reproducedIn)||0:N|[bibo:Document](/ontology/bibo/Document)|
|[bibo:volume](/ontology/bibo/volume)||0:1|xsd:string|
|[bibo:asin](/ontology/bibo/asin)||0:1|xsd:string|
|[bibo:presentedAt](/ontology/bibo/presentedAt)||0:1|[bibo:Event](/ontology/bibo/Event)|
|[bibo:section](/ontology/bibo/section)||0:1|xsd:string|
|[bibo:upc](/ontology/bibo/upc)||0:1|xsd:string|
|[dc:dateSubmitted](/dc/terms/dateSubmitted)||0:1|xsd:DateTime|
|[bibo:editor](/ontology/bibo/editor)||0:1|[prov:Agent](/ns/prov/Agent)|
|[bibo:cites](/ontology/bibo/cites)||0:N|[bibo:Document](/ontology/bibo/Document)|
|[bibo:transcriptOf](/ontology/bibo/transcriptOf)||0:1|[bibo:Literature](/ontology/bibo/Literature)|
|[bibo:locator](/ontology/bibo/locator)||0:1|xsd:string|
|[bibo:eanucc13](/ontology/bibo/eanucc13)||0:1|xsd:string|
|[bibo:editorList](/ontology/bibo/editorList)||0:N|[prov:Agent](/ns/prov/Agent)|
|[bibo:owner](/ontology/bibo/owner)||0:1|[prov:Agent](/ns/prov/Agent)|
|[dc:language](/dc/terms/language)||0:1|xsd:string|
|[bibo:sici](/ontology/bibo/sici)||0:1|xsd:string|
|[bibo:translator](/ontology/bibo/translator)||0:1|[prov:Agent](/ns/prov/Agent)|
|[bibo:translationOf](/ontology/bibo/translationOf)||0:1|[bibo:Document](/ontology/bibo/Document)|
|[dc:format](/dc/terms/format)||0:1|[dc:MediaTypeOrExtent](/dc/terms/MediaTypeOrExtent)|
|[bibo:oclcnum](/ontology/bibo/oclcnum)||0:1|xsd:string|
|[dc:issued](/dc/terms/issued)||0:1|xsd:DateTime|
|[bibo:lccn](/ontology/bibo/lccn)||0:1|xsd:string|
|[bibo:shortTitle](/ontology/bibo/shortTitle)||0:1|xsd:string|
|[bibo:number](/ontology/bibo/number)||0:1|xsd:string|
|[dc:created](/dc/terms/created)||0:1|xsd:Date|
|[dc:contributor](/dc/terms/contributor)||0:N|[prov:Agent](/ns/prov/Agent)|
|[dc:title](/dc/terms/title)||0:1|xsd:string|
|[dc:dateAccepted](/dc/terms/dateAccepted)||0:1|xsd:DateTime|
|[dc:rights](/dc/terms/rights)||0:1|xsd:string|
