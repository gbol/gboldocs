# bibo:prefixName a ObjectProperty

## Domain

definition: The prefix of a name<br>
[foaf:Person](/foaf/0.1/Person)

definition: The prefix of a name<br>
[prov:Person](/ns/prov/Person)

## Range

xsd:string

## Annotations

|||
|-----|-----|
|rdfs:comment|The prefix of a name|
|rdfs:label|prefix name|

