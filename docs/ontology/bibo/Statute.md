# bibo:Statute a owl:Class extends [bibo:Legislation](/ontology/bibo/Legislation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A bill enacted into law.|
|subDomain|BIBO|
|rdfs:label|Statute|

## Properties

