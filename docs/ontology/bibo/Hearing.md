# bibo:Hearing a owl:Class extends [bibo:Event](/ontology/bibo/Event)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|An instance or a session in which testimony and arguments are presented, esp. before an official, as a judge in a lawsuit.|
|subDomain|BIBO|
|rdfs:label|Hearing|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q545861">http://www.wikidata.org/entity/Q545861</a>|

## Properties

