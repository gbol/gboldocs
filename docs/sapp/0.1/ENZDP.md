# sapp:ENZDP a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[pattern](/0.1/pattern)|The pattern used|0:N|xsd:string|
|[maxbitscore](/0.1/maxbitscore)|The maximum bit score obtained|1:1|xsd:Float|
|[likelihoodscore](/0.1/likelihoodscore)||1:1|xsd:Float|
