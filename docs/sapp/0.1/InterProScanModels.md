# sapp:InterProScanModels a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[key](/0.1/key)||0:1|xsd:string|
|[name](/0.1/name)||0:1|xsd:string|
|[description](/0.1/description)|An optional short description|0:1|xsd:string|
|[accession](/0.1/accession)|The accession id of the entry referenced|0:1|xsd:string|
|[xref](/0.1/xref)|Reference to other entries holding entries with related information in other databases|0:N|[XRef](/0.1/XRef)|
