# sapp:Blast a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

[sapp:Cog](/sapp/0.1/Cog)

## Annotations

|||
|-----|-----|
|rdfs:comment|BLAST Modules from SAPP<br>URL: gitlab.com/sapp/Blast|
|rdfs:label|Blast|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[evalue](/0.1/evalue)|The probability of an alignment|0:1|xsd:Double|
|[mismatches](/0.1/mismatches)|The number of mismatches between two sequence objects|0:1|xsd:Integer|
|[bitscore](/0.1/bitscore)|The bitscore of an alignment|0:1|xsd:Float|
|[gaps](/0.1/gaps)|The nuber of gaps between two sequence objects|0:1|xsd:Integer|
|[percIdentity](/0.1/percIdentity)|The percentage identity of a similarity analysis|0:1|xsd:Float|
|[alignmentLength](/0.1/alignmentLength)|The alignment length of two sequence objects|0:1|xsd:Integer|
