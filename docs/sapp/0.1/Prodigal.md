# sapp:Prodigal a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|fromTool|Prodigal|
|rdfs:label|Prodigal|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[rbsMotif](/0.1/rbsMotif)||1:1|xsd:string|
|[gcContent](/0.1/gcContent)||1:1|xsd:string|
|[conf](/0.1/conf)||1:1|xsd:Float|
|[rScore](/0.1/rScore)||1:1|xsd:Float|
|[score](/0.1/score)||1:1|xsd:Double|
|[sScore](/0.1/sScore)||1:1|xsd:Float|
|[cScore](/0.1/cScore)||1:1|xsd:Float|
|[tScore](/0.1/tScore)||1:1|xsd:Float|
|[startType](/0.1/startType)||1:1|xsd:string|
|[rbsSpacer](/0.1/rbsSpacer)||1:1|xsd:string|
|[uScore](/0.1/uScore)||1:1|xsd:Float|
