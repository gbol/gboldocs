# sapp:RNAmmer a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|fromTool|RNAmmer|
|rdfs:label|RNAmmer|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[score](/0.1/score)|Score given by this method|1:1|xsd:Double|
|[attribute](/0.1/attribute)|RNAmmer properties|0:1|xsd:string|
