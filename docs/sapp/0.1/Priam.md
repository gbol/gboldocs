# sapp:Priam a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|Priam|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[profileId](/0.1/profileId)||1:1|xsd:string|
|[profileRegioin](/0.1/profileRegioin)||1:1|[Region](/0.1/Region)|
|[profileProportion](/0.1/profileProportion)||1:1|xsd:Float|
|[evalue](/0.1/evalue)|The probability of an alignment|0:1|xsd:Double|
|[bitScore](/0.1/bitScore)||1:1|xsd:Float|
|[sourceRegion](/0.1/sourceRegion)||1:1|[Region](/0.1/Region)|
|[queryStrand](/0.1/queryStrand)||0:1|xsd:string|
|[positiveHitProbability](/0.1/positiveHitProbability)||1:1|xsd:Float|
|[foundCatalyticPattern](/0.1/foundCatalyticPattern)||1:1|xsd:string|
|[isBestOverlap](/0.1/isBestOverlap)||1:1|xsd:string|
|[profileLength](/0.1/profileLength)||0:1|xsd:Integer|
|[alignLength](/0.1/alignLength)||0:1|xsd:Integer|
