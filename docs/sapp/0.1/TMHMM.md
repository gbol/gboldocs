# sapp:TMHMM a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|TMHMM|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[location](/0.1/location)|TMHMM properties|0:1|[Location](/0.1/Location)|
|[thmmType](/0.1/thmmType)|Type|0:1|xsd:string|
|[sequence](/0.1/sequence)|The sequence, encoded with the default DNA or protein encoding|0:1|xsd:string|
