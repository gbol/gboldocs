# sapp:Cog a owl:Class extends [sapp:Blast](/sapp/0.1/Blast)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|Cog|
|skos:editorialNote|COG results obtained through a BLAST approach|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[letter](/0.1/letter)|The letter of the COG group|0:1|xsd:string|
|[membershipClass](/0.1/membershipClass)|The COG membership class|0:1|xsd:string|
|[label](/0.1/label)|A description of a given object|0:1|xsd:string|
|[organismName](/0.1/organismName)|The name of an organism|0:1|xsd:string|
