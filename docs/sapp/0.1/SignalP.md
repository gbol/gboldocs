# sapp:SignalP a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[d](/0.1/d)|SignalP properties|1:1|xsd:Float|
|[cMax](/0.1/cMax)|SignalP properties|1:1|xsd:Float|
|[cPos](/0.1/cPos)|SignalP properties|1:1|[Base](/0.1/Base)|
|[network](/0.1/network)|SignalP properties|1:1|xsd:string|
|[signal](/0.1/signal)|SignalP properties|1:1|xsd:string|
|[dMaxCut](/0.1/dMaxCut)|SignalP properties|1:1|xsd:Float|
|[sMean](/0.1/sMean)|SignalP properties|1:1|xsd:Float|
|[sMax](/0.1/sMax)|SignalP properties|1:1|xsd:Float|
|[sPos](/0.1/sPos)|SignalP properties|1:1|[Base](/0.1/Base)|
|[yMax](/0.1/yMax)|SignalP properties|1:1|xsd:Float|
|[yPos](/0.1/yPos)|SignalP properties|1:1|[Base](/0.1/Base)|
