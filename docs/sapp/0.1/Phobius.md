# sapp:Phobius a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|Phobius|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[location](/0.1/location)||1:1|[Location](/0.1/Location)|
|[description](/0.1/description)||1:1|xsd:string|
|[phobiusType](/0.1/phobiusType)||1:1|xsd:string|
