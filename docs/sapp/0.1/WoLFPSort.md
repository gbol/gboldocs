# sapp:WoLFPSort a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|WoLFPSort|
|skos:editorialNote|WoLFPSort Module from SAPP<br>URL: gitlab.com/sapp/wolfpsort|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[subLocation](/0.1/subLocation)|subcellular localization of a predicted protein|1:N|IRI|
|[score](/0.1/score)|Score given by this method|1:1|xsd:Double|
