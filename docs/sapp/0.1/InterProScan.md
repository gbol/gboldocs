# sapp:InterProScan a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:label|InterProScan|
|skos:editorialNote|InterProScan Module from SAPP<br>URL: gitlab.com/sapp/annotation/interproscan|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[evalue](/0.1/evalue)|The probability of an alignment|0:1|xsd:Double|
|[familyName](/0.1/familyName)||0:1|xsd:string|
|[score](/0.1/score)|Score given by this method|1:1|xsd:Double|
