# sapp:HMM a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[TDievalue](/0.1/TDievalue)||1:1|xsd:Double|
|[accuracy](/0.1/accuracy)||1:1|xsd:Double|
|[TDbias](/0.1/TDbias)||1:1|xsd:Double|
|[ENVlocation](/0.1/ENVlocation)||1:1|[Location](/0.1/Location)|
|[FSevalue](/0.1/FSevalue)||1:1|xsd:Double|
|[TDcevalue](/0.1/TDcevalue)||1:1|xsd:Double|
|[TDscore](/0.1/TDscore)||1:1|xsd:Double|
|[FSscore](/0.1/FSscore)||1:1|xsd:Double|
|[TDdomainNumber](/0.1/TDdomainNumber)||1:1|xsd:Integer|
|[FSbias](/0.1/FSbias)||1:1|xsd:Double|
|[ALIlocation](/0.1/ALIlocation)||1:1|[Location](/0.1/Location)|
|[TDdomainsTotal](/0.1/TDdomainsTotal)||1:1|xsd:Integer|
|[HMMlocation](/0.1/HMMlocation)||1:1|[Location](/0.1/Location)|
