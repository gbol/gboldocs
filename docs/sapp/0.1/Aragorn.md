# sapp:Aragorn a owl:Class extends [ProvenanceAnnotation](/0.1/ProvenanceAnnotation)

## Subclasses

## Annotations

|||
|-----|-----|
|fromTool|Aragorn|
|rdfs:label|Aragorn|
|skos:exactMatch|<a href="http://www.wikidata.org/entity/Q24634141">http://www.wikidata.org/entity/Q24634141</a>|

## Properties

