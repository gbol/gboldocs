# rov:legalName a ObjectProperty

## Domain

definition: The legal name of the organization<br>
[prov:Organization](/ns/prov/Organization)

definition: The legal name of the organization<br>
[foaf:Organization](/foaf/0.1/Organization)

## Range

xsd:string

## Annotations


