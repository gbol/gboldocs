# void:uriSpace a ObjectProperty

## Domain

definition: This property can be used to state that all entity URIs in a dataset start with a given string. In other words, they share a common “URI namespace”.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


