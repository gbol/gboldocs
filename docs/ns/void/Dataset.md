# void:Dataset a owl:Class

## Subclasses

[Database](/0.1/Database)

[GBOLDataSet](/0.1/GBOLDataSet)

[DataFile](/0.1/DataFile)

## Annotations

|||
|-----|-----|
|skos:definition|The fundamental concept of VoID is the dataset. A dataset is a set of RDF triples that are published, maintained or aggregated by a single provider. Unlike RDF graphs, which are purely mathematical constructs [RDF-CONCEPTS], the term dataset has a social dimension: we think of a dataset as a meaningful collection of triples, that deal with a certain topic, originate from a certain source or process, are hosted on a certain server, or are aggregated by a certain custodian. Also, typically a dataset is accessible on the Web, for example through resolvable HTTP URIs or through a SPARQL endpoint, and it contains sufficiently many triples that there is benefit in providing a concise summary.<br><br>Since most datasets describe a well-defined set of entities, datasets can also be seen as a set of descriptions of certain entities, which often share a common URI prefix (such as <a href="http://dbpedia.org/resource/)">http://dbpedia.org/resource/)</a>.<br><br>In VoID, a dataset is modeled as an instance of the void:Dataset class. Such a void:Dataset instance is a single RDF resource that represents the entire dataset, and thus allows us to easily make statements about the entire dataset and all its triples.<br><br>The relationship between a void:Dataset instance and the concrete triples contained in the dataset is established through access information, such as the address of a SPARQL endpoint where the triples can be accessed.|
|subDomain|DocumentCore|
|skos:editorialNote|#Fields we could include from FAIR<br>#fields taken from fair<br>#dcterms:isPartOf IRI*;<br>#fdp-o:metadataIssued xsd:dateTime;<br>#fdp-o:metadataModified xsd:dateTime;     <br>#dcterms:issued xsd:dateTime*;<br>#dcterms:modified xsd:dateTime*; <br># dcterms:rights IRI*; <br>#dcterms:conformsTo IRI*; <br> #fdp-o:metadataIdentifier @:metadataID;   <br># dcterms:publisher @:agent+;;<br>#dcat:landingPage IRI*;<br>#dcat:theme IRI+;<br>#dcat:keyword xsd:string*;   <br>#Links to distribution|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[dc:license](/dc/terms/license)|A legal document giving official permission to do something with this dataset. It must be an IRI referencing to an resolvable URI returning the text of the license.|0:1|IRI|
|[dc:hasVersion](/dc/terms/hasVersion)|The textual description of the version of the document, please not the version property in GBOLDataSet|1:1|xsd:string|
|[void:uriSpace](/ns/void/uriSpace)|This property can be used to state that all entity URIs in a dataset start with a given string. In other words, they share a common “URI namespace”.|0:1|IRI|
|[void:uriRegexPattern](/ns/void/uriRegexPattern)|In cases where a simple string prefix match is insufficient, this property can be used. It expresses a regular expression pattern that matches the URIs of the dataset's entities.|0:1|xsd:string|
|[wv:waiver](/waiver/terms/normswaiver)|Best practice is use the URI of a waiver legal document as the value of this property.|0:1|xsd:string|
|[wv:norms](/waiver/terms/normsnorms)|Norms are not legally binding but represent the general principles or "code of conduct" adopted by a community for access and use of resources|0:1|IRI|
|[dc:language](/dc/terms/language)|The language of this resource.|0:N|xsd:string|
|[dc:description](/dc/terms/description)|A textual description of the dataset.|0:1|xsd:string|
|[void:uriLookupEndpoint](/ns/void/uriLookupEndpoint)|A protocol endpoint for simple URI lookups for this data set.|0:1|IRI|
|[void:sparqlEndpoint](/ns/void/sparqlEndpoint)|An optional sparql endpoint to query the dataset|0:1|IRI|
|[void:dataDump](/ns/void/dataDump)|If available, the RDF dump of the dataset.|0:1|IRI|
|[rdfs:comment](/2000/01/rdf-schema/comment)|A comment on the dataset|0:1|xsd:string|
|[dc:title](/dc/terms/title)|The name of the dataset.|0:1|xsd:string|
|[rdfs:label](/2000/01/rdf-schema/label)|Used to provide a human-readable version of a resource's name|0:1|xsd:string|
