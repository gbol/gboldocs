# void:sparqlEndpoint a ObjectProperty

## Domain

definition: An optional sparql endpoint to query the dataset<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


