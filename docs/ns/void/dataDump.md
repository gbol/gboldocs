# void:dataDump a ObjectProperty

## Domain

definition: If available, the RDF dump of the dataset.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


