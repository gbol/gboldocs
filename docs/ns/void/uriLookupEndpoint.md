# void:uriLookupEndpoint a ObjectProperty

## Domain

definition: A protocol endpoint for simple URI lookups for this data set.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:anyURI

## Annotations


