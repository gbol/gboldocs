# void:uriRegexPattern a ObjectProperty

## Domain

definition: In cases where a simple string prefix match is insufficient, this property can be used. It expresses a regular expression pattern that matches the URIs of the dataset's entities.<br>
[void:Dataset](/ns/void/Dataset)

## Range

xsd:string

## Annotations


