# prov:Entity a owl:Class

## Subclasses

[Database](/0.1/Database)

[AnnotationResult](/0.1/AnnotationResult)

[TemporaryFile](/0.1/TemporaryFile)

[GBOLDataSet](/0.1/GBOLDataSet)

[GBOLLinkSet](/0.1/GBOLLinkSet)

[DataFile](/0.1/DataFile)

## Annotations

|||
|-----|-----|
|gen:framing|prov:wasAttributedTo<br>prov:wasGeneratedBy|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[prov:wasGeneratedBy](/ns/prov/wasGeneratedBy)|The annotation activity that created this annotation result|0:1|[prov:Activity](/ns/prov/Activity)|
|[prov:wasAttributedTo](/ns/prov/wasAttributedTo)|The agent that performed or initiated the activity that generated this entity|0:1|[prov:Agent](/ns/prov/Agent)|
|[prov:wasDerivedFrom](/ns/prov/wasDerivedFrom)|Any entries used as input in the activity that created this entity|0:N|[prov:Entity](/ns/prov/Entity)|
