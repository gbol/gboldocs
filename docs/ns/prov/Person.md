# prov:Person a owl:Class extends [prov:Agent](/ns/prov/Agent)

## Subclasses

[Curator](/0.1/Curator)

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|
|rdfs:label|prov:Person|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[foaf:phone](/foaf/0.1/phone)|The phone number of the entity|0:1|xsd:string|
|[bibo:suffixName](/ontology/bibo/suffixName)|The suffix of a name|0:1|xsd:string|
|[foaf:givenName](/foaf/0.1/givenName)|The given name of a person|0:1|xsd:string|
|[foaf:surName](/foaf/0.1/surName)|The first name of the person|0:1|xsd:string|
|[bibo:prefixName](/ontology/bibo/prefixName)|The prefix of a name|0:1|xsd:string|
|[orcid](/0.1/orcid)|The unique identifier to identify the scientist|0:1|IRI|
|[foaf:depiction](/foaf/0.1/depiction)|Image of the person use link to image|0:1|IRI|
|[foaf:familyName](/foaf/0.1/familyName)|The familyName of the person, preferable use the surName property|0:1|xsd:string|
|[bibo:localityName](/ontology/bibo/localityName)|Used to name the locality of a publisher, an author, etc.|0:1|xsd:string|
|[foaf:title](/foaf/0.1/title)|Title of the person  (Mr, Mrs, Ms, Dr. etc)|0:1|xsd:string|
|[foaf:firstName](/foaf/0.1/firstName)|The first name of the person|0:1|xsd:string|
|[foaf:homepage](/foaf/0.1/homepage)|A homepage for some thing.|0:1|xsd:string|
|[foaf:mbox](/foaf/0.1/mbox)|mail address use  URI scheme (see RFC 2368).|0:1|IRI|
