# prov:endedAtTime a ObjectProperty

## Domain

definition: The time at which the activity is ended, if unknown set equal to startedAt.<br>
[AnnotationActivity](/0.1/AnnotationActivity)

definition: The time at which the activity is ended, if unknown set equal to startedAt.<br>
[prov:Activity](/ns/prov/Activity)

## Range

xsd:dateTime

## Annotations


