# prov:actedOnBehalfOf a ObjectProperty

## Domain

definition: Tells that this agent performed something for another agent. For example PFam domain scan actedOnBehalfOf InterproScan or a curator that actedOnBehalfOf the EBI.<br>
[prov:Agent](/ns/prov/Agent)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations


