# prov:wasGeneratedBy a ObjectProperty

## Domain

definition: The annotation activity that created this annotation result<br>
[AnnotationResult](/0.1/AnnotationResult)

definition: The annotation activity that created this annotation result<br>
[prov:Entity](/ns/prov/Entity)

## Range

[prov:Activity](/ns/prov/Activity)

[AnnotationActivity](/0.1/AnnotationActivity)

## Annotations


