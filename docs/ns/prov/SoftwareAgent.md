# prov:SoftwareAgent a owl:Class extends [prov:Agent](/ns/prov/Agent)

## Subclasses

[AnnotationSoftware](/0.1/AnnotationSoftware)

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|

## Properties

