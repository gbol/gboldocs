# prov:wasInformedBy a ObjectProperty

## Domain

definition: Denote the parent activity that initiated this task<br>
[prov:Activity](/ns/prov/Activity)

## Range

[prov:Activity](/ns/prov/Activity)

## Annotations


