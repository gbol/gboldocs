# prov:Activity a owl:Class

## Subclasses

[AnnotationActivity](/0.1/AnnotationActivity)

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[prov:startedAtTime](/ns/prov/startedAtTime)|The time at which the activity is started|0:1|xsd:DateTime|
|[wasAssociatedWith](/0.1/wasAssociatedWith)|The agent which be performed this activity|0:1|[prov:Agent](/ns/prov/Agent)|
|[prov:endedAtTime](/ns/prov/endedAtTime)|The time at which the activity is ended, if unknown set equal to startedAt.|0:1|xsd:DateTime|
|[prov:used](/ns/prov/used)|Description of each of the Data Sets used|0:N|[prov:Entity](/ns/prov/Entity)|
|[prov:generated](/ns/prov/generated)|Description of each generated data set|0:N|[prov:Entity](/ns/prov/Entity)|
|[prov:wasInformedBy](/ns/prov/wasInformedBy)|Denote the parent activity that initiated this task|0:1|[prov:Activity](/ns/prov/Activity)|
