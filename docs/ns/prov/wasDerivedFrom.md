# prov:wasDerivedFrom a ObjectProperty

## Domain

definition: Any entries used as input in the activity that created this entity<br>
[TemporaryFile](/0.1/TemporaryFile)

definition: Any entries used as input in the activity that created this entity<br>
[prov:Entity](/ns/prov/Entity)

## Range

[prov:Entity](/ns/prov/Entity)

## Annotations


