# prov:Agent a owl:Class

## Subclasses

[prov:Organization](/ns/prov/Organization)

[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

[prov:Person](/ns/prov/Person)

## Annotations

|||
|-----|-----|
|subDomain|DocumentWiseProv|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[foaf:name](/foaf/0.1/name)|Name of the agent|0:1|xsd:string|
|[prov:actedOnBehalfOf](/ns/prov/actedOnBehalfOf)|Tells that this agent performed something for another agent. For example PFam domain scan actedOnBehalfOf InterproScan or a curator that actedOnBehalfOf the EBI.|0:1|[prov:Agent](/ns/prov/Agent)|
