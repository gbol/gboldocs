# prov:wasAttributedTo a ObjectProperty

## Domain

definition: The agent that performed or initiated the activity that generated this entity<br>
[AnnotationResult](/0.1/AnnotationResult)

definition: The agent that performed or initiated the activity that generated this entity<br>
[prov:Entity](/ns/prov/Entity)

## Range

[prov:Agent](/ns/prov/Agent)

## Annotations


