# prov:startedAtTime a ObjectProperty

## Domain

definition: The time at which the activity is started<br>
[AnnotationActivity](/0.1/AnnotationActivity)

definition: The time at which the activity is started<br>
[prov:Activity](/ns/prov/Activity)

## Range

xsd:dateTime

## Annotations


