GBOLOntology can... using the -doc option compile the markdown docs file and _tmp.yaml file.

The folders inside the doc can be copied manually to the gbol.gitlab.io git repository 
(be aware to not overwrite generic introduction markdown files.

Then you need to manually modify the mkdocs.yml file by removing the part after Empusa and before the google tracker.

Then mkdocs build can be executed or comitted to the server for automatic building and deployment
